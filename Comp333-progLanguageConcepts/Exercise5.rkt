;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-advanced-reader.ss" "lang")((modname Exercise5) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #t #t none #f () #f)))

(define (my-reverse L)
 (if (null? L)
     '()
      (append (my-reverse (cdr L)) (list(car L)))))

(my-reverse (list 1 2 3))
(my-reverse (list '(3 2) 1 '(2)))
(my-reverse null)