/*
 * Joshua Benz
 * 105284931
 * project 4 -- Sleeping barber
 * */

#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>

void randomwait();
void barber_run();
void customer_run();

int seats[2]; //mutex
int customers[2];
int barber[2];  
int freeseats[2];


void V(int pd[]) { //signal
	int a=1;
	write(pd[1],&a,sizeof(int));
}

void P(int pd[]) { //wait
	int a;
	read(pd[0],&a,sizeof(int));
}

void main() {
	pipe(seats); // read/write lock to modify free seats
	pipe(customers); // how many customers are waiting
	pipe(barber); // barber ready to cut? 0=no 1=yes
	pipe(freeseats); // implemented to pass the integer value for number of seats
                    // store the integer value for the number of seats
	
	V(seats); // Sets the initial value of seats to 1
	
	int num=3; // Free Seats = 3
	write(freeseats[1],&num,sizeof(int));
	
	if (fork() == 0) {  // Start 1 barber
		srand(time(0)+11); // Randomized seed
		barber_run();
		return;
	}
	int i;
	for (i=1;i<=10;i++) { // Start 10 customers randomly
		if (fork() == 0) {
			randomwait(i); // random wait before next customer arrives
			srand(time(0)+ i); // different seed than barber for different delays
			customer_run();
			return;
		}
	}
}

void barber_run() {
	int num,i;
    
    for (i=1;i<=10;++i) { // at most 10 customers will get their hair cut
    	printf("Barber %d is trying to get a customer\n",i);

        P(customers);   //barber waiting for customer
        printf("Barber %d is waiting for the seat to become free\n", i);
        P(seats);       //barber waiting for access to seats
        
        read(freeseats[0], &num, sizeof(int));
        num++; //increment freeseats
        write(freeseats[1], &num, sizeof(int));
        printf("Barber %d is increasing number of seats to %d\n", i, num);

        V(seats); //barber is ready to cut
        V(barber); //release the lock on seats
		
        printf("Barber is now cutting hair %d\n",i);
		randomwait(1); // random wait before finishing haircut
    }
}

void customer_run() {
    int num;

	printf("- New customer trying to find a seat\n");
    P(seats); //customer try to access seats
    read(freeseats[0], &num, sizeof(int));
    if(num > 0) { //checks if there are seats available
        num--;
        write(freeseats[1], &num, sizeof(int)); //decrements seats
        printf("- Customer is decreasing the number of free seats to %d\n", num);

        V(customers); //customer is ready
        V(seats); //release access to seats
        P(barber); //wait for a barber
        printf("- Customer is now waiting for the barber\n");
    } else { //there are no free seats
        write(freeseats[1], &num, sizeof(int));
        V(seats); //release access to seats
        printf("* Customer giving up: No free chairs in waiting room\n");
    }
}
 
void randomwait(int d) { // random 0 to d sec delay
	int delay;
	struct timespec tim, tim2;
    tim.tv_sec = 0;
	delay = abs(rand() % 1000000000) * d;
	tim.tv_nsec = delay;
	nanosleep(&tim,&tim2);
}
 
