#!/usr/bin/bash

pwd
mkdir SubDir
cd SubDir
echo "\"I'm personally convinced that computer science has a lot in common with physics. Both are about how the world works at a rather fundamental level. The difference, of course, is that while in physics you're supposed to figure out how the world is made up, in computer science you create the world. Within the confines of the computer, you're the creator. You get to ultimately control everything that happens. If you're good enough, you can be God. On a small scale.\" -Linus
Torvalds" >> data
cat data
cp data try
ls
mkdir NewSubDir 
mv try NewSubDir
cd NewSubDir
mv try newTry
ls -R -l > hold
cat hold
