#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>

int main(int argc, char* argv[])
{
    struct stat sbFile, sbPrgm;

/*    
    DIR *d;
    struct dirent *dir;

    d = opendir(".");

    if(argc < 2) {
        fprintf(stderr, "No arguments supplied!\n");
        exit(1);
    }

    if(d) {
        while((dir = readdir(d)) != NULL) {
           if(stat(dir->d_name, &sbPrgm) < 0 ) {
                fprintf(stderr, "cannot access program file!\n");
                exit(1);
           } else {
                if((sbPrgm.st_mode & S_IXUSR) &&
                    (sbPrgm.st_mode & S_IXGRP) &&
                    (sbPrgm.st_mode & S_IXOTH)) {
                        printf("Found an executable: %s\n", dir->d_name);
                        break;
                }
            }
        }
    }
  */  
    if((stat(argv[1], &sbFile) < 0) || (stat(argv[0], &sbPrgm) < 0)) {
        fprintf(stderr, "Cannot access file!\n");
        exit(1);
    }
    
    if((sbFile.st_gid == getgid())) { //sbPrgm.st_uid)) {
        printf("yes same goup as owner!\n");
    } else {
        printf("NOPE!\n");
    }

    //printf("Ownership: UID=%d, GID=%d", sbPrgm.st_uid, sbPrgm.st_gid);
    return 0;

}

