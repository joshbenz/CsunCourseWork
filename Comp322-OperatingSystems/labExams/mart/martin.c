#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <pwd.h>

int main(int argc, char* argv[])
{

/*********** First Program Question **********/
    struct stat sb;
    stat(argv[1], &sb);

    if(sb.st_mode & S_IRUSR) printf("owner-read\n");
    if(sb.st_mode & S_IWUSR) printf("owner-write\n");
    if(sb.st_mode & S_IXUSR) printf("owner-execute\n");

/*
 * All the constants and everything used in Program 1 was found with
 * man 2 stat
 */

//end First Program

/********** Second Program Question **********/

    struct passwd pwd;
    struct passwd *result;
    size_t buffsize = sysconf(_SC_GETPW_R_SIZE_MAX);
    char* buff = malloc(buffsize);
    char* name = "guest78"; //this is the username that he gave us to login with

    int n = getpwnam_r(name ,&pwd, buff, buffsize, &result);

    if(pwd.pw_uid == sb.st_uid) printf("yes\n");
    else                        printf("no\n");

/*
 * All the info for this one can be found with commands:
 * man pwd.h
 * man getpwuid
 */

//end Second Program

    return 0;
}

