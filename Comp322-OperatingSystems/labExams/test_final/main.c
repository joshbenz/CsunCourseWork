#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pwd.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
    struct stat sb;
    stat(argv[1], &sb);

    struct passwd pwd;
    struct passwd* result;
    size_t buffsize = sysconf(_SC_GETPW_R_SIZE_MAX);
    char* pwbuff = malloc(buffsize);

    int n = getpwuid_r(sb.st_uid, &pwd, pwbuff, buffsize, &result);
    printf("Home directory: %s\n", pwd.pw_dir);

    return 0;
}
