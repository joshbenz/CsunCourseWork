/*
 * Joshua Benz
 * 105284931
 * Project 5 client/server with Sockets
 * 
 * client.c
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <strings.h>
#include <stdlib.h>
#include <unistd.h>

void error(char *msg)
{
    perror(msg);
    exit(0);
}

int main(int argc, char *argv[])
{
    int sockfd, portno, n; //socket file descriptor, port#, number of chars read or written

/*
 * struct sockaddr_in {
 *          short   sin_family;        --> code for address family
 *          u_short sin_port;          --> port
 *          struct  in_addr sin_addr;  --> in_addr = unsigned long s_addr
 *          char    sin_zero[8];       --> host IP address
 *          };
 */
    struct sockaddr_in serv_addr;

    struct hostent *server; //defined in netdb.h
/*
 * struct  hostent {
 * char    *h_name;                 --> official name of host 
 * char    **h_aliases;             --> alias list 
 * int     h_addrtype;              --> host address type 
 * int     h_length;                --> length of address 
 * char    **h_addr_list;           --> list of addresses from name server 
 * #define h_addr  h_addr_list[0]   --> address, for backward compatiblity 
 * };                               --> an alias for the first address in the array of network addresses.
*/
    char buffer[256]; //buffer to wrtie/read to

    if (argc < 3) { // make sure user has 2 args passed when prgm is started
        fprintf(stderr,"usage %s hostname port\n", argv[0]);
        exit(0);
    }

    portno = atoi(argv[2]);
    sockfd = socket(AF_INET, SOCK_STREAM, 0); //open inet stream socket, let OS decide protocol

    if (sockfd < 0) //return entry to descriptor table, or -1 for failure
        error("ERROR opening socket");

    server = gethostbyname(argv[1]);

    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }

    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;                         //init sockaddr_in structure fields
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,server->h_length);
    serv_addr.sin_port = htons(portno);

    if (connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0)  //try to connect
        error("ERROR connecting");
    
    printf("Enter a number: ");
    int num;
    scanf("%d", &num);
    int converted = htonl(num);
    n = write(sockfd, &converted, sizeof(converted));    
    if (n < 0) 
        error("ERROR writing to socket");

    close(sockfd);
    return 0;
}
