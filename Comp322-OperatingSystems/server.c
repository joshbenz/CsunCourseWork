/*
 * Joshua Benz
 * 105284931
 * Project 5 client/server with Socets
 *
 * server.c
 */

#include <stdio.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>

int total[2]; //the added total
int connections[2]; //number of connections

void dostuff(int); /* function prototype */
void error(char *msg)
{
    perror(msg);
    exit(1);
}

int main(int argc, char *argv[])
{
    int setter = 0;
    pipe(total); 
    pipe(connections);
    write(total[1], &setter, sizeof(int));
    write(connections[1], &setter, sizeof(int));

     int sockfd, newsockfd, portno, clilen, pid;
     struct sockaddr_in serv_addr, cli_addr;

/*
struct sockaddr_in {
       	short   sin_family;        --> code for address family
       	u_short sin_port;          --> port
       	struct  in_addr sin_addr;  --> in_addr = unsigned long s_addr
       	char    sin_zero[8];       --> host IP address
};
*/


     if (argc < 2) {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
     }

	//opens new socket
        /*
               	arg1 --> address domain 
                       	AF_INET for internet
                       	AF_UNIX for common file system shared
               	arg2 -->  type of socket
                       	SOCK_STREAM for stream of chars like read from a pipe or file
                       	SOCk_DGRAM  aka datagram, chunks of data
               	arg3 --> Protocol
                       	0 = OS will choose which protocol... TCP for stream, UDP for datagram
       	*/ 

     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) 
        error("ERROR opening socket");
     bzero((char *) &serv_addr, sizeof(serv_addr));
     portno = atoi(argv[1]);
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY; //INADDR_ANY gives machine IP 
     serv_addr.sin_port = htons(portno);  //convert from  network byte order to host byte order
     if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0) 
              error("ERROR on binding");  //bind current host IP and port server should run on to socket
     listen(sockfd,5);
     clilen = sizeof(cli_addr);

     while (1) {
         newsockfd = accept(sockfd,(struct sockaddr *) &cli_addr, &clilen);

         if (newsockfd < 0) error("ERROR on accept");
         pid = fork();
         if (pid < 0) error("ERROR on fork");
         if (pid == 0)  {
             close(sockfd);
             dostuff(newsockfd);
             printf(""); //for whatever reason...it only reads multiple integers if i put this here???
             exit(0);
         } else close(newsockfd);
     } /* end of while */
     return 0; /* we never get here */
}

/******** DOSTUFF() *********************
 There is a separate instance of this function 
 for each connection.  It handles all communication
 once a connnection has been established.
 *****************************************/
void dostuff (int sock)
{

    int recieved = 0;
    int converted;
    int n;
    int count, totl;
    
   n = read(sock, &recieved ,sizeof(int));
   converted = ntohl(recieved);
   
   if (n < 0) error("ERROR reading from socket");
   
    read(connections[0], &count, sizeof(int));
    read(total[0], &totl, sizeof(int));
    totl += converted;
    count++;
    
    write(total[1], &totl, sizeof(int));
    write(connections[1], &count, sizeof(int));
    printf("Recieved Number %d: %d\n",count, converted);
    close(sock);
    if(count == 2) { 
        printf("Here are the numbers added together: %d\n", totl);
        return;
    }
} 
