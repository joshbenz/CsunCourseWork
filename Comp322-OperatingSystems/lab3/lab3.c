/*Joshua Benz, Comp322, lab3*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include <errno.h>


int main(int argc, char *argv[])
{
	pid_t pid = getpid(); /*current process id should be the program itself*/
	int i;

    int errors = 0;
    struct stat buff; //stat structure

	pid_t cpid, mypid, wpid;
	for(i=1; i<argc; i++) {
		cpid = fork(); /*fork a child process*/
		if(cpid == 0) {
			printf("File: %s\n", argv[i]);

            if(stat(argv[i], &buff) < 0) { //collect errors if there are any from stat
                fprintf(stderr, "%s: cannot access %s\n", argv[0], argv[i]);
                errors++;
                continue;
            }

            size_t buffsize = sysconf(_SC_GETPW_R_SIZE_MAX);
            char * pwbuff = malloc(buffsize);
            struct passwd pwd;
            struct passwd *result; //structs to hold pwd info
            uid_t uid = getuid();

            getpwuid_r(uid, &pwd, pwbuff, buffsize, &result);
    		printf("Directory: %s\n", pwd.pw_dir);

	    	if(uid == buff.st_uid) { //owner
		    	printf("You have owner permissions: ");
                printf((buff.st_mode & S_IRUSR) ? "read "    : "");
                printf((buff.st_mode & S_IWUSR) ? "write "   : "");
                printf((buff.st_mode & S_IXUSR) ? "execute " : "");

            } else if(uid == buff.st_gid) { //group
                printf("You have group permissions: ");
                printf((buff.st_mode & S_IRGRP) ? "read "    : "");
                printf((buff.st_mode & S_IWGRP) ? "write "   : "");
                printf((buff.st_mode & S_IXGRP) ? "execute " : "");

            } else { //world
			    printf("You have general permissions: ");
                printf((buff.st_mode & S_IROTH) ? "read "    : "");
                printf((buff.st_mode & S_IWOTH) ? "write "   : "");
                printf((buff.st_mode & S_IXOTH) ? "execute " : "");
		    }


		} else if(cpid > 0) {
			mypid = getpid();
		} else {
			fprintf(stderr, "Fork Failed!");
			return 1;
		}
	}

	if(cpid == 0) { /*do child things*/
		mypid = getpid();
		/*execlp("/bin/ls", "ls", NULL); give child something to execute...for later*/

	} else {	/*do parent things*/
		int status = 0;
		while((wpid = wait(&status)) > 0);
		printf("\nDone!\n");
	}
	return 0;
}


