/*
 *	Joshua Benz
 *	COMP 222 TT 9:30
 *	Assignment 4
 *	Date
 * */

#include "stdio.h"
#include "stdlib.h"
#include "math.h"


#define LRU 0
#define FIFO 1

/********** Function Prototypes **********/

typedef struct {
	int pFrame;	//phsycial frame
	int vFrame;	//virtual page
} node;

/***** Simple Array-Based Queue *****/
int head, tail;
void queueInit();
int enqueue(node n);
node dequeue();
int isEmpty();
int isFull();
node* queue = NULL;
node* list = NULL;
//done with queue prototypes

/***** used for LRU operations *****/
void shift();

int searchTable(int vPage, int pAddr, int vAddr);
void printPage(node n);
void printPageTable();
char* readInput();
void setParams();
void menu();
void map(int vSize, int virtualAddr);
void printTable();
/********** Global Variables **********/
int memSize;
int pgSize;
int policy;
node* pageTable = NULL;

/********** Start Queue implementation **********/
//Note: For my purposes
//	1 = true
//	0 = false

void queueInit()
{
	int i;
	head = -1;
	tail = -1;
	queue = (node*)malloc(((memSize/pgSize)-1) * sizeof(node));

	for(i=0; i<(memSize/pgSize); i++) {
		queue[i].pFrame = -1;
		queue[i].vFrame = -1;
	}
}

int enqueue(node n)
{
//	if(isFull() == 1) {
//		return 0;
//	}
	tail++;
	queue[tail % ((memSize/pgSize))] = n;
	return 1;
}

node dequeue()
{
//	if(isEmpty() == 1) {
//		return queue[0];
//	} else {

	head++; 
	int i = head % (memSize / pgSize);
		node n = queue[i];
		queue[i].pFrame = -1;
		queue[i].vFrame = -1;
		//return queue[head % ((memSize/pgSize))];
		return n;
//	}
}

int isEmpty()
{
	if(head == tail) {
		return 1;
	} else {
		return 0;
	}
}

int isFull()
{
	if((tail - (memSize/pgSize)) == head) {
		return 1;
	} else {
		return 0;
	}
}

/********** End Queue section **********/

char* readInput()
{
	char* line = NULL;
	size_t len = 0;
	ssize_t read;

	read = getline(&line, &len, stdin);
	return line;
}

void setParams()
{
	int i, pFrames;
	printf("\n\nEnter main memory size (words): ");
	sscanf(readInput(), "%d", &memSize);

	printf("Enter page size (words/page): ");
	sscanf(readInput(), "%d", &pgSize);

	printf("Enter replacement policy (0=LRU, 1=FIFO): ");
	sscanf(readInput(), "%d", &policy);

	pFrames = memSize / pgSize; //physical frames
	pageTable = (node*)malloc(pFrames * sizeof(node));

	for(i=0; i<pFrames; i++) {
		pageTable[i].pFrame = -1;
		pageTable[i].vFrame = -1;
	}
}

void menu()
{
	printf("\nJoshua Benz\n");
	printf("Virtual address mapping:\n");
	printf("-----------------------\n");
	printf("1) Enter parameters\n");
	printf("2) Map virtual address\n");
	printf("3) Quit\n");
	printf("Enter selection: ");
}

node createPage(node n)
{
	n.pFrame = -1;
	n.vFrame = -1;

	return n;
}

void printPage(node n)
{
	printf("*** VP %i --> PF %i\n", n.vFrame, n.pFrame);
}

void printPageTable()
{
	int i;

	for(i=0; i<(memSize/pgSize); i++) {
		if(queue[i].pFrame == -1) {
			//do nothing
		} else {
			printPage(queue[i]);
		}
	}
}

void printTable()
{
	int i;
	for(i=(memSize/pgSize)-1; i>=0; i--) {
		if(queue[i].pFrame == -1) {
		
		} else {
			printPage(queue[i]);
		}
	}
}

int searchTable(int vPage, int pAddr, int vAddr)
{
	int i, flag = 0;
	for(i=0; i<(memSize/pgSize); i++) {
		if(queue[i].vFrame == vPage) {
			flag = 1;		//no page fault
		}
	}

	if(flag == 1) { //no page faule
		printf("\n*** Virtual address %i maps to physical address %i\n\n", vAddr, pAddr);
		return flag;
	} else if(flag == 0) {
		printf("\n*** Page fault!\n\n");
		return flag;
	} else {}
}

void map(int pFrameSize, int virtualAddr)
{
	int vPage = virtualAddr / pgSize;		//virtual page
	int physicalAddr = virtualAddr % pgSize;	//phsyical address
	int isLoaded;
	//we also have queue[] as a glbal variable

	node page = createPage(page);
	//page.pFrame = memSize / physicalAddr;///////////////no
	page.vFrame = vPage;
	if(isFull() == 0) {
		page.pFrame = tail-head; //this is from the tail of the queue...tail starts at -1
		isLoaded = searchTable(vPage, physicalAddr, virtualAddr);
		if(isLoaded == 1) { //page already in physical frame
			printPageTable();
		} else {
			enqueue(page);		//since we are just filling it up      
			printPageTable();
		}
		return;
	} else if(isFull() == 1) {
		if(policy == FIFO) {
			isLoaded = searchTable(vPage, physicalAddr, virtualAddr);
			if(isLoaded == 1) { //no page fault
				printPageTable();
			} else {
				node n = dequeue();
				page.pFrame = n.pFrame;	//set the physical frame we replaced
				enqueue(page);
				printTable();
			}
		} else if(policy == LRU) {
			list = queue; //since no longer used as a queue
			isLoaded = searchTable(vPage, physicalAddr, virtualAddr);
			
			if(isLoaded == 1) {
				int i, pos;
				for(i=0; i<memSize/pgSize; i++) {
					if(queue[i].vFrame == vPage) {
						pos = i;
						break;
					}
				}
				node no = queue[pos];
				for(i=0; i < pos; i++) {
					*(list + i) = *(list + i + 1);
				}
				page.pFrame = no.pFrame;
				list[0] = page;
			}
			


		/*	node no = list[0]; //take the least recently used which is the top
				int i;
			for(i=(memSize/pgSize)-1; i>0; i--) {
				*(list - i) = *(list - i - 1);
			}
			page.pFrame = no.pFrame;
			enqueue(page);
			//list[memSize/pgSize] = page;	*/
			printPageTable();
		} else {
			return;
		}
	}
}

int main()
{
	int input = -1;
	int addr;

	while(input != 3) {
		menu();
		sscanf(readInput(), "%d", &input);
		switch(input) {
			case 1:
				input = -1;
				setParams();
				queueInit();
				break;
			case 2:
				input = -1;
				printf("Enter virtual memory address to access: ");
				sscanf(readInput(), "%d", &addr);

				map(memSize/pgSize, addr);

			case 3: //quit
				break;
			defualt:
				break;
		}
	}
	return 0;
}
