/*
 * Programmer: Joshua Benz
 * Class: COMP 222
 * Project: #2 direct mapped cache
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
	int tag;
	int* block;
} node;


node* cache = NULL;
int* memory = NULL;

int* setParams();
int isValid(int* params, int size);
int isPowerOfTwo(int* params, int size);
void error(int x, char* c);

void printMenu();
int getInput();
node* initCache(int cacheSize, int blockSize);
int* initMem(int size);

int getTag(int addr, int* params);
int getLine(int addr, int* params);
int getWord(int addr, int* params);
int log_2(int addr);
void readCache(node* c, int addr, int* params, int* mem);
void printOut(int w, int b, int t, int v);
void writeCache(node*c, int addr, int value, int* params, int* mem);

int* setParams()
{
	int arrSize = 3;
	int* params = malloc(sizeof(int) * arrSize);
	printf("Enter main memory size (words): ");
	scanf(" %d", &params[0]);			/*index 0 = memory size*/

	printf("Enter cache size (words): ");
	scanf(" %d", &params[1]);			/*index 1 = cache size*/

	printf("Enter block size (words/bytes): ");
	scanf(" %d", &params[2]);			/*index 2 = block size*/

	if(isValid(params, arrSize) == 1) {
		printf("** Data Accepted\n\n");
	 	cache = initCache(params[1], params[2]);//allocate stuff, assign blah blah
		memory= initMem(params[0]);
	}
	return params;
}


int* initMem(int size)
{
	int* mem = malloc(sizeof(int)*size);
	return mem;
}

node* initCache(int cacheSize, int blockSize) 
{
	int i;
	int lines = cacheSize * blockSize;
	node* c = malloc(sizeof(node)*lines); /*num lines*/

	for(i=0; i<lines; i++) {
		c[i].tag = -1;
		c[i].block = NULL;
	}
	return c;
}


void printMenu()
{
	printf("Joshua Benz\n\n");
	printf("Main Memory Mapping to Cache memory mapping: \n");
	printf("---------------------------------------------\n");
	printf("1) Set Parameters\n");
	printf("2) Read Cache\n");
	printf("3) Write to Cache\n");
	printf("4) Exit\n");
}

int getInput()
{
	int in;
	printMenu();
	printf("Enter Selection: ");
	scanf(" %d", &in);
	printf("\n");
	return in;
}

/***************************** All functions used for input validation ********************************/
int isValid(int* params, int size)
{
	if(params[1] < params[2]) { /*cache size < block size*/
		error(3, NULL);
		return 0;
	}

	if(params[0] < params[2]) { /*main memory < cache size*/
		error(2,NULL);
		return 0;
	}

	int flag = isPowerOfTwo(params, size);
	char s[30];		/*max string length*/
	if(flag != -1) {			/*Checks if multiple of 2*/
		if(flag == 0) {
			strcpy(s,"main memory size");
		} else if(flag == 1) {
			strcpy(s,"cache size");
		} else if(flag == 2) {
			strcpy(s,"block size");
		} else {}
		error(1, s);
		return 0;
	}
	if(params[1] % params[2] != 0) {	/*cache size % block size != 0*/
						/*if cache size is not some multiple of block size*/
		error(4, NULL);
		return 0;
	}
		return 1; 		/*if all conditions pass, return true*/
}


int isPowerOfTwo(int* params, int size) /*returns which one is wrong or true*/
{
	int flag=-1, tmp; /*starts as true..for my purposes i'm going to be a bad programmer and make true -1*/
	size--;
	while(flag == -1 && size >= 0) {	/*loop through the parameters*/
		tmp = params[size];

		while((tmp%2 == 0) && tmp>1) {	/*while the remainder is even and greater than 1*/
			tmp /= 2;		/*divide by 2*/
		}

		if(tmp != 1) {			/*when loop is donw, if the remainder is 1, then it was not a power of 2*/
			flag = size;
		}
		size--;
	}
	return flag;
}

void error(int x, char c[])
{
	char* e = "\n** Error - ";
	switch(x) {
		case 1:
			printf("%s%s not a multiple of 2!\n\n", e, c);
			break;
		case 2:
			printf("%sblock size larger than main memory!\n\n", e);
			break;
		case 3:
			printf("%sblock size larger than cache size!\n\n", e);
			break;
		case 4:
			printf("%scache size not a multiple of block size!\n\n", e);
			break;
		case 5:
			printf("%sNot a Valid Option!\n\n", e);
			break;
		default:
			printf("%sUnkown Error\n", e);
			break;
	}
}



/*************************** Functions for writing/reading data and other ***************************************/


int getTag(int addr, int* params)
{
//index 0 = memory size
//index 1 = cache size
//index 2 = block size


	return (addr / params[1]);
}

int getLine(int addr, int* params)
{
	return (((addr % params[1])) / (log_2(addr) + 1));
}

int getWord(int addr, int* params)
{
	return (addr % params[1] % params[2]);
}

int log_2(int addr) 
{
	int ans = 0;
	while(addr >>= 1) ans++;
	return ans;	
}

void printOut(int w, int b, int t, int v)
{
	printf("\nWord %d of cache line %d with tag %d contains value %d\n\n", w, b, t, v);
}

void readCache(node* c, int addr, int* params, int* mem)
{
	int tag = getTag(addr, params);
	int word = getWord(addr, params);
	int line = getLine(addr, params);
	int index = line * params[2] - word;
	int i;

	if(c[line].tag == tag) {
		//cache hit
		printf("** Block in cache\n");
		printOut(word, line, tag, c[line].block[word-1]);
	} else if(c[line].tag == -1) {
		//cache miss
		printf("** Read Miss...First Load Block from Memory!\n");
		c[line].block = malloc(sizeof(int) * params[2]);
		for(i=0; i<params[2]; i++) {
			c[line].block[i] = mem[index];
			c[line].tag = tag;
			index++;
		}
		
		printOut(word, line, tag, c[line].block[word-1]);
	} else if(c[line].tag != -1 && c[line].tag != tag) {
		//write current value to memory
		//read in new bloc
		printf("** Read Miss...First Load Block from Memory!\n");
		for(i=0; i<params[2]; i++) {
			c[line].block[i] = mem[index];
			c[line].tag = tag;
			index++;
		}
		

	} else {
		error(-1, NULL);
	}
}

void writeCache(node* c, int addr, int value, int* params, int* mem)
{
	int tag = getTag(addr, params);
	int word = getWord(addr, params);
	int line = getLine(addr, params);
	int index = line * params[2] - word;
	int i;

	if(c[line].tag == tag) {
		//cache hit
		printf("** Block in cache\n");
		c[line].block[word-1] = value;
		mem[index] = value; //wrtie thrugh
		printOut(word, line, tag, c[line].block[word-1]);

	} else if(c[line].tag == -1 || c[line].tag != tag) {
		printf("** Write miss...First Load Block from Memory!\n");
		c[line].block = malloc(sizeof(int) * params[2]);
		mem[index+word-1] = value;
		for(i=0; i<params[2]; i++) {
			c[line].block[i] = mem[index];
			c[line].tag = tag;
			index++;
		}

		printOut(word, line, tag, c[line].block[word-1]);
	} else {
		error(-1, NULL);
	}
}


int main()
{
//cache
////memory pointers
	int i;
	int input = getInput();
	int* params = NULL;
	int addr = 0, value = 0;


	while(input != 4) {
		if(input == 1) {
			params = setParams();
			input = getInput();
		} else if (input == 2) {
			//read from cach
			if(params == NULL) {
				printf("** Error - Set Parameters First!\n");
				input = getInput();
			} else {
				printf("Enter Main memory address to read from:  ");
				scanf(" %d", &addr);
				readCache(cache, addr, params, memory);
				input = getInput();
			}
		} else if (input == 3) {
			//wite to cache
			if(params == NULL) {
				printf("** Error - Set Parameters First!\n");
				input = getInput();
			} else {
				printf("Enter main memory address to write to:  ");
				scanf(" %d", &addr);
				printf("Enter value to write: ");
				scanf(" %d", &value);
				writeCache(cache, addr, value, params, memory);
				input = getInput();
			}
		} else {
			error(5, NULL);
			input = getInput();
		}
	}

	for(i=0; i<params[2]; i++) {
		free(cache[i].block);
	}
		free(cache);
		free(memory);
}
