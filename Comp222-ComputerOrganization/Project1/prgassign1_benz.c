/*
 * Nmae: Joshua Benz
 * Assignment # 1
 * */

#include <stdio.h>
#include <stdlib.h>

typedef struct table {
	float cpi;	/*CPI of each type of instruction*/
	float total;	/*total instruction count*/
	float freq;	/*clock rate*/

} table;

table** inputParams();
void printPerformance(table** pTable);
void printTable(table** pTable);
float avgCPI(table** pTable);
float cpuTime(table** pTable);
float calcMIPS(table** pTable);
float getTotalInst(table** pTable);
int menu();

int classes;	/*global variable that holds number of classes*/

table** inputParams()		/*Use an array of pointers that point to structs*/
{
	float tmpFreq;
	int i;
	printf("Enter the number of instruction classes: ");
	scanf(" %d", &classes);

	table** pTable = malloc(classes * sizeof(table*));		/*allocate memory for pointers to table structs*/
	printf("Enter the frequency of the machine(MHz): ");
	scanf(" %f", &tmpFreq);
	
	for(i=0; i<classes; i++) {
		pTable[i] = malloc(sizeof(table));		/*allocate memory for table structs*/
		pTable[i]->freq = tmpFreq;

		printf("Enter CPI of class %d: ", i+1);
		scanf(" %f", &(pTable[i]->cpi));		/*Get and store the data*/

		printf("Enter instruction count of class %d (millions): ", i+1);
		scanf(" %f", &(pTable[i])->total);
		printf("\n");
	}
	return pTable;
	
}

void printTable(table** pTable)
{
	int i;
	printf("-------------------------\n");
	printf("|Class\t|CPI\t|Count\t|\n");
	printf("-------------------------\n");
	for(i=0; i<classes; i++) {
		printf("|%d\t|%d\t|%d\t|\n", i+1, (int)pTable[i]->cpi, (int)pTable[i]->total);
		printf("-------------------------\n\n");
	}
}

void printPerformance(table** pTable)
{
	printf("-------------------------\n");
	printf("|Performance\t|Value\t|\n");
	printf("-------------------------\n");
	printf("|Average CPI\t|%.2f\t|\n", avgCPI(pTable));
	printf("-------------------------\n");
	printf("|CPU Time (ms)\t|%.2f\t|\n", cpuTime(pTable));
	printf("-------------------------\n");
	printf("|MIPS\t\t|%.2f\t|\n", calcMIPS(pTable));
	printf("-------------------------\n\n");
}

float avgCPI(table** pTable) 
{
	float sum = 0.0;
	int i;
	for(i=0; i<classes; i++) 
		sum += (pTable[i]->cpi * pTable[i]->total);

	return (sum / getTotalInst(pTable));
	
}

float getTotalInst(table** pTable) 
{
	float sum = 0.0;
	int i;
	for(i=0; i<classes; i++) 
		sum += pTable[i]->total;

	return sum;
	
}

float cpuTime(table** pTable)
{
	return((avgCPI(pTable) * getTotalInst(pTable) * 1000.0) / (pTable[0]->freq));
							//1000.0 to convert to ms
}

float calcMIPS(table** pTable)
{
	return (pTable[0]->freq / avgCPI(pTable));
}

int menu()
{
	int option;
	
	printf("Joshua Benz\nPerformance Assessment: \n");
	printf("-----------------------\n");
	
	printf("1) Enter Parameters\n");
	printf("2) Print table of parameters\n");
	printf("3) Print table of performance\n");
	printf("4) Quit\n\n");
	printf("Enter Selection: ");

	if(scanf(" %d", &option) != 1) {
		printf("Please choose a valid option!\n\n");
		option = -1;
	}
	return option;
}

int main()
{
	table** pTable;
	int input = menu();

	while(input != 4) {
		if(input == 1) {
			pTable = inputParams();
			input = 0;
		} else if(input == 2) {
			printTable(pTable);
			input = 0;
		} else if(input == 3) {
			if(pTable != NULL) {
				printPerformance(pTable);
				input = 0;
			} else {
				printf("Input parameters first!\n");
				input = menu();
			}
		} else 
			input = menu();
	}

	return 0;
}
