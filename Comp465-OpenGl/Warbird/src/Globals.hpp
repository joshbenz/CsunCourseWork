#ifndef GLOBALS_HPP
#define GLOBALS_HPP

#ifndef __INCLUDES465__
#include "../includes465/include465.hpp"
#define __INCLUDES465__
#endif
// For the global Constants
#include <string>

const int nModels = 11;
const int nPlanets = 5;
const int nCameras = 5;
int movementDirection = 0;
//int timerDelay = 40;
//int frameCount = 0;
//double lastTime, elapsedTime, currentTime;

char* vertexShaderFile = "simpleVertex.glsl";
char* fragmentShaderFile = "simpleFragment.glsl";


int timeQuantum[4] = {5, 40, 100, 500}; // ace, pilot, training, debug
//std::string titleString = "Warbird Simulator: {q, v, x} ";
//std::string warbirdMissiles = "Warbird: 9 ";
//std::string unumMissiles = "Unum: 5 ";
//std::string secundusMissiles = "Secundus: 5 ";
//std::string ups;
//std::string fps;
//std::string view = "View: ";
//std::string Views[nCameras] = {"Front ", "Top ", "Warbird ","Unum ", "Duo "};
//std::string timerStr[2] = {" U/S 200 ", "  U/S 25 "}; 

std::string timeQState[4] = {"Ace", "Pilot", "Trainee", "Debug"};
int currTimeQ = 0;
int nTimeQ = 4;


glm::mat4 squareRotation = glm::mat4();
glm::mat4 translateSquare = glm::mat4();

const int numberOfSquares = 6;
float squareRotationAmount = 0.0f;	// default rotation amount

/*The order the vertices will be drawn*/
static const unsigned int indices[] = {
	0, 1, 2,
	2, 3, 0
};
GLuint textIBO;
GLuint textBuf;
GLuint textVao;

GLuint TexturePosition;
GLuint vTextCoord;
GLuint IsTexture;
GLuint texture;
GLuint Texture;
glm::mat4 modelViewMatrix;
GLuint NormalMatrix;
GLuint ModelViewMatrix;
glm::mat3 normalMatrix;
glm::mat4 modelViewProjectionMatrix;

/*The texture coordinates and order */
static const GLfloat textCoords[] = {
	0.0f, 1.0f,
	1.0f, 1.0f,
	1.0f, 0.0f,
	0.0f, 0.0f,
};

/*Locations of the vertices*/
static const GLfloat squareVertices[16] = {
	-50000.0f, -50000.0f, 0.0f, 1.0f,	// BL
	50000.0f, -50000.0f, 0.0f, 1.0f,	// BR
	50000.0f, 50000.0f, 0.0f, 1.0f,		// TR
	-50000.0f, 50000.0f, 0.0f, 1.0f		// TL
};

/* Locations of each plane */
glm::vec3 squareTranslationAmounts[6] = {
	glm::vec3(0.0f, 0.0f, -50000.0f),
	glm::vec3(0.0f, 0.0f, 50000.0f),
	glm::vec3(0.0f, -50000.0f, 0.0f),
	glm::vec3(0.0f, 50000.0f, 0.0f),
	glm::vec3(-50000.0f, 0.0f, 0.0f),
	glm::vec3(50000.0f, 0.0f, 0.0f)
};

/* What axis to rotate each plane on */
glm::vec3 squareRotationAxis[6] = {
	glm::vec3(0.0f, 1.0f, 0.0f),
	glm::vec3(0.0f, 1.0f, 0.0f),
	glm::vec3(1.0f, 0.0f, 0.0f),
	glm::vec3(1.0f, 0.0f, 0.0f),
	glm::vec3(0.0f, 1.0f, 0.0f),
	glm::vec3(0.0f, 1.0f, 0.0f),

};

//Ruber point light
GLuint AmbientOn;
bool ambientOn = true;
GLuint Ruber;

GLuint PointLightPosition;
GLuint EyeDirection;
GLuint PointLightOn;
glm::vec3 pointLightPosition = glm::vec3(0, 0, 0);
glm::vec3 eyeDirection = glm::vec3(0.0, 0.0, 0.0);
bool pointLightOn = true;

//headlamp
GLuint LightDirection;
GLuint HeadLampLightOn;
glm::vec3 direction = glm::vec3(0.0, 0.0, 1.0);
bool headLampLightOn = true;

//spotlight
GLuint SpotLightOn;
bool spotLightOn = true;

GLuint LightColor;
glm::vec3 lightColor = glm::vec3(1.0, 1.0, 1.0);
GLuint ConstantAttentuation;
float constantAttentuation = 1.0f;
GLuint LinearAttenuation;
float linearAttenuation = 1.0f;
GLuint QuadraticAttenuation;
float quadraticAttenuation = 1.0f;
GLuint Shininess;
float shininess = 1.0f;
GLuint Strength;
float strength = 1.0f;

enum timeQuantumState {
	ACE,
    PILOT,
    TRAINEE,
    DEBUG
};

enum modelIndex {
	RUBER,
	UNUMN,
	DUO,
	PRIMUS,
	SECUNDUS,
	WARBIRD,
     UNUMN_MISSILESILO,
     SECUNDUS_MISSILESILO,
	WARBIRD_MISSILE,
    UNUMN_MISSILE,
    SECUNDUS_MISSILE
};

char* modelFiles[nModels] = {
	"Ruber.tri",
	"Primus.tri",
	"Primus.tri",
	"Moon.tri",
	"Moon.tri",
	"Warbird.tri",
    "MissileSilo.tri",
    "MissileSilo.tri",
	"Missile.tri",
	"Missile.tri",
	"Missile.tri"
};

int nVertices[nModels] = {
	264 * 3,
	264 * 3,
	264 * 3,
	264 * 3,
	264 * 3,
	996 * 3,
    54  * 3,
    54  * 3,
	282 * 3,
	282 * 3,
	282 * 3
};

float modelSize[nModels] {
	2000.0f,
	200.0f,
	400.0f,
	100.0f,
	150.0f,
	100.0f,
    120.0f,
    120.0f,
	25.0f,	
	25.0f,	
	25.0f	
};

glm::vec3 modelPositions[nModels] = { 
	glm::vec3(0,		0,	0), //ruber
	glm::vec3(4000, 	0, 	0), //unum
	glm::vec3(9000, 	0, 	0), //duo
	glm::vec3(11000, 	0, 	0), //primus
	glm::vec3(13000,	0,	0), //secundus
	glm::vec3(15000, 	0, 	0), //warbird
	glm::vec3(0, 	0, 	0), //silo
	glm::vec3(0, 	0, 	0), //silo
	glm::vec3(0, 	0, 	0), //missle
	glm::vec3(0, 	0, 	0), //missle
	glm::vec3(0, 	0, 	0) //missle
}; 

float rotationValues[nModels] {
	0.0f, //Ruber
	0.004f,
	0.002f,
	0.002f,		//planets
	0.004f,
	0.0f, //Warbird
	0.0f, //silo
	0.0f, //silo
	0.0f, //missile
	0.0f, //missile
	0.0f //missile
};
#endif
