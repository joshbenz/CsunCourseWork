#define __Linux__
#include "../includes465/include465.hpp"
#include "Model3D.hpp"
#include "Globals.hpp"
#include "CameraManager.hpp"
#include "Timer.hpp"
#include "TitleManager.hpp"
#include "Warbird.hpp"
#include "Planet.hpp"
#include "Missile.hpp"
#include "MissileSilo.hpp"
#include <iostream>
#include <vector>
#include <string>

float modelBR[nModels];

GLuint shaderProgram, MVP;
GLuint VAO[nModels];
GLuint VBO[nModels];
GLuint vPosition[nModels], vColor[nModels], vNormal[nModels];

glm::mat4 projectionMatrix;

CameraManager* cameraManager;
Timer* timer;
TitleManager* titleManager;
std::vector<Planet*> planets;
Warbird* warbird;
MissileSilo* unumnMissileSilo, *secundusMissileSilo;


void updateTitleWindow() {
    titleManager->update(warbird->getNumMissiles(), unumnMissileSilo->getNumMissiles(),
            secundusMissileSilo->getNumMissiles());
	titleManager->setView(cameraManager->getCameraIndex());
	std::string title = titleManager->toString();
	glutSetWindowTitle(title.c_str());
}

void init() {
	titleManager = new TitleManager();

	shaderProgram = loadShaders(vertexShaderFile, fragmentShaderFile);
	glUseProgram(shaderProgram);

	glGenVertexArrays(nModels, VAO);
	glGenBuffers(nModels, VBO);

	//load all of the models and check for errors
	for(int i=0; i<nModels; i++) {
		  modelBR[i] = loadModelBuffer(modelFiles[i], nVertices[i], VAO[i], VBO[i], shaderProgram,
					vPosition[i], vColor[i], vNormal[i], "vPosition", "vColor", "vNormal");

		if(modelBR[i] == -1.0f) {
			std::cout << modelFiles[i] << " Failed!\n";
		} else {
			std::cout << "Loaded " << modelFiles[i] << "\n";
		}
	}

	cameraManager = new CameraManager();

//glsl handles	
    MVP = glGetUniformLocation(shaderProgram, "ModelViewProjection");
    ModelViewMatrix = glGetUniformLocation(shaderProgram, "ModelViewMatrix");
	NormalMatrix = glGetUniformLocation(shaderProgram, "NormalMatrix");

	glEnable(GL_DEPTH_TEST);
	glClearColor(0.7f, 0.7f, 0.7f, 1.0f);

  for(int i=0; i<nPlanets; i++) {
    if(i == RUBER) {
        planets.push_back(new Planet(modelSize[i], modelBR[i], 0.8f, 5000.0f, false));
    } else {
        planets.push_back(new Planet(modelSize[i], modelBR[i], 0.0f, 0.0f, false));
        planets[i]->translateModel(modelPositions[i]);
        planets[i]->setRotationValue(rotationValues[i]);
        if(i == UNUMN || i == DUO) planets[i]->toggleOrbital();
    }
  }  

  int nMissilesForSilo = 5;
  unumnMissileSilo = new MissileSilo(modelSize[UNUMN_MISSILESILO], modelBR[UNUMN_MISSILESILO], nMissilesForSilo, planets[UNUMN]);
  unumnMissileSilo->setSurface(150);
    Missile* mi = new Missile(modelSize[UNUMN_MISSILE], modelBR[UNUMN_MISSILE], 25.0f);
    mi->translateModel(getPosition(unumnMissileSilo->getTranslationMatrix()));
    mi->setRotationValue(rotationValues[UNUMN_MISSILESILO]);
    mi->setRotationMatrix(unumnMissileSilo->getRotationMatrix());
    mi->setOrientationMatrix(unumnMissileSilo->getOrientationMatrix());
    unumnMissileSilo->setMissile(mi);

  secundusMissileSilo = new MissileSilo(modelSize[SECUNDUS_MISSILESILO], modelBR[SECUNDUS_MISSILESILO], nMissilesForSilo, planets[DUO]);//planets[SECUNDUS]);
  secundusMissileSilo->setSurface(400);
    Missile* ms = new Missile(modelSize[SECUNDUS_MISSILE], modelBR[SECUNDUS_MISSILE], 25.0f);
    ms->translateModel(getPosition(secundusMissileSilo->getTranslationMatrix()));
    ms->setRotationValue(rotationValues[SECUNDUS_MISSILESILO]);
    ms->setRotationMatrix(secundusMissileSilo->getRotationMatrix());
    ms->setOrientationMatrix(secundusMissileSilo->getOrientationMatrix());
    secundusMissileSilo->setMissile(ms);
  

  warbird = new Warbird(modelSize[WARBIRD], modelBR[WARBIRD], modelPositions[WARBIRD], 9);
  warbird->translateModel(modelPositions[WARBIRD]);
  warbird->setRotationValue(rotationValues[WARBIRD]);
  warbird->setCamera(cameraManager->getCamera(cameraManager->WARBIRD));
  

  //assign ship missiles 
    Missile* m = new Missile(modelSize[WARBIRD_MISSILE], modelBR[WARBIRD_MISSILE], 25.0f);
    m->translateModel(getPosition(warbird->getTranslationMatrix()));
    m->setRotationValue(rotationValues[WARBIRD_MISSILE]);
    m->setRotationMatrix(warbird->getRotationMatrix());
    m->setOrientationMatrix(warbird->getOrientationMatrix());
    warbird->setMissile(m);


  //buffers for texture
  glGenBuffers(1, &textIBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, textIBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    glGenVertexArrays(1, &textVao);
	glBindVertexArray(textVao);

    glGenBuffers(1, &textBuf);
	glBindBuffer(GL_ARRAY_BUFFER, textBuf);
	glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices) + sizeof(textCoords), NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(squareVertices), squareVertices);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(squareVertices), sizeof(textCoords), textCoords);

    TexturePosition = glGetAttribLocation(shaderProgram, "vPosition");
	glVertexAttribPointer(TexturePosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
	glEnableVertexAttribArray(TexturePosition);

	vTextCoord = glGetAttribLocation(shaderProgram, "vTextCoord");
	glVertexAttribPointer(vTextCoord, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(squareVertices)));
	glEnableVertexAttribArray(vTextCoord);

    IsTexture = glGetUniformLocation(shaderProgram, "IsTexture");
	glUniform1ui(IsTexture, false);

    texture = loadRawTexture(texture, "stars.raw", 640, 425);
	if (texture != 0) {
		Texture = glGetUniformLocation(shaderProgram, "Texture");
		printf("texture file, read, texture %1d generated and bound  \n", Texture);
	} else {
        printf("Texture in file %s NOT LOADED !!! \n");
    }

    //Lights
    PointLightPosition = glGetUniformLocation(shaderProgram, "PointLightPosition");
	EyeDirection = glGetUniformLocation(shaderProgram, "EyeDirection");
	PointLightOn = glGetUniformLocation(shaderProgram, "PointLightOn");

    LightDirection = glGetUniformLocation(shaderProgram, "LightDirection");
	HeadLampLightOn = glGetUniformLocation(shaderProgram, "HeadLampLightOn");

    AmbientOn = glGetUniformLocation(shaderProgram, "AmbientOn");
	Ruber = glGetUniformLocation(shaderProgram, "Ruber");
	LightColor = glGetUniformLocation(shaderProgram, "LightColor");
	ConstantAttentuation = glGetUniformLocation(shaderProgram, "ConstantAttenuation");
	LinearAttenuation = glGetUniformLocation(shaderProgram, "LinearAttenuation");
	QuadraticAttenuation = glGetUniformLocation(shaderProgram, "QuadraticAttenuation");
	shininess = glGetUniformLocation(shaderProgram, "Shininess");
	strength = glGetUniformLocation(shaderProgram, "Strength");

    glUniform3f(EyeDirection, eyeDirection.x, eyeDirection.y, eyeDirection.z);
	glUniform1ui(PointLightOn, pointLightOn);

	glUniform3f(LightDirection, direction.x, direction.y, direction.z);
	glUniform1ui(HeadLampLightOn, headLampLightOn);

	glUniform1ui(AmbientOn, ambientOn);
	glUniform3f(LightColor, lightColor.x, lightColor.y, lightColor.z);
	glUniform1ui(ConstantAttentuation, constantAttentuation);
	glUniform1ui(LinearAttenuation, linearAttenuation);
	glUniform1ui(QuadraticAttenuation, quadraticAttenuation);
	glUniform1ui(Shininess, shininess);
	glUniform1ui(Strength, strength);

	timer = new Timer(timeQuantum[currTimeQ]);
	timer->start();

}

void reshape(int width, int height) {
	float aspectRatio = (float)width / (float)height;
	float FOVY = glm::radians(60.0f);

	glViewport(0, 0, width, height);
	projectionMatrix = glm::perspective(FOVY, aspectRatio, 1.0f, 100000.0f);
	printf("reshape: FOVY = %5.2f, width = %4d height = %4d aspect = %5.2f \n",
	FOVY, width, height, aspectRatio);
}

void display() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    pointLightPosition = getPosition(planets[RUBER]->getOrientationMatrix() * cameraManager->getCurrentCamera()->getViewMatrix());
	glUniform3f(PointLightPosition, pointLightPosition.x, pointLightPosition.y, pointLightPosition.z);


  for(int i=0; i<nModels; i++) {
		switch(i) {
			case UNUMN:
				cameraManager->followPlanet(cameraManager->UNUMN, planets[UNUMN]->getOrientationMatrix());
				break;

			case DUO:
				cameraManager->followPlanet(cameraManager->DUO, planets[DUO]->getOrientationMatrix());
				break;

			case PRIMUS://orbit around duo
				planets[PRIMUS]->setOrientationMatrix(planets[DUO]->getOrientationMatrix() *
						planets[PRIMUS]->getRotationMatrix() *
						glm::translate(glm::mat4(1.0f), (modelPositions[PRIMUS] - modelPositions[DUO])));
				break;

			case SECUNDUS://orbit around duo
				planets[SECUNDUS]->setOrientationMatrix(planets[DUO]->getOrientationMatrix() *
						planets[SECUNDUS]->getRotationMatrix() *
						glm::translate(glm::mat4(1.0f), (modelPositions[SECUNDUS] - modelPositions[DUO])));
				break;

            case WARBIRD:
                warbird->updateCamera();
		        modelViewProjectionMatrix  = projectionMatrix * cameraManager->getCurrentCamera()->getViewMatrix() * warbird->getModelMatrix();
             break;
            
            case UNUMN_MISSILESILO:
		        modelViewProjectionMatrix  = projectionMatrix * cameraManager->getCurrentCamera()->getViewMatrix() * unumnMissileSilo->getModelMatrix();
            break;

            case SECUNDUS_MISSILESILO:
		        modelViewProjectionMatrix  = projectionMatrix * cameraManager->getCurrentCamera()->getViewMatrix() * secundusMissileSilo->getModelMatrix();
            break;

            case WARBIRD_MISSILE:
                warbird->getMissile()->setTranslationMatrix(warbird->getTranslationMatrix());
                warbird->getMissile()->setRotationMatrix(warbird->getRotationMatrix());
                warbird->getMissile()->setOrientationMatrix(warbird->getOrientationMatrix());
                break;
            
            case UNUMN_MISSILE:
                unumnMissileSilo->getMissile()->setTranslationMatrix(unumnMissileSilo->getTranslationMatrix());
                unumnMissileSilo->getMissile()->setRotationMatrix(unumnMissileSilo->getRotationMatrix());
                unumnMissileSilo->getMissile()->setOrientationMatrix(unumnMissileSilo->getOrientationMatrix());
                break;

            case SECUNDUS_MISSILE:
                secundusMissileSilo->getMissile()->setTranslationMatrix(secundusMissileSilo->getTranslationMatrix());
                secundusMissileSilo->getMissile()->setRotationMatrix(secundusMissileSilo->getRotationMatrix());
                secundusMissileSilo->getMissile()->setOrientationMatrix(secundusMissileSilo->getOrientationMatrix());
                break;

			default: break;
		}

    if(i < nPlanets) {
		  modelViewProjectionMatrix  = projectionMatrix * cameraManager->getCurrentCamera()->getViewMatrix() * planets[i]->getModelMatrix();
    } 

		glUniformMatrix4fv(MVP, 1, GL_FALSE, glm::value_ptr(modelViewProjectionMatrix));
		glBindVertexArray(VAO[i]);
		  glDrawArrays(GL_TRIANGLES, 0, nVertices[i]);
  
  }

  glUniform1ui(IsTexture, true);

  glm::mat4 modelMatrix, modelViewMatrix, viewMatrix;
		for (int i = 0; i < numberOfSquares; i++) {
			if (i > 1) {
				squareRotationAmount = PI / 2;
			}
			else {
				squareRotationAmount = 0.0f;
			}
            glm::mat4 modelViewMatrix = viewMatrix * glm::translate(translateSquare, squareTranslationAmounts[i])
				* glm::rotate(squareRotation, squareRotationAmount, squareRotationAxis[i]);

			glUniformMatrix4fv(ModelViewMatrix, 1, GL_FALSE, glm::value_ptr(modelViewMatrix));
			normalMatrix = glm::mat3(modelViewMatrix);
			glUniformMatrix3fv(NormalMatrix, 1, GL_FALSE, glm::value_ptr(normalMatrix));
			modelViewProjectionMatrix = projectionMatrix * modelViewMatrix;
			glUniformMatrix4fv(MVP, 1, GL_FALSE, glm::value_ptr(modelViewProjectionMatrix));
			glBindVertexArray(textVao);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, textIBO);
			glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, BUFFER_OFFSET(0));
		}

		glUniform1ui(IsTexture, false);

	glutSwapBuffers();

	timer->incrementFPS();
	timer->reset();
	titleManager->setFPS(timer->getFPS());
	updateTitleWindow();
}


void SceneCollisionCheck() {
    glm::vec3 objectPos = getPosition(warbird->getOrientationMatrix());

    if(warbird->isAlive()) {
        for(int i=0; i<planets.size(); i++) {
            float distanceFromPlanets = distance(objectPos, getPosition(planets[i]->getOrientationMatrix()));
            if(distanceFromPlanets < modelBR[WARBIRD] + 10.0f + modelSize[i]) {
                warbird->destroy(); //crashed into a planet
                cameraManager->setMainCamera(0);
            }
        }

        //hit by your own missile
        if(warbird->getMissile()->isSmart()) {
            float distanceFromMissile = distance(objectPos, getPosition(warbird->getMissile()->getOrientationMatrix()));
            if(distanceFromMissile < modelBR[WARBIRD] + 10.0f + modelBR[WARBIRD_MISSILE]) {
                warbird->destroy(); //crashed into a missile
                cameraManager->setMainCamera(0);
        }
    }
        //hit by other missiles
        if(unumnMissileSilo->getMissile()->isSmart()) {
            float distanceFromMissile = distance(objectPos, getPosition(unumnMissileSilo->getMissile()->getOrientationMatrix()));
            if(distanceFromMissile < modelBR[WARBIRD] + 10.0f + modelBR[UNUMN_MISSILE]) {
                warbird->destroy(); //crashed into a missile
                cameraManager->setMainCamera(0);
            }
        }

        if(secundusMissileSilo->getMissile()->isSmart()) {
            float distanceFromMissile = distance(objectPos, getPosition(secundusMissileSilo->getMissile()->getOrientationMatrix()));
            if(distanceFromMissile < modelBR[WARBIRD] + 10.0f + modelBR[SECUNDUS_MISSILE]) {
                warbird->destroy(); //crashed into a missile
                cameraManager->setMainCamera(0);
            }
        }
    }

    //warbird missile destroys missile silos
    objectPos = getPosition(unumnMissileSilo->getOrientationMatrix());
    if(unumnMissileSilo->isAlive()) {
        float distanceFromMissile = distance(objectPos, getPosition(warbird->getMissile()->getOrientationMatrix()));
        if(distanceFromMissile < modelBR[UNUMN_MISSILESILO] + 10.0f + modelBR[WARBIRD_MISSILE]) {
            unumnMissileSilo->destroy();
            warbird->getMissile()->destroy();
        }
    }


    objectPos = getPosition(secundusMissileSilo->getOrientationMatrix());
    if(secundusMissileSilo->isAlive()) {
        float distanceFromMissile = distance(objectPos, getPosition(warbird->getMissile()->getOrientationMatrix()));
        if(distanceFromMissile < modelBR[SECUNDUS_MISSILESILO] + 10.0f + modelBR[WARBIRD_MISSILE]) {
            secundusMissileSilo->destroy();
            warbird->getMissile()->destroy();
        }
    }
}


void update(int i) {
	glutTimerFunc(timer->getTimeDelay(), update, 1);
	for(int i = 0; i < nPlanets; i++) planets[i]->update();
  warbird->update();
  unumnMissileSilo->update();
  secundusMissileSilo->update();

  warbird->missileLogic(unumnMissileSilo, secundusMissileSilo);
  unumnMissileSilo->missileLogic(warbird->getOrientationMatrix());
  secundusMissileSilo->missileLogic(warbird->getOrientationMatrix());

  SceneCollisionCheck();
  
    bool gravity = false;

  for(int i=0; i<planets.size(); i++) {
        if(planets[i]->getGravityState()) {
            glm::vec3 shipPos = getPosition(warbird->getOrientationMatrix());
            glm::vec3 vecPoint = getPosition(planets[i]->getOrientationMatrix()) - shipPos;
            float distanceToPlanet = glm::length(vecPoint);

            if(distanceToPlanet < planets[i]->getGravityField()) {
                glm::vec3 gravity = (vecPoint / distanceToPlanet);
                warbird->setTranslationMatrix(glm::translate(warbird->getTranslationMatrix(),
                            gravity * glm::vec3(0.8f, 0.8f, 0.8f)));
            }
        }
  }

  if(!unumnMissileSilo->isAlive() && !secundusMissileSilo->isAlive()) {
        //win
  }

  if(!warbird->isAlive()) {
    //lose
  }

  glutPostRedisplay();

}

void gravitySwitch() {
    planets[RUBER]->toggleGravity();
}



void keyboard(unsigned char key, int x, int y) {
	switch (key) {
		case 033: case 'q':  case 'Q': exit(EXIT_SUCCESS); break;

		case 'v': case 'V':
			cameraManager->nextCamera();
			break;

		case 'x': case 'X':
			cameraManager->prevCamera();
			break;
        
        case 'f': case 'F':
            warbird->fireMissile();
            break;

        case 't': case 'T':
            currTimeQ = (currTimeQ + 1) % nTimeQ;
            timer->setTimeDelay(timeQuantum[currTimeQ]);
            break;

        case 'r': case 'R' :
            warbird->reset();
            unumnMissileSilo->reset();
            secundusMissileSilo->reset();
            break;

        case 's': case 'S':
            warbird->nextSpeed();
            break;

        case 'g': case 'G':
            gravitySwitch();
            break;

        case 'w': case 'W': {
            int spots = warbird->getWarpSpots();
            int currSpot = warbird->getCurrWarpSpot();

            warbird->setCurrWarpSpot((currSpot+1) % spots);
            currSpot = warbird->getCurrWarpSpot();
            switch(currSpot) {
                case 0:
                    warbird->warp(glm::mat4(), glm::vec3(), glm::mat4());
                    break;
                case 1:
                    warbird->warp(planets[UNUMN]->getTranslationMatrix(),
                            cameraManager->getCamera(cameraManager->UNUMN)->getEye(),
                            planets[UNUMN]->getRotationMatrix());
                    break;
                case 2:
                    warbird->warp(planets[DUO]->getTranslationMatrix(),
                            cameraManager->getCamera(cameraManager->DUO)->getEye(),
                            planets[DUO]->getRotationMatrix());
            }
        }
	}
}

void specialKeypress(int key, int x, int y) {
switch (key)
	{
	case GLUT_KEY_UP:
		if (glutGetModifiers() == GLUT_ACTIVE_CTRL)
			warbird->setPitch(1);
		else 
			warbird->setDirection(-1);
		break;

	case GLUT_KEY_DOWN:
		if (glutGetModifiers() == GLUT_ACTIVE_CTRL)
			warbird->setPitch(-1);
		else 
			warbird->setDirection(1);
		break;

	case GLUT_KEY_LEFT:
		if (glutGetModifiers() == GLUT_ACTIVE_CTRL)
			warbird->setRoll(1);
		else 
			warbird->setYaw(1);
		break;

	case GLUT_KEY_RIGHT: 
		if (glutGetModifiers() == GLUT_ACTIVE_CTRL)
			warbird->setRoll(-1);
		else 
			warbird->setYaw(-1);
		break;
	}
}



int main(int argc, char* argv[]) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(800, 600);

	glutInitContextVersion(3, 3);
	glutInitContextProfile(GLUT_CORE_PROFILE);
	glutCreateWindow("Warbird Simulator: {q, x, v, t, s, f, g, w, r} : front view");
  // initialize and verify glew
	glewExperimental = GL_TRUE;  // needed my home system
	GLenum err = glewInit();

	if (GLEW_OK != err)
      printf("GLEW Error: %s \n", glewGetErrorString(err));
    else {
		printf("Using GLEW %s \n", glewGetString(GLEW_VERSION));
      	printf("OpenGL %s, GLSL %s\n",
        	glGetString(GL_VERSION),
        	glGetString(GL_SHADING_LANGUAGE_VERSION));
	}
  // initialize scene
  init();
  // set glut callback functions
  glutDisplayFunc(display);
  glutReshapeFunc(reshape);
  glutKeyboardFunc(keyboard);
  glutSpecialFunc(specialKeypress);
  glutTimerFunc(timer->getTimeDelay(), update, 1);
  glutIdleFunc(display);
  glutMainLoop();
  printf("done\n");
  return 0;
  }
