#ifndef CAMERA_HPP
#define CAMERA_HPP

#ifndef __INCLUDES465__
#include "../includes465/include465.hpp"
#define __INCLUDES465__
#endif

class Camera {
	private:
		glm::vec3 planetEye = glm::vec3(-4000, 0, -4000);
		glm::mat4 viewMatrix;
		glm::vec3 Eye, At, Up;

	public:
		Camera(glm::vec3 eye, glm::vec3 at, glm::vec3 up):Eye(eye), At(at), Up(up) {
			viewMatrix = glm::lookAt(
					eye,	//eye position 
					at, 	//look at position
					up);	//up vector
		}

		Camera(glm::mat4 m):viewMatrix(m) {}

		glm::mat4 getViewMatrix() 	{ return viewMatrix; 	}
		glm::vec3 getEye()			{ return Eye; 			}
		glm::vec3 getAt()			{ return At;		 	}
		glm::vec3 getUp()			{ return Up; 		    }

		void setCamera(glm::vec3 e, glm::vec3 a, glm::vec3 u) {
			Eye = e;
			At = a;
			Up = u;

			viewMatrix = glm::lookAt(Eye, At, Up);
		}

		void setViewMatrix(glm::mat4 m) { viewMatrix = m; }
		glm::vec3 getPlanetEye() { return planetEye; }
        void setEye(glm::vec3 e) { Eye = e; }

};

#endif
