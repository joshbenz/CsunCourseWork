#ifndef MODEL_HPP
#define MODEL_HPP

#ifndef __INCLUDES465__
#include "../includes465/include465.hpp"
#define __INCLUDES465__
#endif

class Model3D {
	protected:
		glm::mat4 rotationMatrix;
		glm::mat4 scaleMatrix;
		glm::mat4 translationMatrix;
		glm::mat4 orientationMatrix;
		glm::vec3 scale;
		glm::vec3 rotationAxis;

		float rotationValue;
		float modelSize;
		float boundingRadius;
		bool orbital = false;

	public:
		Model3D(float size, float br):modelSize(size), boundingRadius(br) {
			rotationMatrix 		  = glm::mat4();
			translationMatrix 	= glm::mat4();
			orientationMatrix	  = glm::mat4();

			scale = glm::vec3(size / br);
			scaleMatrix = glm::scale(glm::mat4(), glm::vec3(scale));

			rotationAxis = glm::vec3(0, 1, 0);
		}

		glm::mat4 getRotationMatrix() 		{ return rotationMatrix; 	  }
		glm::mat4 getTranslationMatrix()	{ return translationMatrix; }
		glm::mat4 getOrientationMatrix()	{ return orientationMatrix; }
        glm::mat4 getScaleMatrix()        { return scaleMatrix;       }
		float getRotationValue()			    { return rotationValue; 	  }
		
		glm::mat4 getModelMatrix() { 
			return orientationMatrix * scaleMatrix;
		}
		void setTranslationMatrix(glm::mat4 m) 	{ translationMatrix 	= m;  }
		void setRotationMatrix(glm::mat4 m)		  { rotationMatrix 		  = m;  }
		void setRotationValue(float r )			    { rotationValue 		  = r;  }
		void setOrientationMatrix(glm::mat4 o)	{ orientationMatrix 	= o;  }
		void toggleOrbital()					          { orbital = !orbital;	 	    }
		void translateModel(glm::vec3 v) {
			translationMatrix = glm::translate(translationMatrix, v);
		}

		void update() {
			rotationMatrix = glm::rotate(rotationMatrix, rotationValue, rotationAxis);

			if(orbital) orientationMatrix = rotationMatrix * translationMatrix;
			else orientationMatrix = translationMatrix * rotationMatrix;
		}
};


#endif
