#ifndef PLANET_HPP
#define PLANET_HPP
 
#ifndef __INCLUDES465__
#include "../includes465/include465.hpp"
#define __INCLUDES465__
#endif

class Planet : public Model3D {
  private:
    float gravityVector, gravityField;
    bool gravityState;
    const float gravity = 90000000.0f; 

  public:
    Planet(float size, float br, float gv, float gf, bool g) : Model3D(size, br) {
      gravityVector = gv;
      gravityField = gf;
      gravityState = g;
    }

    void toggleGravity() { gravityState = !gravityState; }
    float getGravityField() { return gravityField; }
    bool getGravityState()  { return gravityState; }

};
#endif
