#ifndef MISSILE_HPP
#define MISSILE_HPP

#ifndef __INCLUDES465__
#include "../includes465/include465.hpp"
#define __INCUDES465__
#endif


class Missile : public Model3D {
  private:
    float speed, axisDirection;
    int framecount;
    bool smart, targeted, fired;

    glm::vec3 deltaTranslation, direction, axis,
      targetVec, distance, missileVec;
    glm::mat4 target, location;

    const int lifetime = 2000;
    const int activation = 200;
    const int detectionRadius = 3000;



  public:
    Missile(float size, float br, float speed) : Model3D(size, br) {
      smart = fired = targeted = false;
      framecount = 0;
    }

    int getFramecount()       { return framecount; }
    glm::vec3 getTargetVec()  { return targetVec;  }
    glm::vec3 getDirection()  { return direction;  }
    float getSpeed()          { return speed;      }
    float getDetectionRadius(){ return detectionRadius;}

    bool isSmart()    { return smart;     }
    bool isTargeted() { return targeted;  }
    bool isFired()    { return fired;     }

    void smartActivation()  { smart = true; }
    int getActivation()     { return activation; }

    void setDirection(glm::vec3 d)  { direction = d;  }
    glm::mat4 getTarget()           { return target;  }

    void setTarget(glm::mat4 loc) { 
      target = loc;
      targeted = true;
    }

    void reset() {
        targeted = fired = smart = false;
    }
    void destroy() {
      smart = targeted = fired = false;
      framecount = 0;
      translationMatrix = orientationMatrix = glm::mat4();
    }

    void fire() { 
        fired = true; 
        
    }



    void update() {
      if(fired) {
        framecount++; //start moving if fired
        distance = glm::vec3(0, 0, -speed);
        translationMatrix = glm::translate(glm::mat4(), distance);

        if(framecount < lifetime) destroy();

        if(smart && targeted) {
          targetVec = getPosition(target) - getPosition(orientationMatrix);
          missileVec = getIn(orientationMatrix); //direction of missile

          targetVec = glm::normalize(targetVec);
          missileVec = glm::normalize(missileVec);

          //if they are colinear then we dont need to update the rotation
          if(!(colinear(missileVec, targetVec, 0.1))) {
            axis = glm::cross(missileVec, targetVec);
            axis = glm::normalize(axis);
            axisDirection = axis.x + axis.y + axis.z;

            if(axisDirection <= 0) {
              rotationValue = -glm::acos(glm::dot(targetVec, missileVec));
            } else {
              rotationValue = 2 * PI + glm::acos(glm::dot(targetVec, missileVec));
            }

            rotationAxis = axis;
            glm::quat quaternion = glm::angleAxis(rotationValue, rotationAxis);
            rotationAxis = glm::axis(quaternion);
            rotationValue = glm::angle(quaternion);
            rotationMatrix = glm::rotate(glm::mat4(), rotationValue, rotationAxis);
          }
        }
      }

      orientationMatrix = orientationMatrix * translationMatrix * rotationMatrix;
    }
};

#endif
