# version 330 core

uniform vec3 PointLightPosition;
uniform vec3 EyeDirection;
uniform bool PointLightOn;

uniform vec3 LightDirection; // Direction toward the light.
uniform bool HeadLampLightOn; 

uniform bool SpotLightOn;
uniform vec3 ConeDirection; // adding spotlight attributes
uniform float SpotCosCutoff; 
uniform float SpotExponent; 

uniform vec3 LightColor; 
uniform float ConstantAttenuation;
uniform float LinearAttenuation;
uniform float QuadraticAttenuation;
uniform float Shininess; 
uniform float Strength; 
uniform bool AmbientOn; 
uniform bool Ruber;

uniform sampler2D Texture;
uniform bool IsTexture;

in vec4 Color;
in vec3 Position;
in vec3 Normal; // surface normal
in vec2 TextCoord;

out vec4 FragColor;

vec3 pointLight(vec3 LightPosition){
	
	float ambient;

	if (AmbientOn)
		ambient = 0.2f;
	else
		ambient = 0.0f;

	// find the direction and distance of the light, which changes fragment to fragment for a local light
	vec3 lightDirection = LightPosition - vec3(Position);
	float lightDistance = length(lightDirection);

	lightDirection = lightDirection / lightDistance; 

	float attenuation = 1.0 / (ConstantAttenuation +
								LinearAttenuation * lightDistance +
								QuadraticAttenuation * lightDistance
								 * lightDistance);

	// The direction of maximum highlight also changes per fragment.
	vec3 halfVector = normalize(lightDirection + EyeDirection);

	float diffuse;
	float specular;


	if (Ruber) { 
		diffuse = max(0.0, dot(-Normal, lightDirection));
	    specular = max(0.0, dot(-Normal, halfVector));
	} else {
		diffuse = max(0.0, dot(Normal, lightDirection));
		specular = max(0.0, dot(Normal, halfVector));
	}

	if(diffuse == 0.0)
		specular = 0.0;
	else
		specular = pow(specular, Shininess) * Strength;

	vec3 scatteredLight = ambient + LightColor * diffuse;
	vec3 reflectedLight = LightColor * specular * Strength;


	vec3 rgb = min(Color.rgb * scatteredLight + reflectedLight, vec3(1.0));
	
	return rgb;
}

vec3 headLampLight()
{
	float ambient;

	if (AmbientOn)
		ambient = 0.2f;
	else
		ambient = 0.0f;

	vec3 halfVector = normalize(LightDirection);

	float diffuse = max(dot(Normal, LightDirection), 0.0);
	float specular = max(dot(Normal, halfVector), 0.0);


	if (diffuse == 0)
		specular = 0.0;
	else
		specular = pow(specular, Shininess); // sharpen the highlight

	vec3 scatteredLight = ambient + LightColor * diffuse;
	vec3 reflectedLight = LightColor * specular * Strength;
	vec3 rgb = min(Color.rgb * scatteredLight + reflectedLight, vec3(1.0));

	return rgb;
}


void main() {

  if(IsTexture) {
	FragColor = texture(Texture, TextCoord);
  } else {

		vec3 tempColor = vec3(Color) * 0.1f; // initial value

		if (HeadLampLightOn)
			tempColor += headLampLight();
			
		if (PointLightOn) 
			tempColor += pointLight(PointLightPosition);
		
		FragColor = vec4(tempColor, 1.0);	
	}
}
