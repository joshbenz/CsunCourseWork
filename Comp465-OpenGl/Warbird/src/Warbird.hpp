#ifndef WARBIRD_HPP
#define WARBIRD_HPP

#ifndef __INCLUDES465__
#include "../includes/include465.hpp"
#define __INCLUDES465__
#endif
#include "Missile.hpp"
#include "MissileSilo.hpp"
class Warbird : public Model3D {
  private:

    glm::vec3 initialPosition;
    glm::vec3 dist, delta;

    bool alive;
    float speed;
    int pitch, roll, yaw;
    int step; //step 
    int nMissiles;
    int speeds[3] = {10, 50, 200};
    int currentSpeed;
    const int nSpeeds = 3;
    const int warpSpots = 3;
    int currWarpSpot = 0;

    Camera* camera;
    Missile* missile;
    MissileSilo* target;

  public:
    Warbird(float size, float br, glm::vec3 position, int n) : Model3D(size, br) {
        initialPosition = position;
        translateModel(position);
        currentSpeed = 0;
        speed = speeds[currentSpeed];
        pitch = roll = yaw = step = 0;
        alive = true;
        nMissiles = n;
    }

    Missile* getMissile()  { return missile;    }
    void setMissile(Missile* m) { missile = m; }
    int getNumMissiles()        { return nMissiles;      }

    void setSpeed(float s)  { speed = s;          }
    void toggleIsAlive()    { alive = !alive;     }
    void setDirection(int d){ step = d;           }
    void setPitch(int p)    { pitch = p;          }
    void setYaw(int y)      { yaw = y;            }
    void setRoll(int r)     { roll = r;           }
    void setCamera(Camera* c) { camera = c;       }
    int getWarpSpots()     { return warpSpots;     }
    int getCurrWarpSpot()    { return currWarpSpot; }
    void setCurrWarpSpot(int s)   { currWarpSpot = s; }
    Camera* getCamera()       { return camera;    }
    bool isAlive()               { return alive; }

    void nextSpeed() {
        int nSpeeds = 3;
        currentSpeed = (currentSpeed + 1) % nSpeeds;
        speed = speeds[currentSpeed];
    }


    void warp(glm::mat4 pos, glm::vec3 eye, glm::mat4 rot) {

        if(currWarpSpot == 0) {
            translationMatrix = glm::translate(glm::mat4(), modelPositions[WARBIRD]);
            rotationMatrix = glm::rotate(glm::mat4(), 0.0f, glm::vec3(0, 1, 0));
        } else {
            translationMatrix = glm::translate(glm::mat4(), getPosition(glm::translate(pos, eye)));
            rotationMatrix = glm::rotate(rot, PI, glm::vec3(0, 1, 0));
        }
    }

    void destroy() {
        orientationMatrix = glm::mat4(); //identity, just hide the ship in the sun :)
        alive = false;
    }

    void reset() {
        alive = true;
        translationMatrix = glm::translate(glm::mat4(), initialPosition); 
		rotationMatrix = glm::rotate(glm::mat4(), 0.0f, glm::vec3(0, 1, 0));//point it back towards the center
        nMissiles=9;
        missile->reset();
    }

    void fireMissile() {

        if(nMissiles > 0) {
            missile->fire();
            nMissiles--;
        }
    }


    void missileLogic(MissileSilo* unum, MissileSilo* duo) {
        if(missile->isFired()) {
            if(missile->getFramecount() > missile->getActivation()) {
                missile->smartActivation();
            }
        }

        if(missile->isFired()) {
            if(missile->isSmart()) {
                if(!missile->isTargeted()) {
                    float distanceFromUnum = distance(getPosition(unum->getOrientationMatrix()), 
                            getPosition(missile->getOrientationMatrix()));
                    float distanceFromDuo = distance(getPosition(duo->getOrientationMatrix()), 
                            getPosition(missile->getOrientationMatrix()));
                    
                    if(distanceFromUnum <= distanceFromDuo) {
                        missile->setTarget(unum->getOrientationMatrix());
                        target = unum;
                    } else if(distanceFromUnum > distanceFromDuo) {
                        missile->setTarget(duo->getOrientationMatrix());
                        target = duo;
                    }
                } else {
                    missile->setTarget(target->getOrientationMatrix());
                }
            }
        } else {
            missile->setOrientationMatrix(glm::translate(getOrientationMatrix(), glm::vec3(-33, 0, -30)));
        } 
        missile->update();
    } 


    void update() {
		if (!alive)
			return;

		delta = getIn(getModelMatrix()) * (-step * speed);

		rotationAxis = glm::vec3(pitch, yaw, roll);

		if ((pitch != 0) || (yaw != 0) || (roll != 0))
			rotationMatrix = glm::rotate(rotationMatrix, rotationValue, rotationAxis);


		translateModel(delta);
		orientationMatrix = translationMatrix * rotationMatrix;
		step = pitch = yaw = roll = 0;
        
/*
        if(pitch != 0)
            rotationMatrix = glm::rotate(rotationMatrix, pitch*rotationValue, glm::vec3(1,0,0));
        else if(yaw != 0)
            rotationMatrix = glm::rotate(rotationMatrix, yaw*rotationValue, glm::vec3(0,1,0));
        else if(roll != 0)
            rotationMatrix = glm::rotate(rotationMatrix, roll*rotationValue, glm::vec3(0,0,1));

        delta = getIn(getModelMatrix()) * (-step * speed);
        translationMatrix = glm::translate(translationMatrix, delta);
        step = pitch = yaw = roll = 0;
        */
    }



    void updateCamera() {
        glm::vec3 eye = getPosition(getModelMatrix()) - getIn(rotationMatrix) * 1000.0f 
                + getUp(rotationMatrix) * 200.0f;

        glm::vec3 at = getPosition(getModelMatrix() * 
                glm::translate(glm::mat4(), glm::vec3(0.0f, 300.0f, 0.0f)));

        camera->setCamera(eye, at, camera->getUp());
    }
};

#endif
