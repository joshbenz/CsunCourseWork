#ifndef TITLEMANAGER_HPP
#define TITLEMANAGER_HPP

#ifndef __INCLUDES465__
#include "../includes465/include465.hpp"
#define __INCLUDES465__
#endif

#include <string>
#include "Globals.hpp"

class TitleManager {  
	private:		
	std::string titleString = "Warbird Simulator: {q, v, x, s, t, f, g, w, r} ";
	std::string warbirdMissiles = "Warbird: ";
	std::string unumMissiles = "Unum:  ";
	std::string secundusMissiles = "Secundus: ";
	std::string ups;
	std::string fps;
	std::string view;
	std::string Views[nCameras] = {"Front ", "Top ", "Warbird ","Unum ", "Duo "};
	std::string timerStr[2] = {" U/S 200 ", "  U/S 25 "}; 
	
    public:
	TitleManager() {}
	
	void setFPS(int f) {
		fps = "F/S" + std::to_string(f);
	}

	void setView(int index) { 
		view = "View: " + Views[index]; 
	}
	std::string toString() {
		return titleString + warbirdMissiles + unumMissiles + secundusMissiles + 
			ups + timerStr[ACE] + fps + view;
	}

    void update(int warMiss, int UnumnMiss, int secundMiss) {
        warbirdMissiles = "Warbrd: " + std::string(std::to_string(warMiss));
        unumMissiles = "Unum: " + std::string(std::to_string(UnumnMiss));
        secundusMissiles = "Secundus: " + std::string(std::to_string(secundMiss));
    }
};

#endif
