#ifndef MISSILESILO_HPP
#define MISSILESILO_HPP
 
#ifndef __INCLUDES465__
#include "../includes465/include465.hpp"
#define __INCLUDES465__
#endif

#include "Missile.hpp"
#include "Planet.hpp"

class MissileSilo : public Model3D {
  private:
    bool alive;
    int nMissiles, missilesLeft;
    //std::vector<Missile*> missiles;
    Missile* missile;
    Planet* planet;   
//    std::vector<Missile*> firedMissiles;
    float surface = 0.0f;

  public:
    MissileSilo(float size, float br, int n, Planet* p) : Model3D(size, br) {
      alive = true;
      nMissiles = missilesLeft = n;
      planet = p;
    }

    int getNumMissiles() { return missilesLeft; }

    void setSurface(float s) { surface = s; }
    //void addMissile(Missile* m) { missiles.push_back(m); }
    //Missile* getMissile(int i)  { return missiles[i]; }
    void setMissile(Missile* m) { missile = m; }
    Missile* getMissile() { return missile; }
    bool isAlive()          {return alive;}

    void destroy() {
      orientationMatrix = glm::mat4();
      alive = false;
    }

    void reset() {
      alive = true;
      translationMatrix = planet->getTranslationMatrix();
      rotationMatrix = planet->getRotationMatrix();
      orientationMatrix = planet->getOrientationMatrix();
      missilesLeft = nMissiles;

        missile->reset();
    }

    void fireMissile() {
        if(missilesLeft > 0) {
            missile->fire();
            missilesLeft--;
        }
    }

    void missileLogic(glm::mat4 warbird) {
        if(missile->isFired()) {
            if(missile->getFramecount() > missile->getActivation()) {
                missile->smartActivation();
            }
        }

        if(!missile->isFired()) {
            float distanceFromWarbird = distance(getPosition(warbird), getPosition(missile->getOrientationMatrix()));
            if(distanceFromWarbird <= missile->getDetectionRadius()) {
                fireMissile();
                missile->setTarget(warbird);
            }
        }

        if(missile->isSmart()) {
            missile->setTarget(warbird);
        }

        missile->update();
    }

    void update() {
      if(!alive) return;

      translationMatrix     = planet->getTranslationMatrix();
      rotationMatrix        = planet->getRotationMatrix();
      orientationMatrix     = planet->getOrientationMatrix();

      translationMatrix = glm::translate(orientationMatrix, glm::vec3(0, surface, 0));/////////get this amount per the planet
      orientationMatrix = translationMatrix;
      missile->update();
    }
};

#endif
