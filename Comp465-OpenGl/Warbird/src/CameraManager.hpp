#ifndef CAMERAMANAGER_HPP
#define CAMERAMANAGER_HPP

#ifndef __INCLUDES465__
#include "../includes465/include465.hpp"
#define __INCLUDES465__
#endif

#include "Camera.hpp"
#include <vector>
#include <iostream>

class CameraManager {
	private:
		std::vector<Camera*> cameras;
		Camera* mainCamera;
		int mainCameraIndex;
		int nCameras;

	public:
    enum cameraIndex {
      FRONT,
      TOP,
      WARBIRD,
      UNUMN,
      DUO
    };
		CameraManager() {

			//create cameras
			cameras.push_back(new Camera( //front camera
				glm::vec3(0, 10000, 20000),
				glm::vec3(0, 0, 0),
				glm::vec3(0, 1, 0)));

			cameras.push_back(new Camera( //top camera
				glm::vec3(0, 20000, 0),
				glm::vec3(modelPositions[WARBIRD]),
				glm::vec3(0, 0, 1)));

			cameras.push_back(new Camera( //ship camera
				glm::vec3(0, 300, 1000),
				glm::vec3(0, 0, 0),
				glm::vec3(0.0f, 1.0f, 0.0f)));

			cameras.push_back(new Camera( //unumn
				glm::vec3(-4000, 0, -4000),
				glm::vec3(modelPositions[UNUMN]),
				glm::vec3(0, 1, 0)));

			cameras.push_back(new Camera( //Duo
			   glm::vec3(-4000, 0, -4000),
			   glm::vec3(modelPositions[DUO]),
			   glm::vec3(0, 1, 0)));

			mainCameraIndex = 0;
			mainCamera = cameras[mainCameraIndex];
			nCameras = 5;
		}

		int getCameraIndex()			{ return mainCameraIndex; 	}
		Camera* getCurrentCamera() 		{ return mainCamera; 		}
		void setMainCamera(Camera* c)   { mainCamera = c; 		    }
        void setMainCamera(int index)   { mainCamera = cameras[index];}

		void addCamera(glm::vec3 eye, glm::vec3 at, glm::vec3 up) {
			cameras.push_back(new Camera(glm::vec3(eye),
										glm::vec3(at),
										glm::vec3(up)));
            nCameras++;
		}

    void setCamera(int index, Camera* c) {
      cameras[index] = c;
    }

		void nextCamera() {
			mainCameraIndex = (mainCameraIndex + 1) % nCameras;
            printf("camera index: %d", mainCameraIndex);
            showMat4(" ", cameras[mainCameraIndex]->getViewMatrix());
			mainCamera = cameras[mainCameraIndex];
		}

		void prevCamera() {
			mainCameraIndex = (mainCameraIndex -1 + nCameras) % nCameras;
			mainCamera = cameras[mainCameraIndex];
		}

		Camera* getCamera(int i) { return cameras[i]; }

		void followPlanet(int cameraIndex, glm::mat4 orientationMatrix) {
			cameras[cameraIndex]->setCamera(getPosition(glm::translate(orientationMatrix, //eye
						cameras[cameraIndex]->getPlanetEye())),getPosition(orientationMatrix), //at
						cameras[cameraIndex]->getUp());//up
		}

};
#endif
