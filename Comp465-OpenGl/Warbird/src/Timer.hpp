#ifndef TIMER_HPP
#define TIMER_HPP

#ifndef __INCLUDES465__
#include "../includes465/include465.hpp"
#define __INCLUDES465__
#endif

class Timer {
	private:
		int timeDelay;
		int framecount;
		double lastTime, elapsedTime, currentTime;
		int fps;

	public:
		Timer(int delay):timeDelay(delay) {
			fps = 0;
			framecount = 0;
			lastTime = 0; elapsedTime = 0; currentTime = 0;
		}

		void start() {
			lastTime = glutGet(GLUT_ELAPSED_TIME);
		}

		void reset() {
			currentTime = glutGet(GLUT_ELAPSED_TIME);
			elapsedTime = currentTime - lastTime;
			fps = (framecount / (elapsedTime / 1000));
			lastTime = currentTime;
			framecount = 0;
		}
		
		int getFPS() { return fps; }
		int getTimeDelay() { return timeDelay; }
		void incrementFPS() { framecount++; }
		void setTimeDelay(int d) { timeDelay = d; }
};


#endif
