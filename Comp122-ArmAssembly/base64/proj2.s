@  Joshua Benz
@  Comp 122 Mon/Wed
@  Project 2
@  Base 64 encoder

@ Written using vim editor on a linux machine running armsim
@ Tabs were used for Spacing
@
@ Everything seems to work except I can't quite get the output right
@ The first output line is correct and follows the input line as far
@ as where the new line occurs. However, After that, the output is
@ written to a new line every 4 characters. I'm not sure why.
@ They are, however, printed to STDOUT with correctly placed
@ new lines.


.equ SWI.open,          0x66	@ open  file
.equ SWI.close,         0x68	@ close file
.equ SWI.PrChar,        0x00	@ print ascii char to Stdout
.equ SWI.PrStr,         0x69	@ print null-terminated string to file
.equ SWI.RdStr,         0x6a	@ read string from file
.equ SWI.exit,          0x11	@ exit the program

.global _start
.text

_start:

OpenFiles:
        LDR r0, =InFileName	@ set file name
        MOV r1, #0		@ set input mode
        SWI SWI.open
	LDR r1, =InputHandle
        STR r0, [r1]		@ move file handle to r7

	LDR r0, =OutFileName
	MOV r1, #1
	SWI SWI.open
	LDR r1, =OutputHandle
	STR r0, [r1]
        

ReadFromFile:
	LDR r0, =InputHandle
        LDR r0, [r0]		@ get file handle
        LDR r1, =InString	@ set input string
        MOV r2, #1024		@ max string length
        SWI SWI.RdStr		@ set char array index address in r2
	BCS EofReached
	MOV r3, #0		@ reset counter

Encode:
	LDR r5, =InString
	LDRB r4, [r5, r3]	@ get a byte
	CMP r4, #0x00		@ check for null
	BEQ skip2		
	ADD r3, r3, #1		@ increment counter
	MOV r6, r4, LSR #2	@ r6 has the first 6 bits
	LDR r8, =LTable		@ set lookup table
	LDRB r9, [r8, r6]	@ get base64 char in r9
	
	LDR r1, =OutString

	STRB r9, [r1, #0]	@ store OutString sav, des, pos
	LDRB r0, [r1, #0]	@ des, string des, pos
        SWI SWI.PrChar

	AND r4, r4, #3		@ get last 2 bits of first number
	MOV r4, r4, LSL #4
	LDRB r1, [r5, r3]	@ get second char r1=2nd char
	MOV r1, r1, LSR #4	@ get last 4 bits we need
	ORR r9, r1, r4		@ or bits together for second num
	LDRB r9, [r8, r9]	@ lookup base64 char

	LDR r1, =OutString
	STRB r9, [r1, #1]	@ store 2nd char into OutString

        MOV r0, r9		@ write to file
        SWI SWI.PrChar

	LDRB r4, [r5, r3]	@ get the second char
	CMP r4, #0x00		@ check for null
	BEQ skip0
	ADD r3, r3, #1		@ increment counter
	LDRB r1, [r5, r3]	@ get 3rd char
	AND r4, r4, #15		@ get 4 least sig bits
	MOV r4, r4, LSL #2
	MOV r1, r1, LSR #6	@ get 2 sig bits
	ORR r9, r4, r1		@ or them together
	LDRB r9, [r8, r9]	@ lookup base64
	
	LDR r1, =OutString	
	STRB r9, [r1, #2]	@ store 3rd char into OutString
	
	MOV r0, r9
	SWI SWI.PrChar		@ write to file

	LDRB r4, [r5, r3]	@ get 3rd char
	CMP r4, #0x00		@ check for null
	BEQ skip1
	ADD r3, r3, #1	
	AND r4, r4, #63		@ get least sig 6 bits
	LDRB r9, [r8, r4]	@ lookup base64

	LDR r1, =OutString
	STRB r9, [r1, #3]	@ store 4th char into OutString
	MOV r0, r9
	SWI SWI.PrChar		@ Write to file

	LDR r0, =OutputHandle
	LDR r0, [r0]
	LDR r1, =OutString
	SWI SWI.PrStr

	B Encode
skip0:
	MOV r0, #0x3d		@ == case
	STRB r0, [r1, #2]	@ store padding 1 into OutString as 3rd char
	SWI SWI.PrChar
	MOV r0, #0x3d
	STRB r0, [r1, #3]	@ store padding 2 into OutString as 4th char
	SWI SWI.PrChar

	B skip2

skip1:
	MOV r0, #0x3d		@ = case
	STRB r0, [r1, #3]	@ store padding into OutString as 4th char
	SWI SWI.PrChar

	B skip2
skip2:
	MOV r0, #0x0d		@ carriage return
	STRB r0, [r1, #4]	@ store into OutString 5th char
	SWI SWI.PrChar
	MOV r0, #0x0a		@ new line
	STRB r0, [r1, #5]	@ store into OutString 6th char
	SWI SWI.PrChar

	LDR r0, =OutputHandle
	LDR r0, [r0]
	LDR r1, =OutString	@ set handler
	SWI SWI.PrStr

	MOV r9, #0
	MOV r4, #0		@reset registers	

	B ReadFromFile		

EofReached:
	LDR r0, =InputHandle
	LDR r0, [r0]		@ close input file
	SWI SWI.close

	LDR r0, =OutputHandle
	LDR r0, [r0]		@close output file
	SWI SWI.close		

	SWI SWI.exit

.data
InFileName:     .asciiz "input.txt"
OutFileName:    .asciiz "output64.txt"
LTable:         .asciiz "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

.align
InputHandle:    .skip 4
OutputHandle:   .skip 4
InString:       .skip 1024
OutString:      .skip 5
.end                                                                                                                                                                                                                   73,4          Bot
