@ Joshua Benz
@ 12 May, 2015
@ Comp 122 TUE/THURS
@ Project Last: Safe Control
@ Fries 


.equ SEG_A,0x80
.equ SEG_B,0x40
.equ SEG_C,0x20
.equ SEG_D,0x08
.equ SEG_E,0x04
.equ SEG_F,0x02
.equ SEG_G,0x01
.equ SEG_P,0x10


.equ key00,     0x01
.equ key01,     0x02
.equ key02,     0x04
.equ key03,     0x08
.equ key04,     0x10
.equ key05,     0x20
.equ key06,     0x40
.equ key07,     0x80
.equ key08,     1<<8
.equ key09,     1<<9
.equ key10,     1<<10
.equ key11,     1<<11
.equ key12,     1<<12
.equ key13,     1<<13
.equ key14,     1<<14
.equ key15,     1<<15

.equ SWI.SegDisplay,            0x200
.equ SWI.BlackPressed,          0x202
.equ SWI.KeyPad,                0x203
Modes:
        .word SEG_G|SEG_E|SEG_D|SEG_C|SEG_B             @ U
        .word SEG_G|SEG_E|SEG_D                         @ L
        .word SEG_E|SEG_G|SEG_A|SEG_B|SEG_F             @ P
        .word SEG_A|SEG_G|SEG_E|SEG_D                   @ C
        .word SEG_A|SEG_G|SEG_F|SEG_E                   @ F
        .word SEG_A|SEG_G|SEG_E|SEG_F|SEG_B|SEG_C       @ A
        .word SEG_A|SEG_G|SEG_E|SEG_D|SEG_F             @ E


.text
.global _start
@ Unlocked r9 = 1
@ locked r9 = 0
@ programming r8 = 1
@ Confirming r8 = 2
@ forgetting r8 = 3
@ success  r8 = 0
@ error r8 = -1
@ r7 = end of codes

_start:
	BL unlockSafe
	LDR r7, =codes
	B main

main:
@ while true, run through button polls
	BL pollBlackButtons
	CMP r0, #0
	BEQ main
	CMP r0, #1
	BLEQ onRightBlack
	CMP r0, #2
	BEQ onLeftBlack

onLeftBlack:
	STMFD sp!, {r4-r6, lr}
	CMP r9, #0
	BEQ enterCode

	enterCode:
		BL pollBlueButtons
		BL isValid

	LDMFD sp!, {r4-r6, pc}


isValid:
	STMFD sp!, {r4-r6, lr}
		LDR r4, =codes
		MOV r5, r7
		MOV r2, #0
		MOV r3, #0

		LDR r6, [r4, r2]
		LDR r2, [r7, r3]

		CMP r6, r2
		BNE inc
		BEQ incBoth
	inc:
		ADD r6, r6, #4
	incBoth:
		ADD r6, r6, #4
		ADD r2, r2, #4
		CMP r6, #16
		BEQ codeIsValid

	codeIsValid:
		BL unlockSafe
		
	LDMFD sp!, {r4-r6, pc}
@available actions on the right 
onRightBlack:
	STMFD sp!, {r4-r6, lr}
	MOV r8, #1 @ change mode to programming mode
	MOV r0, #2 @ display P segment
	BL displayMode

programCodeLoop:
	BL pollBlackButtons
	CMP r0, #0 @ no buttons were pressed
	BEQ pollBlueButtons @get codes
	CMP r0, #2
	BEQ exitProgramLoop @leave programming mode to lock
	CMP r0, #1
	BEQ doneWithProgramLoop @successful loop

	B programCodeLoop

	exitProgramLoop:
		BL lockSafe
		LDMFD sp!, {r4-r6, lr}
		B main

	doneWithProgramLoop:
		MOV r1, #16
		ADD r2, r2, #4
		STR r1, [r4, r2]
		BL ConfirmCode
		
	LDMFD sp!, {r4-r6, pc}


ConfirmCode:
	STMFD sp!, {r4-r6, lr}
		MOV r0, #3
		BL displayMode
		
confirmCodeLoop:
        BL pollBlackButtons
        CMP r0, #0 @ no buttons were pressed
        BEQ confirmCodeLoop @get codes
        CMP r0, #2
        BEQ exitProgramLoop @leave programming mode to lock
        CMP r0, #1
	BLEQ pollBlueButtons
        BEQ doneWithProgramLoop @successful loop

        B confirmCodeLoop

        exitConfirmLoop:
                BL lockSafe
		LDMFD sp!, {r4-r6, lr} @take things off the stack to preserve order
                B main

        doneWithConfirmLoop:
                ADD r2, r2, #4		
                LDR r1, [r4, r2]
		CMP r1, #16
		BEQ confirmed
		BNE notConfirmed

	confirmed:
                BL ConfirmCode
		MOV r7, r2
		MOV r0, #5
		BL displayMode
		LDMFD sp!, {r4-r6, lr}
		B main

	notConfirmed:
		MOV r0, #6
		BL displayMode
		LDMFD sp!, {r4-r6, lr}
		B main

	LDMFD sp!, {r4-r6, pc}


pollBlueButtons:
	STMFD sp!, {r4-r6, lr}
	MOV r4, r7
	blueLoop:
		MOV r0, #0
	buttonsPressed:
		SWI SWI.KeyPad
	
		CMP r0, #0
		BEQ checkEnd

		CMP r0, #key15
		BEQ FIFTEEN

		CMP r0, #key14
		BEQ FOURTEEN

		CMP r0, #key13
		BEQ THIRTEEN

		CMP r0, #key12
		BEQ TWELVE

		CMP r0, #key11
		BEQ ELEVEN

		CMP r0, #key10
		BEQ TEN

		CMP r0, #key09
		BEQ NINE

		CMP r0, #key08
		BEQ EIGHT

		CMP r0, #key07
		BEQ SEVEN

		CMP r0, #key06
		BEQ SIX

		CMP r0, #key05
		BEQ FIVE

		CMP r0, #key04
		BEQ FOUR

		CMP r0, #key03
		BEQ THREE

		CMP r0, #key02
		BEQ TWO

		CMP r0, #key01
		BEQ ONE

		CMP r0, #key00
		BEQ ZERO

	ZERO:
		MOV r1, #0
		ADD r4, r4, #4
		STR r1, [r7, r4]
		BAL checkEnd

	ONE:
		MOV r1, #1
		ADD r4, r4, #4
		STR r1, [r7, r4]
		BAL checkEnd

	TWO:
		MOV r1, #2
		ADD r4, r4, #4
		STR r1, [r7, r4]
		BAL checkEnd

	THREE:
		MOV r1, #3
		ADD r4, r4, #4
		STR r1, [r7, r4]
		BAL checkEnd

	FOUR:
		MOV r1, #4
		ADD r4, r4, #4
		STR r1, [r7, r4]
		BAL checkEnd

	FIVE:
		MOV r1, #5
		ADD r4, r4, #4
		STR r1, [r7, r4]
		BAL checkEnd

	SIX:
		MOV r1, #6
		ADD r4, r4, #4
		STR r1, [r7, r4]
		BAL checkEnd

	SEVEN:
		MOV r1, #7
		ADD r4, r4, #4
		STR r1, [r7, r4]
		BAL checkEnd

	EIGHT:
		MOV r1, #8
		ADD r4, r4, #4
		STR r1, [r7, r4]
		BAL checkEnd

	NINE:
		MOV r1, #9
		ADD r4, r4, #4
		STR r1, [r7, r4]
		BAL checkEnd

	TEN:
		MOV r1, #10
		ADD r4, r4, #4
		STR r1, [r7, r4]
		BAL checkEnd

	ELEVEN:
		MOV r1, #11
		ADD r4, r4, #4
		STR r1, [r7, r4]
		BAL checkEnd

	TWELVE:
		MOV r1, #12
		ADD r4, r4, #4
		STR r1, [r7, r4]
		BAL checkEnd

	THIRTEEN:
		MOV r1, #13
		ADD r4, r4, #4
		STR r1, [r7, r4]
		BAL checkEnd

	FOURTEEN:
		MOV r1, #14
		ADD r4, r4, #4
		STR r1, [r7, r4]
		BAL checkEnd

	FIFTEEN:
		MOV r1, #15
		ADD r4, r4, #4
		STR r1, [r7, r4]
		BAL checkEnd

	checkEnd:
		BL pollBlackButtons
		CMP r0, #0
		BEQ blueLoop
		CMP r0, #2
		BEQ exitBlueLoop
		CMP r0, #1
		BEQ doneWithBlueLoop

	exitBlueLoop:
		BL lockSafe
		LDMFD sp!, {r4-r6, lr}
		B main

	doneWithBlueLoop:
		MOV r2, r4
		LDMFD sp!, {r4-r6, pc}



	
@ returns which button was pressed in r0
@ r0 = 2 is left
@ r0 = 1 is right
pollBlackButtons:	
	STMFD sp!, {r4-r7, lr}
	SWI SWI.BlackPressed
	LDMFD sp!, {r4-r7, pc}


@ r0 = segment letter
lockSafe:
	STMFD sp!, {r4-r7, lr}
	MOV r9, #0
	MOV r0, #1
	BL displayMode
	MOV r0, #0
	LDMFD sp!, {r4-r7, pc}

@ r0 = segment letter
unlockSafe:
	MOV r0, #0
	MOV r9, #1
	STMFD sp!, {r0-r2, lr}
	BL displayMode
	LDMFD sp!, {r0-r2, pc}


displayMode:
	STMFD sp!, {r0-r2, lr}
	LDR r2, =Modes
	LDR r0, [r2, r0, LSL #2]
	SWI SWI.SegDisplay
	LDMFD sp!, {r0-r2, pc}
.data
.align
codes: .skip 1024
.end
