

public class SLinkNode 
{
	public Object item;
	public Node next;

	SListNode(Object item, SLinkNode next)
	{
		this.item = item;
		this.next = next;
	}

}
