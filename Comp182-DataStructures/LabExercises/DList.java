
class DListNode {
        String item;
        DListNode next;
        DListNode prev;


        public DListNode() {
        	item = "";
		next = null;
		prev = null;
        }
	
	public DListNode(String i, DListNode n) {
		item = i;
		next = n;
	}

	public DListNode(String i) {
		item = i;
		next = null;
	}
}


class DList {
	private DListNode head;
	private long size;

	DList() {
		head = new DListNode();
		head.prev = head;
		head.next = head;
		size = 0;
	}

	public void insertBack() {
		
	}

	public void removeBack() {
		if(head.prev == head) {
			head.prev = head.prev.prev;
			head.prev.next = head;
			size--;
		}
	}

	public static void main(String[] args) {
		
		DList d = new DList();
		
		DListNode sentinal = new DListNode();
		DListNode node = new DListNode("food");
		System.out.println(sentinal.item);
	}



}
