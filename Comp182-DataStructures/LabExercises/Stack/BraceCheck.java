//Written by KonkCode bitch

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class BraceCheck {
	private Node head;
	private long size;
	private Scanner read;

	public BraceCheck()
	{
		head = null;
		size = 0;
	}

	public void push(String s, long i)
	{
		head = new Node(s, head);	
		head.setLineCount(i);
		size++;
	}

	public String peek()
	{
		return(head.getData());
	}

	public String pop()
	{
		String data = new String();
		if(head == null)
			data = null;
		else {	
			data = head.getData();
			head = head.getNext();
			size--;
		}
		return data;
	}

	private boolean check(String lefty, String righty)
	{
			
		if(lefty.equals("{") && righty.equals("}"))
			return true;
		else if(lefty.equals("[") && righty.equals("]"))
			return true;
		else if (lefty.equals("(") && righty.equals(")"))
			return true;
		else if(lefty.equals(righty))
			return true;
		else
			return false;	
	}

	public void printStack()
	{
		Node curr = head;
		while(curr != null) {
			System.out.println(curr.getData());
			curr = curr.getNext();
		}
	}
	
	private String getRighty(String s)
	{
		String rtn = new String();
		if(s == null)
			return null;
	
		if("{[(".contains(s)) {
			if(s.equals("{"))
				rtn = "}";
			if(s.equals("["))
				rtn = "]";
			if(s.equals("("))
				rtn = ")";
			//if(s.equals("\""));
			//	rtn = "\"";
		} else
			rtn = null;
		return rtn;
	}	

	private void doTheWork(String s, long count)
	{

		String str = new String();
	
		for(int i = 0; i<s.length(); i++) {
			char c = s.charAt(i);
			str = Character.toString(c);

		 if("{([".contains(str))
                                push(str, count);

			if("})]".contains(str)) {
	
				String lefty = pop();
				String righty = getRighty(lefty);

				if(lefty == null) {
					System.out.println("Extra " + str + " at line " + count);
					return;
				}

				if(!check(lefty, righty)) {
					System.out.println("Missing " + righty + " at line " + count);
					return;
				}
			}
		}
	}

	private void handleQuotes(String s, long count)
	{
		int counter = 0;
		for(int i=0; i<s.length(); i++) {
			if(Character.toString(s.charAt(i)).equals("\"")) 
				counter++;				
				
		}
		
		if(counter % 2 != 0) {
			System.out.println("Extra or Missing \"  at line " + count);
			System.exit(0);
		}
	}
	public void readFile(String fileName)
	{
		String s = new String();
		long count = 0;
		try {
			read = new Scanner(new File(fileName));
			while (read.hasNextLine()) { 
     				s = read.nextLine();
				count++;
				
				handleQuotes(s, count);				
				doTheWork(s,count);
			}
		
		read.close();
		} catch(FileNotFoundException e) {
			System.out.println("No such File!");
			System.exit(0);
		}

		if(size > 0) 
			System.out.println("Missing " + getRighty(peek()) + " at line " + head.getLineCount());
		else 
			System.out.println("Everything looks good I think.");
		
	}	


	public static void main(String[] args)
	{
		BraceCheck stack = new BraceCheck();
		stack.readFile(args[0]);
	}

}

class Node {

	private String data;
	private long lineCount;
	private Node next;

	public Node(String data, Node next)
	{
		this.data = data;
		this.next = next;
		lineCount = 0;
	}

	public long getLineCount()
	{
		return lineCount;
	}
	
	public void setLineCount(long lineCount)
	{
		this.lineCount = lineCount;
	}

	public Node getNext()
	{
		return next;
	}

	public String getData()
	{
		return data;
	}

	public void setData(String d)
	{
		data = d;
	}
	
	public void setNext(Node n)
	{
		next = n;
	}
}
