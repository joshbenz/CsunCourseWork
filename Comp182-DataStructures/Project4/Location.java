//Name: Joshua Benz
//SID:105284931

public class Location {
	private int xCoordinate, yCoordinate;
	private String name;

	public Location(int x, int y, String nm) {
		xCoordinate = x;
		yCoordinate = y;
		name = nm;
	}

	public int getX() {
		return xCoordinate;
	}

	public void setX(int x) {
		xCoordinate = x;
	}

	public int getY() {
		return yCoordinate;
	}

	public int[] getPair() {
		int[] pair = {xCoordinate, yCoordinate};
		return pair;
	}

	public String getName() {
		return name;
	}

	public String toString() {
		return xCoordinate + ":" + yCoordinate + " " + name;
	}

	//gives teh distance from the origin

	public double distanceFromOrigin() {
		return (Math.sqrt(
		(Math.pow((xCoordinate - 0), 2) +
		Math.pow((yCoordinate - 0), 2))));

	}
}
