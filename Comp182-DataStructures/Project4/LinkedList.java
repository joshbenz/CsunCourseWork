//Name: Joshua Benz
//SID:105284931

public class LinkedList {
	class Node {
		private Location data;
		private Node next;

		public Node() {
			data = null;
			next = null;
		}

		public Node(Location l) {
			data = l;
			next = null;
		}

		public Node(Location l, Node n) {
			data = l;
			next = n;
		}

		public Location getData() {
			return data;
		}

		public void setData(Location l) {
			data = l;
		}

		public Node getNext() {
			return next;
		}

		public void setNext(Node n) {
			next = n;
		}
	}

	private Node head;
	//private Node tail;
	public LinkedList() {
		head = null;
		//tail = null;
	}

	public void setHead(Node n) {
		head.next = n;
	}	

	public Node getHead() {
		return head;
	}

	public Node getTail() {
		Node curr = head;
		while(curr != null) {
			curr = curr.next;
		}
		return curr;
	}

	public void addFirst(Location l) {
		head = new Node(l,head);
	}

	public void addLast(Location l) {
		if(head == null)
			addFirst(l);

		else {
			Node curr = head;
			while(curr != null) {
				curr = curr.next;	
			}

			curr.next = new Node(l);
		}
	}

	public Location removeFirst() {
		if(head == null)
			return null;
		Location rtn = head.data;
		head = head.next;

		return rtn;
	}


}

