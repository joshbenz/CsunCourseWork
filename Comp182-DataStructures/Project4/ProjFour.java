//Name: Joshua Benz
//SID:105284931

public interface ProjFour {
	public void readInitialFromFile();
	public void addLocation(Location l);
	public void deleteLocation(int loc);
	public void tradeItems(int i, int j);
	public void sortByX();
	public void sortByY();
	public void sortByDist();
	public double travelDistance();
	public void writeFinalToFile();
}
