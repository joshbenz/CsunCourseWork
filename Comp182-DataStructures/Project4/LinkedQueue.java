//Name: Joshua Benz
//SID:105284931


public class LinkedQueue {

	private class Node {
		public Location data;
		public Node next;

		public Node(Location data, Node next) {
			this.data = data;
			this.next = next;
		}
	}



	private Node head;
	private Node tail;
	private int size;

	public LinkedQueue() {
		head = null;
		tail = null;
		size = 0;
	}

	public Node getHead() {
		return head;
	}


	public void enqueue(Location l) {
		Node n = new Node(l, null);
		if (isEmpty()) 
			head = n;
		else 
			tail.next = n;

		tail = n;
		size++;
	}

	public Location dequeue() {
		Location rtn = head.data;
		if (tail == head) 
			tail = null;

		head = head.next;
		size--;
		return rtn;
	}

	public Location peek() {
		return head.data;
	}

	public boolean isEmpty() {
		return (size==0);
	}
}
