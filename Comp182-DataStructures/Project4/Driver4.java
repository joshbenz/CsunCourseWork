//Name: Joshua Benz
//SID:105284931

import java.util.Scanner;
public class Driver4 {
	public static String file = "locations.txt";
	private VectorOfLocations loc;

	public Driver4() {
		loc = new VectorOfLocations();
	}
	
// gets user's command 
        public static String getCommand()
        {
                Scanner scan = new Scanner(System.in);

                System.out.print("> ");
                String cmd = scan.nextLine().trim();
                return cmd;
        }

/****************************************************************************************
	Proccesses all of the user input commands. Attempts to deal with invalid
	input and executes the requested command with the parsed data if it is valid.
*****************************************************************************************/

	public void exeCmd(String cmd) {
		cmd = cmd.toLowerCase();
		if(cmd.equals("show all"))
			loc.showAll();
		else if(cmd.equals("sort dist"))
			loc.sortByDist();
		else if(cmd.equals("sort x"))
			loc.sortByX();
		else if(cmd.equals("sort y"))
			loc.sortByY();
		else if(cmd.contains("new ")) {
			String[] arr = cmd.split(" ");
			if(arr.length <= 4) {
				String[] data = new String[3];
				data[0] = arr[1];
				data[1] = arr[2];
				data[2] = arr[3];
			
				if(isInt(data[0]) && isInt(data[1]))
					loc.addLocation(loc.createLocation(data));
			} else {
				return;
			}
		} else if(cmd.contains("switch")) {
                        String[] arr = cmd.split(" ");
                        	
			if(arr.length <= 3) {
				String[] data = new String[2];
				data[0] = arr[1];
				data[1]	= arr[2];
                        
                        	if(isInt(data[0]) && isInt(data[1]))
					loc.tradeItems(Integer.parseInt(arr[1]), Integer.parseInt(arr[2]));
			} else {
				return;
			}
		} else if(cmd.contains("remove")) {
                        String[] arr = cmd.split(" ");
                        if(arr.length > 2)
                        
                        if(isInt(arr[1]))
				loc.deleteLocation(Integer.parseInt(arr[1]));
		} else if(cmd.equals("quit")) {
				loc.writeFinalToFile();
				System.exit(0);
		} else 
			return;
	}
/****************************************************************************************
	Checks if the input string can be parsed as an integer. Returns true if it can
	be parsed and false if it cannot.
*****************************************************************************************/
	        private boolean isInt(String s)                
        {
                boolean flag = false;
                try {
                        int l = Integer.parseInt(s);
                        flag = true;
                } catch(NumberFormatException e) {
                        flag = false;
                }

                return flag;
        }

	


	public static void main(String args[]) {

		Driver4 d4 = new Driver4();
		System.out.println("Welcome Traveler\n Loading File now...");
		d4.loc.readInitialFromFile();
		while(true) {
			d4.exeCmd(Driver4.getCommand());
		}
	}
}
