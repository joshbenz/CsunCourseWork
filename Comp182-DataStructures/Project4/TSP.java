//Name: Joshua Benz
//SID:105284931


/****************************************************************************************
	The idea was to store the indicies of a particular permutation and the total
	distance it gave, so that when I was compared, I would have the order of the
	indicies i needed to  obtain the optimal distance.
*****************************************************************************************/

public class TSP {
	public int[] loc;		//for indicies
	private double distance;	//for the total distance


	public TSP(int[] size) {
		loc = size;
		distance = 0.0;
	}

	public double getDistance() {
		return distance;
	}

	public int[] getLocs() {
		return loc;
	}

	public void setDistance(double d) {
		distance = d;
	}

	
}
