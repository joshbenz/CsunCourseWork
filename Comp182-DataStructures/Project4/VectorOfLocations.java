
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.util.Random;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class VectorOfLocations implements ProjFour {
	private int size, capacity;
	Location[] thePlaces;

	VectorOfLocations() {
		size = 0;
		capacity = 5;
		thePlaces = new Location[capacity];
	}

	public boolean isFull() {
		return(size==capacity);
	}
	
	public boolean isEmpty() {
		return(size == 0);
	}

	public int getSize() {
		return size;
	}

	public void incrementCapacity() {
		Location[] tmp = new Location[capacity + 5];

		for(int i=0; i<size; i++) {
			tmp[i] = thePlaces[i];
		}
		thePlaces = tmp;
		capacity += 5;
	}
/*****************************************************************************************
	reads the input file, performs varios checks to make sure that the format is
	correct. Calls the methods to create the objects and store them into the vector
*****************************************************************************************/
	 public void readInitialFromFile() {
		 String filename = Driver4.file;
		 String data = new String();
		 String[] dataArray = new String[2];
	
		 try {
			Scanner scan = new Scanner(new File(filename));
			while(scan.hasNextLine()) {
				data = scan.nextLine();

				if(!isValidFileInput(data)) {			//checks if input is valid
					System.out.println("Invalid input file!");
					System.exit(0);
				} else {
					dataArray = data.split(":");
					addLocation(createLocation(dataArray));
				}
			}
				
			scan.close();
		}
		catch(FileNotFoundException e) {
			System.out.println("No such File!");
			System.exit(0);
		}
	}

/****************************************************************************************
	checks if the data from the file is formatted correctly by counting the number
	of : in the file. It also calls a method to ensure the numbers that follow are
	Integers.
*****************************************************************************************/

	private boolean isValidFileInput(String s) {
		int count = 0;

		for(int i=0; i<s.length(); i++) {
			if(s.charAt(i) == ':')
				count++;
		}
		if(count != 3 && !isFileInt(s))
			return true;

		return false;
	}

/****************************************************************************************
	Attempts to parse the numbers from the locations.txt and throws an exception
	if they cannot be parsed, thus telling you something is wrong with the format
	of the file.
*****************************************************************************************/

	public boolean isFileInt(String s) {
		String[] input = s.split(":");

		try {
			Integer.parseInt(input[0]);
			Integer.parseInt(input[1]);
			return false;
		} 
		catch(NumberFormatException e) {
			return true;
		}
	}

/****************************************************************************************
	creates and returns the Location Object
*****************************************************************************************/

	public Location createLocation(String[] sA) {
		return new Location(Integer.parseInt(sA[0]), Integer.parseInt(sA[1]), sA[2]);
	}

/****************************************************************************************
	checks if the vector is full, and grows it accordingly. Then adds the Location 
	object to the vector and increments the size counter for the vector.
*****************************************************************************************/

	public void addLocation(Location l) {
		if(isFull()) {
			incrementCapacity();
		}
		thePlaces[size] = l;
		size++;
	}

/****************************************************************************************
	Decrements the size counter of the vector, then checks if it is already empty.
	If not, then I removes the Location object at the requested location and shits
	the other elements over.
*****************************************************************************************/

	public void deleteLocation(int loc) {
		if(!isEmpty()) {
			size--;
			for(int i=loc-1; i<size; i++) {
				thePlaces[i] = thePlaces[i+1];
			}
		} else {
			System.out.println("Empty List!");
			return;
		}
	}

/****************************************************************************************
	Checks if the requested Locaions can be swapped and does so accordingly
*****************************************************************************************/

	public void tradeItems(int i, int j) {

		if(i < 0 || j < 0 || i == j) {
			return;
		}
		if(!isEmpty()) {
			Location tmp = thePlaces[i];
			thePlaces[i] = thePlaces[j];
			thePlaces[j] = tmp;
		} else {
			System.out.println("Empty List!");
			return;
		}
	}

/****************************************************************************************
	Calculates the distance between 2 points assuming they are connected by a 
	straight line.
*****************************************************************************************/
	public double travelDistance(int[] p1, int[] p2) {
		return (Math.sqrt(
            		Math.pow(p1[0] - p2[0], 2) +
            		Math.pow(p1[1] - p2[1], 2)));
	}

/****************************************************************************************
	Calculates the Total distance from the origin to all of the points
*****************************************************************************************/

	public double travelDistance() {
		int[] origin = {0,0};
		double total = 0.0; //= thePlaces[0].distanceFromOrigin() + thePlaces[size].distanceFromOrigin();

		for(int i=0; i<size; i++) {
			total = total + travelDistance(origin, thePlaces[i].getPair());
			origin = thePlaces[i].getPair();
		}

		return total;
	}

//calls slelection sort

	public void sortByDist() {
		selectionSort();
	}

/****************************************************************************************
	The usual selection sort, Finds the min travel distance from the origin to the 
	location and sorts it by trading the smallest to the front.
*****************************************************************************************/

	public void selectionSort() {
		int length = size, minIndex = 0;
		Location min = null;
		
		for(int i=0; i<size; i++) {
			minIndex = i;
			for(int j=i+1; j<size; j++) {
				if(thePlaces[j].distanceFromOrigin() < thePlaces[minIndex].distanceFromOrigin())
					tradeItems(j, minIndex);	
			}
		}
	}
	
	//converts linked list to a vector

	public void linkedListToVector(LinkedList.Node head) { 
		int count = 0;

		while(head != null) {
			thePlaces[count] = head.getData();
			head = head.getNext();
		}
	}

	public LinkedList vectorToLinkedList() {
		LinkedList list = new LinkedList();

		for(int i=0; i<size; i++) {
			list.addFirst(thePlaces[i]);
		}

		return list;
	}

/****************************************************************************************
	Converts the vector to a linked list, Sorts it and converts it back to the 
	vector.
*****************************************************************************************/

	public void sortByY() {
		LinkedList list = vectorToLinkedList();
		//System.out.println("meh");
		quickSortLinkedList(list.getHead());
		linkedListToVector(list.getHead());
	}  

	public int listLength(LinkedList.Node head) {
		int size = 0;
		while(head != null) {
			size++;
			head = head.getNext();
		}
		return size;			
	}

/****************************************************************************************
	My attempt at selecting a random pivot.
*****************************************************************************************/

	public int pickPivot(LinkedList.Node head) {
		int n = listLength(head);
		Random rand = new Random();
		int k = rand.nextInt(n);
		LinkedList.Node pivot = head;

		for(int i=0; i<k; i++) {
			pivot = pivot.getNext();
		}

		return pivot.getData().getY();
	}

	public void quickSortLinkedList(LinkedList.Node newHead) {
	
		if(listLength(newHead) <= 1) {//if lists are empty then return
			//System.out.println("Empty List!");
			return;
		}

		LinkedList smaller = new LinkedList();
		LinkedList larger = new LinkedList();
		LinkedList pivots = new LinkedList();

        //find pivot randomly
        //populate list, go through every value to see which list it goes in

		int pivot = pickPivot(newHead);
		//System.out.println(pivot);
		LinkedList.Node curr = newHead;

		while(curr != null) {
			int value = curr.getData().getY();
			System.out.println(pivot);
			if(value < pivot) {
				smaller.addLast(curr.getData());
			} else if(value > pivot) {
				larger.addLast(curr.getData());
				//System.out.println(value);
			} else {
				pivots.addLast(curr.getData());           //value is the whole location -->current.getLocation.getY()
				//System.out.println(value);
			}

			curr = curr.getNext();
		}
        //sort the 3 lists independently
		quickSortLinkedList(smaller.getTail());
		quickSortLinkedList(larger.getTail());

        //put them together(link them)
        //set tail of smaller_list to head of pivot_list

		smaller.setHead(pivots.getTail());
		pivots.setHead(larger.getTail());	
	}

/****************************************************************************************
	Returns the minimum x coordinate in the vector
****************************************************************************************/

	public int getMin() {
		int min = 0, digit = 0;
		for(int i=0; i<size; i++) {
			digit = thePlaces[i].getX();
			if(min > digit && digit < 0)
				min = digit;
		}
		return min;
	}

/****************************************************************************************
	Calls the above function to find the smallest number and add it as an offset to 
	each X coordinate, also returns the number offset. Will be used in Radix sort. 
*****************************************************************************************/

	public int addNegOffset() {
		int offset = Math.abs(getMin());
		int tmp = 0;
		for(int i=0; i<size; i++) {
			tmp = thePlaces[i].getX();
			thePlaces[i].setX(tmp + offset);
		}

		return offset;
	}

/****************************************************************************************
	Removes the offset from each x coordinate.
*****************************************************************************************/
	public void removeOffset(int off) {
		int tmp = 0;
		for(int i=0; i<size; i++) {
			tmp = thePlaces[i].getX();
			thePlaces[i].setX(tmp - off);	
		}
	}

/****************************************************************************************
	To remove the problem of negative numbers, we have found the smallest number
	and used it as an offset, thus removing any negative numbers. The list is then
	sorted using radix sort and the Offset is removed again, returning the 
	Coordinates back to their original numbers.
*****************************************************************************************/

	public void sortByX() {

		int offset = addNegOffset();
		radixSort();
		removeOffset(offset);
	}

// Radix Sort

	public void radixSort() {
		LinkedQueue[] bucket = new LinkedQueue[10];	//array of queues implemented with linked lists

		for(int i=0; i<10; i++) {			//Initialze all of these queues or else we get
			bucket[i] = new LinkedQueue();		//nullpointer exceptions later
		}
		Location[] tmp = new Location[size];
		int curr = 0, 	//stores the current radix it is on
		div = 0, 	//stores the divisor used to advance the radix
		num = 0, 	//Stores the X coordinate
		digit = 0, 	//stores the digit of the current radix
		index = 0;	//stores the current index of when dequeueing
		
		int maxDigits = getMax();	//gets amount of digits of largest number
			
		while(curr < maxDigits) {
			for(int i=0; i<size; i++) {	//loop through vector
				div = (int)(Math.pow(10,curr));
				num = thePlaces[i].getX();	//get value of current index
				digit = (int)((num/div)%10); 	//get the decimal digit of the current spot
				bucket[digit].enqueue(thePlaces[i]); 	//place the digits into the right buckets
			}

			index = 0;
			for(int j=0; j<10; j++) {	//loop throug all the buckets
				while(!bucket[j].isEmpty()) {	//if its not empty then dequeue it
					thePlaces[index] = bucket[j].dequeue();
					index++;
				}
			}
		curr++;
		}
	}

/****************************************************************************************
	Counts the number of digits in the largest number by first finding the largest
	number, then converting it to a string and counting characters.
*****************************************************************************************/

	public int getMax() {
		int max = 0;
		int digit = 0;

		for(int i=0; i<size; i++) {
			digit = thePlaces[i].getX();

			if(digit > max)
				max = digit;
		}

		String s = Integer.toString(max);
		
		int numDigits = 0;
		while(numDigits < s.length()) {
			numDigits++;
		}
		return numDigits;
	}

//Loops through the vector and prints everything along with the total distance

	public void showAll() {
		for(int i=0; i<size; i++) {
			System.out.println(thePlaces[i].toString());
		}
		System.out.println("Total Distance: " + travelDistance());
	}

	public void writeFinalToFile() {

		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("locations.txt"), "utf-8"))) {
    			for(int i=0; i<size; i++) {
				writer.write(thePlaces[i].toString() + "\n");
			}

		} catch (IOException e) {
			File file = new File("locations.txxt");
		}  
	}


/****************************************************************************************

					Part II

****************************************************************************************/


/****************************************************************************************
	My attempt here was to get the indicies of each element in the vector and
	store every permutation of it. Then I stored it to a string array and parsed
	it to Integers in a TSP object, which has fields arry for the indicies and double
	for the total distance of that particular permutation. I thought this was a 
	better idea than I had before becuase I was just permutating a list of integers
	rather than a vector of Location Objects. However, this is as far as I got
	I was working on calculating the the optimal distance. Also, for the 
	improvement I was going to attempt the greedy neighbor idea where it finds the
	closest neighbor and uses that distance.
*****************************************************************************************/




 
//Attempt at the Brute Force Method
	public void BruteOptimalDistance(int x, int y) {
		int[] locIndicies = getIntArray();
	
		String list = "";
		do {	
			for(int i=0; i<size; i++)			//store permutations as a string
				 list = list + " " + locIndicies[i];
				
		} while(nextPermutation(locIndicies));
		list = list.trim();
		String[] data = list.split(" ");
		TSP[] locs = new TSP[factorial(size)];			//place the permustations in a String array
									
		int i = 0;
		int j = 0;

		int[] tmp = new int[size];
		while(j<factorial(size)) {
			for(int k=0; k<size; k++) {			//place the permutations in an object so the
				tmp[k] = Integer.parseInt(data[i]);	//data is together when I need it
			}
			locs[j] = new TSP(tmp);
			j++;
		}

		int[] origin = {x,y};
		double d = 0.0;

		for(i=0; i<locs.length; i++) {
			for(j=0; j>size; j++) {
				d = d + (travelDistance(origin, thePlaces[locs[j].loc[j]].getPair()));	//calulate the total distance of 
				origin = thePlaces[locs[i].loc[i]].getPair();				//that route so I can store it in
			}										//the Object for comparison later
		}
			
		
	}


//gets the number of elements in the array and stores them in an array

	public int[] getIntArray() {
		int[] rtn = new int[size];
		for(int i=0; i<size; i++) {
			rtn[i] = i;
		}

		return rtn;
	}

/****************************************************************************************
	Gives me the next Permutations for my brute force search
*****************************************************************************************/

	public boolean nextPermutation(int[] a) {
		int i = a.length-1;
		while(i > 0 && a[i-1] >= a[i]) {
			i--;
		}
		if(i <= 0)
			return false;
		
		int j = a.length - 1;
		while(a[j] <= a[i-1]) {
			j--;
		}

		int tmp = a[i-1];
		a[i-1] = a[j];
		a[j] = tmp;

		j = a.length-1;
		while(i < j) {
			tmp = a[i];
			a[i] = a[j];
			a[j] = tmp;
			i++;
			j--;
		}
		return true;
	}

/****************************************************************************************
	Factorial to tell me how many permutations there will be so i can sore them
****************************************************************************************/

	public int factorial(int n) {
		int fact = 1;
	
		for(int i=1; i<=n; i++) {
			fact *= i;
		}

		return fact;
	}
}
