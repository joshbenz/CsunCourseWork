/*
Programmer: Joshua Benz
Project# 2
Description: Interface for the Contact book.
*/

public interface projTwo
{
	public abstract void readInitialFromFile();
	public abstract void writeFinalToFile();
	public abstract void addContact(Contact c);
	public abstract void deleteContact(String nm);
	public abstract void showByName(String nm);
	public abstract void showByPhoneNumber(long pN);
	public abstract void showByComment(String c);
}
