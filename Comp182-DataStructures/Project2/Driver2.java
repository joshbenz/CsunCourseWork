/*
Programmer: Joshua Benz
Project# 2
Driver2 class description: The driver of the Contact book, creates the neccessary object
			   based on desired vector type. 
*/

import java.util.Scanner;

public class Driver2 
{
	public static String txt = "contacts.txt";	//filename
	private VectorOfContacts vector;		//vector object prototype
	private boolean quitFlag;			//set to true when quit is entered
	
	public Driver2(String s)
	{
		quitFlag = false;
		if(s.toLowerCase().contains("ordered vector")) {	//casts to an OrderedVectorOfContacts object
			vector = new OrderedVectorOfContacts();
			System.out.println("placing in Ordered vector!"); 
		} else if(s.toLowerCase().contains("vector")) {
			vector = new VectorOfContacts();			//normal vector
			System.out.println("Placing in Vector!");
		} else if(s.toLowerCase().contains("quit"))
			System.exit(0);
		else if (s.contains("\\s+")) 
			Driver2.errors(2);
		else {
			vector = new VectorOfContacts();			//default is the normal vector
			System.out.println("Placing in Defualt Vector");
		}
	}

	public Driver2()					
	{
		quitFlag = false;					//constructor
		vector = new VectorOfContacts();
		System.out.println("Placing in default vector!");
	}


	public static void intro()
	{
		System.out.println("Welcome to Contact Book.\n\n");
		System.out.println("Would you like to use a Vector or an Ordered Vector" + 
			"?(Ordered by phone number)");
	}

	public static String getCommand()
	{
		Scanner scan = new Scanner(System.in);		//gets the users command

		System.out.print("> ");
		String cmd = scan.nextLine().trim();	
		return cmd;
	}

	public static void errors(int error)
	{
		if(error == 1)
			System.out.println("Not a valid Command!");
		if(error == 2)
			System.out.println("Invalid input!");
	}	

	private String split(String c)			//splits at a space, rejoins the needed parts, returns string
	{
		String[] sA = c.split("\\s+");
		String str = "";

		for(int i=2; i<sA.length; i++)
			str = " " + sA[i];                 

		return(str.trim());
	}

	private String[] add()			//prompts and saves info to add a new contact
	{
		String[] sA = new String[3];
		System.out.print("Name: ");
		sA[0] = getCommand();
		
		System.out.print("Phone Number: ");
		sA[1] = getCommand();
		
		System.out.print("Comment");
		sA[2] = getCommand();

		return sA;
	}
	
	private boolean isLong(String s)		//checks if the phone number entered can be parsed
	{
		boolean flag = false;
		try {
			long l = Long.parseLong(s);
			flag = true;
		} catch(NumberFormatException e) {
			flag = false;
		}

		return flag;
	}

	private void exeCmd(String cmd)			//checks for the commands given by user and executes accordinly
	{

		if(cmd.contains("find")) {
			if(cmd.contains("find name")) 
				vector.showByName(split(cmd));
			else if(cmd.contains("find phone")) {
				String str = split(cmd);
				if(isLong(str))
					vector.showByPhoneNumber(Long.parseLong(str));
				else
					errors(2);
			} else if(cmd.contains("find comment"))
				vector.showByComment(split(cmd));
			else {
				errors(1);
				return;
			}	
		} else if(cmd.contains("add")) {
			String[] info = add();
			vector.addContact(vector.createContact(info));
		} else if(cmd.contains("remove")) {
			String[] s = cmd.split("\\s+");
			String name = "";
			for(int i=1; i<s.length; i++) 
				name = name + " " + s[i];
			vector.deleteContact(name.trim());
		} else if(cmd.contains("quit")) {
			vector.writeFinalToFile();
			quitFlag = true;
		} else {
			errors(1);
			return;
		}
	}

	public static void main(String args[])
	{
		Driver2.intro();

		Driver2 d2 = new Driver2(Driver2.getCommand());
		d2.vector.readInitialFromFile();
		
		while(!d2.quitFlag) {
			d2.exeCmd(Driver2.getCommand());
		}
				
	}
}
