/*
Programmer: Joshua Benz
Project# 2
Contact class Description: Provides the gettors and settors to create a contact with the given fields.
*/
public class Contact
{
	private String name;
	private long phoneNumber;
	private String comment;

	public Contact()
	{
		name = "";
		phoneNumber = 0;
		comment = "";
	}

	public void setName(String nm)
	{
		name += nm;
	}

	public void setPhoneNumber(long pN)
	{
		phoneNumber = pN;
	}

	public void setComment(String c)
	{
		comment += c;
	}

	public String getName()
	{
		return(name);
	}

	public String getComment()
	{
		return(comment);
	}

	public long getPhoneNumber()
	{
		return(phoneNumber);
	}

	public String toString()
	{
		return("\n" + name + ":" + phoneNumber + ":" + comment);
	}

	//toString function and 3 mutators
}
