/*
programmer: Joshua Benz
Project# 2
VectorOfContacts class Description: Creates a vector and stores the data from contacts.txt into the vector.
                                    It also has methods to add and retrive information based on the class fields.
*/

import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.PrintWriter;


public class VectorOfContacts implements projTwo
{

	
	private Scanner scan;

	private int size;
	private int capacity;			//class fields
	private int incrementCapacity;
	Contact[] people;

	public VectorOfContacts()              //constructors
	{
		incrementCapacity = 10;
		capacity = incrementCapacity;
		size = 0;
		people = new Contact[10];
	}

	public VectorOfContacts(int inCapacity)
	{
		incrementCapacity = inCapacity;
		capacity = incrementCapacity;
		size = 0;
		people = new Contact[capacity];
	}
                                                     //getters and setters
	public int getSize()
	{
		return size;
	}

	public void incrementSize() 
	{
		size++;
	}
	
	public boolean isEmpty()
	{
		return(size==0);
	}

	public boolean isFull()
	{
		return(size==capacity);
	}

	public void incrementCapacity()
	{
		Contact[] tmp = new Contact[capacity + incrementCapacity];
	
		for(int i=0; i<size; i++) {
			tmp[i] = people[i];
		}
		
		people = tmp;
		capacity += incrementCapacity;
	}
							//reads from file, saves to array and passes to addContact() with 
							// createContact() as a paramater, thus lodaing the data into the vector
	public void readInitialFromFile()
	{
		String fileName = Driver2.txt;
		String data = new String();
		String[] dataArray = new String[2];
		try {
			scan = new Scanner(new File(fileName));
			
			while(scan.hasNextLine()) {
				data = data.concat(scan.nextLine());
				dataArray = parseContact(data); 
				addContact(createContact(dataArray));

				data = "";
				dataArray = new String[2];
				
			}
		scan.close();
		}
		catch(FileNotFoundException e) {
			System.out.println("No such File!");
			System.exit(0);
		}

	}
	
	public Contact createContact(String[] sA)		//takes the array, creates contact object and returns it
	{
		Contact contact = new Contact();
		
		contact.setName(sA[0]);
		contact.setPhoneNumber(Long.parseLong(sA[1]));
		contact.setComment(sA[2]);

		return contact;
	}

	private static String[] parseContact(String contactInfo)	//parses the contacts.txt and returns an array 
	{
		String[] info = contactInfo.split(":");
			
		return info;
	}

								
	public void writeFinalToFile()					//writes the data in the vector to contacts.txt
	{
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(Driver2.txt);
			System.out.println("Writing contacts.txt");
		
			for(int i=0; i<size; i++){
				writer.print(people[i].getName() + ":" +people[i].getPhoneNumber() + ":" + people[i].getComment() + "\n");
			}
		} catch(FileNotFoundException e) {
			File file = new File("output.txt");
		}
	}

	public void addContact(Contact c)		//adds contact object to vector
	{
		 if(isFull())
                        incrementCapacity();

                people[size] = c;
                size++;

	}

	private void shift(int pos)			//shifts the elements to the left at a given position
	{
		for(int i=pos+1; i<size; i++) {
			people[i-1] = people[i];
		}
	}

	private int getIndex(String nm)			//gives the index of the name matching the passed parameter 
	{
		int index = -1;
		System.out.println(nm);
		for(int i=0; i<size; i++) {
			if(people[i].getName().toLowerCase().contains(nm))             
				index = i;
		}
		return index;
	}

	public void deleteContact(String nm)  		//deletes a contact and calls shift() to adjust the vector
	{
		nm = nm.toLowerCase();
		int index = getIndex(nm);

		if(index == -1) {
			System.out.println(nm + " does not exist!");
			return;
		}

		if(nm == "" || nm == "\\s+") {
			System.out.println("Nothing to remove");
			return;
		}

		if(size != 0) {

			if(people[index].getName().toLowerCase().contains(nm)) {
	        		System.out.println("Removed: " + people[index].toString());
				people[index] = null;
				shift(index);
				size--;
			} else {
				System.out.println(nm + " does not exist!");
				return;
			}
		} else {
			System.out.println("Contact book is empty!");
			return;
		}
	}

	public void showByName(String nm)
	{
		nm = nm.toLowerCase();
		int index = getIndex(nm);

		if(index == -1) {
			System.out.println(nm + " does not exist!");
			return;
		}

		if(people[index].getName().toLowerCase().contains(nm))
			System.out.println(people[index].toString());
	}

	public void showByPhoneNumber(long pN)
	{
		int index = -1;

		for(int i=0; i<size; i++) {
			if(people[i].getPhoneNumber() == pN)
				index = i;
		}

		if(index == -1) {
			System.out.println(pN + " does not exist!");
			return;
		}
		
		if(people[index].getPhoneNumber() == pN)
			System.out.println(people[index].toString());
		else {
			System.out.println(pN + " does not exist!");
			return;
		}
	}

	public void showByComment(String c)
	{
		int[] index = new int[size];
		int count = 0;
		index[0] = -1;
		
		for(int i=0; i<size; i++) {
			if(people[i].getComment().contains(c)) {
				index[count] = i;
				count++;
			}
		}

		if(index[0] == -1) {
			System.out.println("\"" + c + "\"" + " does not exist!");
			return;
		}

		for(int j=0; j<count; j++) {
			if(people[index[j]].getComment().contains(c))
				System.out.println(people[index[j]].toString());
		}
	}
}
