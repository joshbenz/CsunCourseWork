/* John Noga COMP182 Lab Exam 2 
 * Fall 2012
 */

import java.util.*;

public class LinkedListOfInts {
   Node head;


/* FROM HERE - FROM HERE - FROM HERE - FROM HERE - FROM HERE */


	public boolean isSortedEitherWay() {
		Node curr = head.getNext();
		Node prev = head;
		boolean flag = true;

		while(curr != null) {
			if(curr.getValue() >= prev.getValue()) {
				prev = prev.getNext();
				curr = curr.getNext();
				flag = true;
			
			} else {
				flag = false;
				curr = null;
			}
		}

			curr = head.getNext();
			prev = head;
			while(curr != null) {
				if(curr.getValue() <= prev.getValue()) {
					prev = prev.getNext();
					curr = curr.getNext();
					flag = true;
				} else {
					flag = false;
					curr = null;
				}
			} 
		return flag;
	}


	public void swapNodes(int i, int j) {
		
		Node first = head;
		Node second = head;
		Node curr = head;
		int count = 0;
/*
			if(i > j) {
				int tmp = j;
				j = i;
				i = tmp;
			} */

			while(curr != null) {
				if(count < i) 
					first = first.getNext();
				
				if(count < j)
					second = second.getNext();

				count++;
				curr = curr.getNext();
				System.out.println(count);
			}

			curr = head;
			curr.setNext(first.getNext().getNext());
			first.getNext().setNext(second.getNext().getNext());
			second.setNext(curr.getNext());
	}


   /* You can use the yourMainMethod method to do any testing you'd
      like to do (none is also allowed). It will be automatically
      called when you run the class without any command line
      parameters.
    */
   public static void yourMainMethod() {
    

	LinkedListOfInts yourList = new LinkedListOfInts();
        yourList.insert(5);
        yourList.insert(4);
        yourList.insert(3);
        yourList.insert(2);
	
			
	yourList.swapNodes(0,2);

	yourList.toString();

	//System.out.println(yourList.isSortedEitherWay());

	  /*
       * LinkedListOfInts yourList = new LinkedListOfInts();
       * yourList.insert(3);
       * yourList.insert(12);
       * yourList.insert(-2);
       * yourList.insert(7);
       * yourList.insert(-3);
       * yourList.insert(2);
       * yourList.insert(4);
       * System.out.println(yourList);
       */
   }


/* TO HERE - TO HERE - TO HERE - TO HERE - TO HERE - TO HERE */


   public LinkedListOfInts() {
      head = null;
   }

   public void insert(int n) {
      Node nd = new Node(n);
      nd.setNext(head); 
      head = nd;
   }

   public String toString() {
      String str = "[";
      Node n = head;
      while (n != null) {
         str += n.getValue() + " ";
         n = n.getNext();
      }
      str += "]";
      return str;
   }

   public static void main(String[] args) {
      LinkedListOfInts theList;
      int n, number,trials = 10;

      if (args.length==0)
         yourMainMethod();
      else { /*
         n = Integer.parseInt(args[0]);
         Random r = new Random(n);
         for (int i=0; i<trials; i++) {
            theList = new LinkedListOfInts();
            number = r.nextInt(10);
            for (int j=0; j<number; j++) 
               theList.insert(r.nextInt(i/2+5));
            System.out.println(theList);
            System.out.println(theList.isSorted());
            theList.squareAllValues();
            System.out.println(theList);
            theList.removeEvenValues();
            System.out.println(theList+"\n");
       
	 }*/
      } 
   }
}

class Node {
   private int value;
   private Node next, prev;

   public Node(int v) {
      value = v; 
      next = null;
   }
   public int  getValue() {
      return value;
   }
   public Node getNext() {
      return next;
   }
   public void setNext(Node nd) {
      next = nd;
   }
   public void setValue(int v) {
      value = v;
   }
}

