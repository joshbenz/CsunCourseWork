//Name: Joshua Benz
//SID:105284931 

/* 
Project#: 3
Destription: Contains all the data to be saved for a Dog
*/

public class Dog {
	private String name;
	private int tagNumber;
	private String breed;
	private String color;
	private String owner;
	private int incedents;


	public Dog()
	{
		name = new String();
		tagNumber = 0;
		breed = new String();
		color = new String();
		owner = new String();
		incedents = 0;
	}

	public String toString()
	{
		String s = new String();

		if(breed.equals("")) {					//How to print if there is no breed
			s = name + ", " + tagNumber + ", " 	
				+ color + ", " + owner
                                + ", " + incedents;
		} else {
			s = name + ", " + tagNumber + ", " + breed	//Normal way to print
				+ ", " + color + ", " + owner 
				+ ", " + incedents;
		}
		return s;
	}

	public void setName(String nm) 
	{ 
		name = nm; 
	}

	public void setTagNumber(int tn) 
	{ 
		tagNumber = tn; 
	}

	public void setBreed(String b) 
	{ 
		breed = b; 
	}

	public void setColor(String c) 
	{ 
		color = c; 
	}

	public void setOwner(String o) 
	{ 
		owner = o; 
	}

	public void setIncedents(int i)
	{
		incedents = i;
	}

	public void incrementIncedents() 
	{ 
		incedents++; 
	}

	public String getName()
	{
		return name;
	}

	public int getTagNumber()
	{
		return tagNumber;
	}

	public String getBreed()
	{
		return breed;
	}

	public String getColor()
	{
		return color;
	}

	public String getOwner()
	{
		return owner;
	}

	public int getIncedents()
	{
		return incedents;
	}
}
