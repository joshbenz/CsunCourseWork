//Name: Joshua Benz
//SID:105284931

/*
Description: Node class that stores the dog object and the next and prev references
*/

public class Node {
	
	private Dog d;
	private Node prev;
	private Node next;


	public Dog getDog()
	{
		return d;
	}

	public Node getPrev()
	{
		return prev;
	}

	public Node getNext()
	{
		return next;
	}

	public void setDog(Dog dog)
	{
		d = dog;
	}

	public void setNext(Node n)
	{
		next = n;
	}

	public void setPrev(Node p)
	{
		prev = p;
	}

}
