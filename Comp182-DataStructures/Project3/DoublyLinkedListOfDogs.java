//Name: Joshua Benz
//SID:105284931

/*
Specification: Your DoublyLinkedListOfDogs class (which is similar to DList class from the lecture) will be a list of Dogs. It will
need fields: Node head, Node tail, and int size. It should have a no-arg constructor and implement the ProjThree
interface below.

Description: Uses Doubly Linked list data structure to store Dog objects.
*/

import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.PrintWriter;

public class DoublyLinkedListOfDogs implements ProjThree {

	private int size;	//fields
	private Node head;
	private Node tail;

        private Scanner scan;


	public DoublyLinkedListOfDogs()
	{
		size = 0;
		head = null;
		tail = null;
	}

	public boolean isEmpty()	//checks if the list is empty
	{
		return head == null;
	}
	public void readInitialFromFile()	//Reads from Dogs.txt and places in the linked list
	{
		String fileName = Driver3.theTxt;
           	String data = new String();
                String[] dataArray = new String[6];

                try {
                        scan = new Scanner(new File(fileName));

                        while(scan.hasNextLine()) {
                                data = data.concat(scan.nextLine());
                                addDog(createDog(parseDogInfo(data)));

                                data = "";
                                dataArray = new String[6];

                        }
                scan.close();
                }
                catch(FileNotFoundException e) {
                        System.out.println("No such File!");
                        System.exit(0);
                }

	}

        public Dog createDog(String[] sA)		//places the parsed information into the Dog object fields
        {
                Dog dog = new Dog();

                dog.setName(sA[0].trim());
                dog.setTagNumber(Integer.parseInt(sA[1].trim()));
                dog.setBreed(sA[2].trim());
                dog.setColor(sA[3].trim());
                dog.setOwner(sA[4].trim());
                dog.setIncedents(Integer.parseInt(sA[5].trim()));

                return dog;
        }


        private static String[] parseDogInfo(String dogInfo)		//parses the Dog data from file, or via add funstion
        {								//handles the special case of no breed as well
                String[] info = new String[6];
                info = dogInfo.split(",");

                if(info.length == 5) {                          //checks size of the array
                        String[] tmp = new String[6];           //create the new temporary array
                        tmp[2] = "";                            //set the place holder (empty string) for the empty breed
                        for(int i=0,j=0; j<tmp.length; i++) {   //TWO counters here
                                if(tmp[i] == "")                //finds the place holder
                                        j++;                    //skips the element with the place holder
                                tmp[j] = info[i];               //counter j keeps track of the tmp array
                                j++;                            //counter i keeps track of the info array
                                }
                        info = tmp;                             //change reference to the corectly formatted array

                }

                return info;
        }


	public void addDog(Dog d)		//adds dog to the list and updates size
	{
		Node node = new Node();
		node.setDog(d);


		if(head == null) {
			head = node;
			tail = head;
		} else {
			head.setPrev(node);
			node.setNext(head);
			head = node;
		}

		size++;					
	}

	private void deleteAt(int pos)		//deletes by adjusting references so that previos dog and next dog point
	{					// point to each other
		if(pos == 1) {
			if(size == 1) {		//if there is only one dog in the list
				head = null;
				tail = null;
				size = 0;
				return;
			}

			head = head.getNext();
			head.setPrev(null);
			size--;
		}

		if(pos == size) {		//if it's the last dog in the list
			tail = tail.getNext();
			tail.setPrev(null);
		}

		Node node = head.getNext();
		
		for(int i=2; i<size; i++) {
			if(i == pos) {
				Node p = node.getPrev();
				Node n = node.getNext();

				//size--;
				return;
			}
		node = node.getNext();
		}
	}


	public void deleteDog(int tgNm)
	{
		int index = 1;
		Node curr = head;
								//checks if its empty and looks for the dog to delete
		if(!isEmpty()) {				// based on tag number
			while(curr != null) {
				if(curr.getDog().getTagNumber() == tgNm) {
					System.out.println("DLL:\n" + curr.getDog().toString());
					System.out.println("**** TEN INCIDENTS REMOVE FROM OWNER AND CITY");
					deleteAt(index);
					size--;
					break;
				}
				index++;
				curr = curr.getNext();	
			}
		} else
			System.out.println("No dogs!");
	}

	public void showByTag(int tgNm)			//goes through the list looking for matching tag number
	{
		int count = 0;
		int index = 1;
		Node curr = head;
		
		while(curr != null) {
			if(curr.getDog().getTagNumber() == tgNm) {
				System.out.println(index + ": " + curr.getDog().toString());
				count++;
			}
			index++;
			curr = curr.getNext();
		}
		if(count == 0)
			System.out.println(tgNm + " does not exist!");
	}

	public void showByBreed(String br)		//goes through list and looks for the breed
	{
		int count = 0;
		int index = 1;
		Node curr = head;
		
		while(curr != null) {
			if(br.equals(curr.getDog().getBreed())) {
				System.out.println(index + ": " + curr.getDog().toString());
				count++;
			}
			index++;
			curr = curr.getNext();
		}
		if(count == 0)
			System.out.println("No dogs by the breed of " + br);
	}

	public void showByColor(String col)		//goes through the list and prints the dog based on the specified color
	{
		int count = 0;
		int index = 1;
		Node curr = head;

		while(curr != null) {
			if(col.equals(curr.getDog().getColor())) {
				System.out.println(index + ": " + curr.getDog().toString());
				count++;
			}
			index++;
			curr = curr.getNext();
		}	
		if(count == 0)
			System.out.println("No dogs by the color of " + col);
	}

	public void showByBreedAndColor(String br, String col)		//goes through the list and prints based on the specified color and breed
	{
		int count = 0;
		int index = 1;
		Node curr = head;

		while(curr != null) {
			if((col.equals(curr.getDog().getColor()) && br.equals(curr.getDog().getBreed())) ||           //made parameters interchangeable
                                (col.equals(curr.getDog().getBreed()) && br.equals(curr.getDog().getColor()))) {
				
				System.out.println(index + ": " + curr.getDog().toString());
				count++;
			}
			curr = curr.getNext();
			index++;
		}
		if(count == 0)
			System.out.println("No " + col + " " + br + " dogs");
	}
	
	public void incrementIncedents(int tgNm)		//increments the incedent count of the dog with specified tag number
	{
		int count = 0;
		int index = 1;
		Node curr = head;

		while(curr != null) {
			if(curr.getDog().getTagNumber() == tgNm) {
				curr.getDog().incrementIncedents();
				System.out.println(index + ": " + curr.getDog().toString());
				count++;
			}
			curr = curr.getNext();
			index++;
		}
		if(count == 0)
			System.out.println(tgNm + " does not exist!");
	}

	public void showAll()					//uses the head to print all the dogs in the list
	{
		if(head.getNext() == null) 
			System.out.println(head.getDog().toString());
		
		Node curr = head;
		
		while(curr != null) {
			System.out.println(curr.getDog().toString());
			curr = curr.getNext();
		}
	}

	public void showAllReversed()				//uses the tail to print all the dogs in the list reversed
	{
		if(head.getNext() == null)
			System.out.println(head.getDog().toString());

		Node curr = tail;
		
		while(curr != null) {
			System.out.println(curr.getDog().toString());
			curr = curr.getPrev();
		}
	}
}
