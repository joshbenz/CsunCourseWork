//Name: Joshua Benz
//SID: 105284931


import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.PrintWriter;

public class VectorOfDogs implements ProjThree {

	private Scanner scan;

	private int size;
	private int capacity;
	Dog[] theDogs;

	public VectorOfDogs()
	{
		capacity = 10;
		size = 0;
		theDogs = new Dog[capacity];
	}

	public int getSize()
	{
		return size;
	}

	public void readInitialFromFile()		//reads from dogs.txt and places the into into the Dog object
	{						// and places the dog into the vector
		String fileName = Driver3.theTxt;
		String data = new String();
                String[] dataArray = new String[6];

                try {
                        scan = new Scanner(new File(fileName));

                        while(scan.hasNextLine()) {
                                data = data.concat(scan.nextLine());
                                addDog(createDog(parseDogInfo(data)));

                                data = "";
                                dataArray = new String[6];

                        }
                scan.close();
                }
                catch(FileNotFoundException e) {
                        System.out.println("No such File!");
                        System.exit(0);
                }

	
	}

	private void incrementCapacity()		//adds to the capacity of the vector
	{
		Dog[] tmp = new Dog[capacity + 10];

		for(int i=0; i<size; i++) {
			tmp[i] = theDogs[i];
		}

		theDogs = tmp;
		capacity += 10;
	}

	public boolean isFull()				//checks if the Vector is full
	{
		return(size==capacity);
	}

	public Dog createDog(String[] sA)		//creates the dog object with the parsed info
	{
		Dog dog = new Dog();		
	
		dog.setName(sA[0].trim());
		dog.setTagNumber(Integer.parseInt(sA[1].trim()));
		dog.setBreed(sA[2].trim());
		dog.setColor(sA[3].trim());
		dog.setOwner(sA[4].trim());
		dog.setIncedents(Integer.parseInt(sA[5].trim()));

		return dog;
	}
	
	private static String[] parseDogInfo(String dogInfo)	//parses the input from the parameter and handles the sepcial case
	{
		String[] info = new String[6];
		info = dogInfo.split(",");

		if(info.length == 5) {				//checks size of the array
			String[] tmp = new String[6];		//create the new temporary array
			tmp[2] = "";				//set the place holder (empty string) for the empty breed
			for(int i=0,j=0; j<tmp.length; i++) {   //TWO counters here
				if(tmp[i] == "")		//finds the place holder
					j++;			//skips the element with the place holder
				tmp[j] = info[i];		//counter j keeps track of the tmp array
				j++;				//counter i keeps track of the info array
				}
			info = tmp;				//change reference to the corectly formatted array

		}

		return info;
	}

	public void addDog(Dog d)		//adds the dog to the Vector of dogs
	{
		if(isFull())
			incrementCapacity();

		theDogs[size] = d;
		size++;
	}

	public void deleteDog(int tgNm)		//removes a dog if the incedent count is 10 or above
	{
		int pos = -1;
		for(int i=0; i<size; i++) {
			if(theDogs[i].getTagNumber() == tgNm) {
				System.out.println("Vector:\n" + theDogs[i].toString());
				System.out.println("**** TEN INCIDENTS REMOVE FROM OWNER AND CITY");
				pos = i;
			}
		}		
		
		if(pos > -1) {
			theDogs[pos] = theDogs[size-1];
			theDogs[size-1] = null;
			size--;
		}
	}

	public void showAllReversed()		//prints all the dogs in the vector in reverse
	{
		for(int i=size-1; i>=0; i--) {
			System.out.println(theDogs[i].toString());
		}
	}
	
	public void showAll()			//prints all the dogs in the Vector
	{
		for(int i=0; i<size; i++) 
			System.out.println(theDogs[i].toString()); 
		
	}

	public void showByTag(int tgNm)		//prints the dog according to tag number
	{
		int count = 0;

		for(int i=0; i<size; i++) {
			if(tgNm == theDogs[i].getTagNumber()) {
				System.out.println((i+1) + ": " + theDogs[i].toString());
				count++;
			}
		}
			if(count == 0)
				System.out.println(tgNm + " does not exist!");
	}

	public void showByBreed(String br)	//prints the dog according to breed
	{
		int count = 0;
		
		for(int i=0; i<size; i++) {
			if(br.equals(theDogs[i].getBreed())) {
				System.out.println((i+1) + ": " + theDogs[i].toString());
				count++;
			}
		}
			if(count == 0)
				System.out.println("No dogs by the breed of " + br);		
	}

	public void showByColor(String col)	//prints the dog according to color
	{
		int count = 0;		

		for(int i=0; i<size; i++) {
			if(col.equals(theDogs[i].getColor())) {
				System.out.println((i+1) + ": " + theDogs[i].toString());
				count++;
			}
		}
			if(count == 0)
				System.out.println("No dogs by the color of " + col);
	}
	
	public void showByBreedAndColor(String br, String col)		//prints the dog according to breed and color
	{
		int count = 0;	

		for(int i=0; i<size; i++) {
			if((col.equals(theDogs[i].getColor()) && br.equals(theDogs[i].getBreed())) ||		//made parameters interchangeable
				(col.equals(theDogs[i].getBreed()) && br.equals(theDogs[i].getColor()))) {
				System.out.println((i+1) + ": " + theDogs[i].toString());
				count++;
			}
		}
			if(count == 0)
				System.out.println("No " + col + " " + br + " dogs");
	}

	public int checkIncedents()	//checks ths incedent count of all dogs
	{
		int tag = 0;
		
		for(int i=0; i<size; i++) {
			if(theDogs[i].getIncedents() >= 10)
				tag = theDogs[i].getTagNumber();
		}

		return tag;
	}

	public void incrementIncedents(int tgNm)	//increments the incedent of a dog
	{
		int count = 0;

		for(int i=0; i<size; i++) {
			if(tgNm == theDogs[i].getTagNumber()) {
				theDogs[i].incrementIncedents();
				System.out.println((i+1) + ": " + theDogs[i].toString());
				count++;
			}
		}
		if(count == 0)
			System.out.println(tgNm + " does not exist!");
	}
}
