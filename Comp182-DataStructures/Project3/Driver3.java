//Name: Joshua Benz
//SID:105284931

/*
Description: The program driver, parses commands and executes accordingly
*/

import java.util.Scanner;

public class Driver3 {
	
	private DoublyLinkedListOfDogs list;
	private VectorOfDogs vec;
	public static String theTxt = "dogs.txt";

	public Driver3()
	{
		vec = new VectorOfDogs();
		list = new DoublyLinkedListOfDogs();
	}

        public static String getCommand()
        {
                Scanner scan = new Scanner(System.in);          //gets the users command

                System.out.print("> ");
                String cmd = scan.nextLine().trim();
                return cmd;
        }

	private String[] add()					//gets the input and returns it as a string array
	{
		String[] sA = new String[6];
		System.out.println("Name?");
		sA[0] = getCommand();

		System.out.println("Tag Number?");
		sA[1] = getCommand();

		System.out.println("Breed?");
		sA[2] = getCommand();

		System.out.println("Color?");
		sA[3] = getCommand();

		System.out.println("Owner");
		sA[4] = getCommand();
	
		sA[5] = "0";

		return sA;
	}

        private String split(String c)                  //splits at a space, rejoins the needed parts, returns string
        {
		String str = "";

		if(c.contains("and")) {
			String[] arr = c.split("\\s+");
			str = arr[2] + " " + arr[5];
		} else {
                	String[] sA = c.split("\\s+");

                	for(int i=2; i<sA.length; i++)
                        	str = " " + sA[i];
		}
                return(str.trim());
        }

        private boolean isInt(String s)                //checks if the phone number entered can be parsed
        {
                boolean flag = false;
                try {
                        int l = Integer.parseInt(s);
                        flag = true;
                } catch(NumberFormatException e) {
                        flag = false;
                }

                return flag;
        }


	private void checkIncedentNumber()		//check the incedent count of both data structures
	{						//and delete dog if it is <= 10
		int tag = vec.checkIncedents();
                if(tag != 0 ) {
                        vec.deleteDog(tag);
                        list.deleteDog(tag);
                }

	}

	private void exeCmd(String cmd)                 //checks for the commands given by user and executes accordinly
        {
		checkIncedentNumber();		

		cmd = cmd.toLowerCase();
                if(cmd.contains("find")) {
                        if(cmd.contains("find tag")) {
				String s = split(cmd);

				if(isInt(s)) {
					System.out.println("Vector:");
                                	vec.showByTag(Integer.parseInt(s));
				
					System.out.println("DLL: ");
					list.showByTag(Integer.parseInt(s));
				} else 
					System.out.println("Invalid tag number!");
			} else if(cmd.contains("and")) {				//the find breed and color checks for "and" for validation
				String[] sA = (split(cmd)).split(" ");

				System.out.println("Vector:");
				vec.showByBreedAndColor(sA[0], sA[1]);

				System.out.println("DLL: ");
				list.showByBreedAndColor(sA[0], sA[1]);

                        } else if(cmd.contains("find breed")) {
				System.out.println("Vector:");
                                vec.showByBreed(split(cmd));

				System.out.println("DLL: ");
				list.showByBreed(split(cmd));

                        } else if(cmd.contains("find color")) {
				System.out.println("Vector:");
                                vec.showByColor(split(cmd));

				System.out.println("DLL: ");
				list.showByColor(split(cmd));
                        } else 
                                return;
        
                } else if(cmd.contains("add")) {
                        String[] info = add();

                        vec.addDog(vec.createDog(info));
			list.addDog(vec.createDog(info));

		} else if(cmd.equals("show dogs")) {
			System.out.println("Vector:");
			vec.showAll();

			System.out.println("DLL: ");
			list.showAll();

		} else if(cmd.equals("show reversed")) {
			System.out.println("Vector:");
			vec.showAllReversed();

			System.out.println("DLL: ");
			list.showAllReversed();

                } else if(cmd.contains("report incedent")) {
			String s = split(cmd);
	
			if(isInt(s)) {
				System.out.println("Vector:");
				vec.incrementIncedents(Integer.parseInt(s));

				System.out.println("DLL: ");
				list.incrementIncedents(Integer.parseInt(s));

			checkIncedentNumber();
			} else
				System.out.println("Invalid input!");
		} else if(cmd.equals("quit")) {
                        System.exit(0);
                } else {
                        System.out.println("Not a valid Command.");
			return;
                }
        }


	public static void main(String[] args)
	{
		Driver3 d3 = new Driver3();
		d3.vec.readInitialFromFile();
		d3.list.readInitialFromFile();

		while(true) {
			d3.exeCmd(Driver3.getCommand());		
		}
	}
}
