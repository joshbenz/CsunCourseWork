//Name: Joshua Benz
//SID:105284931


/*
	Description: Interface for the Vector of Dogs
*/

public interface ProjThree
{
	public void readInitialFromFile();
	public void addDog(Dog d);
	public void deleteDog(int tgNm);
	public void showByTag(int tgNm);
	public void showByBreed(String br);
	public void showByColor(String col);
	public void showByBreedAndColor(String br, String col);
	public void showAll();
	public void showAllReversed();
}
