/*
Programmer: Joshua Benz
Project: 24 slide puzzle
Project Description: 2 Dimensional, text-based puzzle. The objective is to put all 
                     of the numbers in numerical order.
Class Description: Interacts with the user by promptin and getting input. Parsing
                   the input and printing any errors that may occur.
*/

import java.util.Scanner;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.File;

public class Driver1 {

   //private static int[][] outFrame;   // these will be used to copy the ending frame
   private static int x,y;                  // to write to Output.txt
   
   public Driver1() {}

//Print the valid commands available

   public static void commands() {
      
      System.out.println("\nWelcome to 24 Puzzle!\n\nLegal Commands:\n\n" +
         "help - prints this helpful information\n" +
         "quit - ends the game\n" +
         "move # - which attempts to move the tile with that number into the empty position\n");

   }
//various messages to print depending on the situation

   public static void theCode(int code) {
      if(code == 0) {
         Driver1.commands();
      }
      if(code == 1) {
         System.out.println("Thanks for Playing!\n\n");
         System.out.println("Writing to Output.txt");
         Driver1.outputFile("Output.txt");
         System.exit(0);
      } 
      if(code == 2) {
         System.out.println("Not a legal Command!");
         return;
      }
   }

//returns the parsed integer inputed by user

   private static int parse(String in) {
      int rtn = 0;
      in = in.trim();
      in = in.toLowerCase();
      boolean flag = true;
      int counter = 0;   
 
      for(int i=0; i<in.length(); i++) {
         if(in.charAt(i) == ' ') {
            counter++;
         }
      }

      if(counter > 1) {
         flag = false;
      }

      if(in.contains("help")) {
         Driver1.theCode(0);
      } else if(in.contains("quit")) {
         Driver1.theCode(1);
      } else if(in.contains("move") && in.contains(" ")) {
        String[] s = in.split(" ");
        s[1].trim();

        for(int i=0; i<s[1].length(); i++) {
           char tmp = s[1].charAt(i);
           if(!Character.isDigit(tmp)) {
              flag = false;
           }
        }
        
        if(flag) {     
           rtn = Integer.parseInt(s[1]);
        } else {
           System.out.println("Must be an integer!");
        }

      } else {
        Driver1.theCode(2);
      }

      return rtn;
     
   }

//prompts and gets input from the user

   private int getInput() {
      int num = 0;
      Scanner in = new Scanner(System.in);
      
      System.out.print("command: ");
      String input = in.nextLine();
      num = parse(input);

      return num;
   }

//output the last frame to Output.txt file one the quit command

    public static void outputFile(String fileName) {
      PrintWriter writer = null;// = new PrintWriter ("Output.txt", "UTF-8");
      
      try {

      writer = new PrintWriter("Output.txt");
      for(int i=0; i<x; i++) {
         for(int j=0; j<y; j++) {
            writer.print(Board.outFrame[i][j] + " ");
            if(j == 4) {
               writer.print("\n");
            }
         }
      }
      }
      catch(FileNotFoundException e) {
         File file = new File("Output.txt");
      }
      writer.close();
   }

   public static void main(String[] args) {
      Board b = new Board();
      Driver1 d1 = new Driver1();

      Driver1.commands();
      b.readInitialBoard("input.txt");
      b.showBoard();
  
      x = b.SIZE_X;          //dimensions of board used by outputFile()
      y = b.SIZE_Y;

      while(!b.isCorrect()) { 
        b.makeMove(d1.getInput());
        b.updateOutFrame();
        b.showBoard();
      }
      System.out.println("Congrats! You have solved the 24 puzzle. Thanks for playing!");
   }
}//end Driver1
