/*
Programmer: Joshua Benz
Class Description: Create and control the text-based 2 dimensional board. 
*/


import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.File;

public class Board {
   private int[][] the_frame; 
   private Scanner scan;             //fields and constants
   public final int SIZE_X;
   public final int SIZE_Y;
   public static int[][] outFrame;

   public Board() { 
      SIZE_X = 5;     //constant dimensions set
      SIZE_Y = 5;
      the_frame = new int[SIZE_X][SIZE_Y];
   }


//read in board data from input.txt

   public void readInitialBoard (String fileName) { 
      int data = 0;
      System.out.println("Loading board...");
      try {
         scan = new Scanner(new File(fileName));
  
         while(scan.hasNextInt()) {
            for(int j=0; j<SIZE_X; j++) {
               for(int l=0; l<SIZE_Y; l++) {
                  data = scan.nextInt();
                  the_frame[j][l] = data;
               }
            }        
         }
         scan.close();   
      }   
      catch (FileNotFoundException e) {
         System.out.println("No such File!");
         System.exit(0);
      } 
   }

//Display data as a 2D text-based board

   public void showBoard () {
      System.out.print(" -----------------------------");
      int counter = 0;     

      for(int l=0; l<SIZE_X; l++) {
         
         System.out.println();       
         for(int a=0; a<SIZE_Y; a++) { 
      
            counter++;
            if(the_frame[l][a] <= 9) {
               if(the_frame[l][a] == 0) {
                  System.out.print("|  " + " " + "  ");
              } else {
                  System.out.print("|  " + the_frame[l][a] + "  ");
              }
               if(a == 4) {
                  System.out.println("|");
                  System.out.print("|-----------------------------|");
               }
            } else {
               System.out.print("| " + the_frame[l][a] + "  ");

               if(a == 4) {
                  System.out.println("|");
                  System.out.print("|-----------------------------|");
               }
            }
         }
      }
      if(counter != SIZE_X * SIZE_Y) {
        
         System.out.println("Not a valid input file!");
         System.exit(0);
      }
      System.out.println("\n\nCurrent Board\n");
   }   

  
//returns true if the board is solved

   public boolean isCorrect() {

      boolean flag = false;
      int counter = 1;   

      for(int i=0; i<SIZE_X; i++) {
         for(int j=0; j<SIZE_Y; j++) {
             if(the_frame[i][j] == counter) {
               counter++;
            }
         }
      }
      
      if(counter == (SIZE_X * SIZE_Y) && (the_frame[SIZE_X-1][SIZE_Y-1] == 0)) {
         flag = true;
      } 
      return flag;
   }

//return true if the move is within the bounds of the array

   private boolean checkSpace(int zeroX, int zeroY, int posx, int posy) {
      boolean flag = false;
   
      if((zeroX+1 == posx && zeroY == posy) || (zeroX-1 == posx && zeroY== posy) 
          || (zeroY+1 == posy && zeroX == posx) || (zeroY-1 == posy && zeroX == posx)) {
         flag = true;
      }

      return flag;
   }

//switches the neccesary elemnts in the array 

   public void makeMove(int number) {
    
      int posX = 0, posY = 0;
      int[] zero = new int[2];

      for(int i=0; i<SIZE_X; i++) {
         for(int j=0; j<SIZE_Y; j++) {

            if(the_frame[i][j] == number) {
               posX = i;
               posY = j;
            }
         }
      }

      for(int k=0; k<SIZE_X; k++) {
         for(int l=0; l<SIZE_Y; l++) {
      
            if(the_frame[k][l] == 0) {
              zero[0] = k;
               zero[1] = l;
               if(checkSpace(zero[0], zero[1], posX, posY)) {
                  the_frame[k][l] = number;
                  the_frame[posX][posY] = 0;
               } else {
                 return;
               }
            }
         }
      }
   }

   public void updateOutFrame() {
      outFrame = new int[SIZE_X][SIZE_Y];
      for(int i=0; i<SIZE_X; i++) {
         for(int j=0; j<SIZE_Y; j++) {
            //System.out.println(Board.outFrame[i][j]);
            Board.outFrame[i][j] = the_frame[i][j];
         }
      }
   }

/*
   public void outputFile(String fileName) {
      PrintWriter writer = new PrinterWriter("Output.txt", "UTF-8");
      int counter = 0;
      
      for(int i=0; i<SIZE_X; i++) {
         for(int j=0; j<SIZE_Y; j++) {
            writer.print(the_frame[i][j] + " ");
            if(counter == SIZE_X-1) {
               writer.println();
            }
         }
      }
      writer.close();
   }
*/
}//end Board
