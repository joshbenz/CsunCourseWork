
import java.util.*;

public class LinkedListOfInts {
   Node head;


/* FROM HERE - FROM HERE - FROM HERE - FROM HERE - FROM HERE */

   public void squareAllValues() {

	Node curr = head;

	while(curr != null) {
		curr.setValue(curr.getValue()*curr.getValue());
		curr = curr.getNext();
	}
   }

   public boolean isSorted() {
	Node curr = head;
	boolean flag = false;
	int count = 0;

	while(curr.getNext() != null) {
		if(curr.getValue() < curr.getNext().getValue())
			count++;
		curr = curr.getNext();
	}
		if(count == 0)
			flag = true;
	return flag;
   }

   public void removeEvenValues() {

	Node curr = head.getNext();
	Node trail = head;

		while(curr != null) {
			if(curr.getValue() % 2 == 0) {
				curr = curr.getNext();
				trail = trail.getNext();
			} else {
				curr = curr.getNext();
				trail.setNext(curr);
			}
		}
   }

   /* You can use the yourMainMethod method to do any testing you'd
      like to do (none is also allowed). It will be automatically
      called when you run the class.
    */
   public static void yourMainMethod() {

	
        LinkedListOfInts yourList = new LinkedListOfInts();
        yourList.insert(3);
        yourList.insert(12);
        yourList.insert(-2);
        yourList.insert(7);
        yourList.insert(-3);
        yourList.insert(2);
        yourList.insert(4);
        System.out.println(yourList.toString());
    	yourList.squareAllValues();
        System.out.println(yourList.toString());
	System.out.println(yourList.isSorted());
	yourList.removeEvenValues();
        System.out.println(yourList.toString());
 

   }


/* TO HERE - TO HERE - TO HERE - TO HERE - TO HERE - TO HERE */


   public LinkedListOfInts() {
      head = null;
   }

   public void insert(int n) {
      Node nd = new Node(n);
      nd.setNext(head); 
      head = nd;
   }

   public String toString() {
      String str = "[";
      Node n = head;
      while (n != null) {
         str += n.getValue() + " ";
         n = n.getNext();
      }
      str += "]";
      return str;
   }

   public static void main(String[] args) {

         yourMainMethod();

      }
   }


class Node {
   private int value;
   private Node next, prev;

   public Node(int v) {
      value = v; 
      next = null;
   }
   public int  getValue() {
      return value;
   }
   public Node getNext() {
      return next;
   }
   public void setNext(Node nd) {
      next = nd;
   }
   public void setValue(int v) {
      value = v;
   }
}
