/*
* Joshua Benz
* Comp 282 TT
* Assignment 3
* April 21, 2015
*/


import java.util.Random;

class ArraySorts {

   //Drivers, all take the array and array size

    public static void InsertionSort(int[] a, int n) {
        InsertionSort(a, 0, n-1);
    }

    public static void QuickSort1(int[] a, int n) {
        QuickSort1(a, 0, n-1);
     //   InsertionSort(a, (n-1) - 20, n-1);   

    }

    public static void QuickSort2(int[] a, int n) {
        QuickSort2(a, 0, n-1);
        //InsertionSort(a, endPoint[0], endPoint[1]);

    }

    public static void QuickSort3(int[] a, int n){
        QuickSort3(a, 0, n-1);
        //InsertionSort(a, (n-1) - 20, n-1); 

    }

    public static void QuickSort4(int[] a, int n){
        QuickSort4(a, 0, n-1);

    }

    public static void QuickSort5(int[] a, int n) {
        QuickSort5(a, 0, n-1);
        //InsertionSort(a, (n) - 500, n-1);

    }
  
    public static void QuickSort6(int[] a, int n) {
        QuickSort6(a,0, n-1);
    }

   // end Drivers

/**
*  InsertionSort: Use the left and right pointers as a range for the
*  array. "Insert" element into the sorted (left side of arrray) element
*  list. Once placed in position, shift the remaining elements down.
* 
*  @param a     The array to be operated on
*  @param left  starting index pointer of what needs to be sorted
*  @param right Ending index pointer of what needs to be sorted
*/
    private static void InsertionSort(int[] a, int left, int right) {
        int val, pos;        

        for(int i=left; i<right; i++) {
            val = a[i];                                 //position of value to insert
            pos = i;

            while((pos > 0) && a[pos-1] > val) {
                a[pos] = a[pos-1];                      //shifting
                pos--;
            }
            a[pos] = val;
        }
    }


/**
*  QuickSort1: book’s partition, random pivot, easiest case = 20
*  
*  @param a     Array to work on
*  @param start Beginning index of array portion to work on
*  @param end   end index of array portion to work on
*/
    private static void QuickSort1(int[] a, int start, int end) { 
        while(start < end && end-start >= 20) {
            int index = partition1(a, start, end, true);     //get middle index that splits array
        
            if((index-1)-start < end-(index+1)) {             //find which partition is smaller
                QuickSort1(a, start, index-1);                //left is smaller sort left
                start = index + 1;                           //QuickSort(a, index+1, end)
            } else {                                         //right is smaller, sort right
                QuickSort1(a, index+1, end);                 //sort right    
                end = index - 1;                            //QuickSort1(a, start, index-1)
            }
        }
        if(end-start < 20) {
            InsertionSort(a, start, end+1);
        }
    }

/**
*  QuickSort2: 2 ptr partition, random pivot, easiest case = 20
*
*  @param  a      The array to be operated on
*  @param  start  Beginning index of array
*  @param  end    Ending index of array
*
*/
    private static void QuickSort2(int[] a, int start, int end) {
        int pivotIndex = randPivot(start, end);               //generate random pivot
        int index = partition2(a, start, end, a[pivotIndex]); //partition and save index where splits

        if(end - start < 20) {
            InsertionSort(a, start, end+1);    //easy case
        } else {

            if(start < index-1) {                 //Quicksort left partition
               QuickSort2(a, start, index-1);
            }

            if(end > index) {                     //Quicksort right partition
                QuickSort2(a, index, end);
            }
        }
    }

/**
*  QuickSort3: book’s partition, random = a[lf], easiest case = 20
*
*  @param a     Array to work on
*  @param start Beginning index of array portion to work on
*  @param end   end index of array portion to work on
*/
    private static void QuickSort3(int[] a, int start, int end) {
        while(start < end && end-start >= 20) {
            int index = partition1(a, start, end, false);     //get middle index that splits array

            if((index-1)-start < end-(index+1)) {             //find which partition is smaller
                QuickSort3(a, start, index-1);                //left is smaller sort left
                start = index + 1;                           //QuickSort(a, index+1, end)
            } else {                                         //right is smaller, sort right
                QuickSort3(a, index+1, end);                 //sort right
                end = index - 1;                            //QuickSort1(a, start, index-1)
            }
        }

        if(end-start < 20) {
            InsertionSort(a, start, end+1);
        }
    }

/** 
*  QuickSort4: 2 ptr partition, random pivot, easiest case = 1
*
*  @param int[] a    The array to be operated on
*  @param int start  Beginning index of array
*  @param int end    Ending index of array
*
*/
    private static void QuickSort4(int[] a, int start, int end) {
        int pivotIndex = randPivot(start, end);               //generate random pivot
        int index = partition2(a, start, end, a[pivotIndex]); //partition and save index where splits
  
        if(start < index-1) {                 //Quicksort left partition
            QuickSort4(a, start, index-1);
        }

        if(end > index) {                     //Quicksort right partition
            QuickSort4(a, index, end);
        } 
    }

/**           
*  QuickSort5: 2 ptr partition, random pivot, easiest case = 500
*
*  @param int[] a    The array to be operated on
*  @param int start  Beginning index of array
*  @param int end    Ending index of array
*
*/
    private static void QuickSort5(int[] a, int start, int end) {
        int pivotIndex = randPivot(start, end);               //generate random pivot
        int index = partition2(a, start, end, a[pivotIndex]); //partition and save index where splits

        if(end - start < 500) {
            InsertionSort(a, start, end+1);//easy case
        } else {

            if(start < index-1) {                 //Quicksort left partition
                QuickSort5(a, start, index-1);
            }

            if(end > index) {                     //Quicksort right partition
                QuickSort5(a, index, end);
            }
        }
    }

/**
*  QuickSort6: book’s partition, random pivot, easiest case = 1
*
*  @param a     Array to work on
*  @param start Beginning index of array portion to work on
*  @param end   end index of array portion to work on
*/
    private static void QuickSort6(int[] a, int start, int end) {
        while(start < end && end-start >= 1) {
            int index = partition1(a, start, end, true);     //get middle index that splits array
    
            if((index-1)-start < end-(index+1)) {             //find which partition is smaller
                QuickSort6(a, start, index-1);                //left is smaller sort left
                start = index + 1;                           //QuickSort(a, index+1, end)
            } else {                                         //right is smaller, sort right
                QuickSort6(a, index+1, end);                 //sort right
                end = index - 1;                            //QuickSort1(a, start, index-1)
            }
        }
    }


/**
*  partition1: the books partition method, swap pivot to beginning of array and
*  keep track of everything less than the pivot by swapping it with the last 
*  known element that was smaller than the pivot. Have an "unknown" pointer
*  move along the array, if the pivot it smaller than the current element
*  then just skip it. At the end we have 2 partitions, the left side is 
*  everything less than pivot, the right is everything greater than the pivot.
*  Also swap pivot with the element pointed to by the "less" pointer to preseve
*  ascending order.
*
*  @param a     The array to work on
*  @param start The beginning index of current array portion to work on
*  @param end   The ending index of the current array portion to work on
*  @param flag  Used to determine whether we want a random pivot or not. flase = random
*               true = left element as pivot
*  @return less the index of the pivot
*/

    private static int partition1(int[] a, int start, int end, boolean flag) {
        int pivot;
        if(flag) {                               //check which pivot is desired
            pivot = randPivot(start, end);   //get random pivot index
        } else {
            pivot = start;
        }

        swap(a,start, pivot);                //swap pivot to beginning
        pivot = a[start];                   //save pivot value
        int less = start;                   //pivot is only known element in the less list

        for(int uk = start+1; uk<=end; uk++) {      //go until last element
            if(a[uk] < pivot) {                     //unkown is less than pivot
                less++;
                swap(a, uk, less);                  //add the element to less than pivot list
            } else {
                //do nothing because it's bigger than pivot so it stays out of the less list
            }
        }
        swap(a, start, less);              //swap pivot back into original position
        return less;                      //return index where the array is split
    }

/**
*  Partition2: 2 ptr partition method. Moves 2 pointers (variables holding the current index)
*  along the array, swapping all values to the left or right side of array until the pointers
*  cross.
*
*  @param a      The array to be operated on
*  @param left   Everything smaller than or equal to the pivot goes to the left of this ptr
*  @param right  Everything larfer than pivot goes to right of this ptr
*  @return index The index where the partiton stopped
*/

    private static int partition2(int[] a, int left, int right, int pivot) {
        while(left <= right) {        //loop until pointers are equal
            while(a[left] < pivot) {  //Find value larger than pivot
                left++;               //increment pointer until the value is found
            }
        
            while(a[right] > pivot) { //Find value smaller than pivot
                right--;              //decrement since starting from the right side
            }

            if(left <= right) {       //swap the 2 values
                swap(a, left, right);
                left++;              //update pointers
                right--;
            }
        }
        return left;                 //return index that splits the partition
    }

/**
*  randPivot: Generates a random index between the given range using java.util.Random
*  @param min  The lowerbound of indicies allowed
*  @param max  The upperbound of indicies allowed
*  @return int The randomly generated number
*/
    private static int randPivot(int min, int max) {
return min + (int)(Math.random() * ((max - min) + 1));
        //return new Random().nextInt(max - min + 1) + min; 
               //Create Random object
               //pass in the positive upper bound, max-min +1
              //type cast to int using nextInt, put value in range by adding min
    }

/**
*  swap: swaps 2 values in an array. NOTE: this method works with indicies
*
*  @param a    The array to operate one
*  @param pos  index of a value to swap
*  @param pos1 index of a value to swap
*/
    private static void swap(int[] a, int pos, int pos1) {
        int tmp = a[pos];
        a[pos] = a[pos1];
        a[pos1] = tmp;
    }




/*************** Heap sort and helper methods ************/

    public static void HeapSort1(int[] a, int n) {
        heapSort1(a, n);
    }

    public static void HeapSort2(int[] a, int n) {
        heapSort2(a, n);
    }

/**
*  heapSort1: driver for HeapSort1, build heap bottom up
*
*  @param a The array to operate on
*  @param n The size of the array
*/
    private static void heapSort1(int[] a, int n) {
        bottomUp(a, n);                    //build heap bottom up
        int end = n-1;                     //end of array
     
        while(end > 0) {                  //iterate until heap is gone
            swap(a, end, 0);              //swap largest element to the end of heap+1
            end--;                        //mark the new end
            trickleDown(a, 0, end);       //trickle down
        }
    }

    private static void heapSort2(int[] a, int n) {
        topDown(a, n);                    //build heap top down
        int end = n-1;                     //end of array

        while(end > 0) {                  //iterate until heap is gone
            swap(a, end, 0);              //swap largest element to the end of heap+1
            end--;                        //mark the new end
            trickleDown(a, 0, end);       //trickle down
        }
 
    }

    //Getters for easy math

    private static int getLeft(int i) {    //return left child
        return 2 * i + 1; 
    }

    private static int getRight(int i) {   //return right child
        return 2 * i + 2; 
    }
  
    private static int getParent(int i) {  //return parent
        return (i-1) / 2;
    }

/**
*  bottomUp: used to build heap bottom up. Iterate through array, shift
*  the needed elements up by using trickle up to keep heap prperties.
*
*  @param a The array to operate on
*  @param n The size of the array
*/

    private static void bottomUp(int[] a, int n) {
        for(int i=0; i<n; i++) {
            trickleUp(a, i);
        }
    }

    private static void topDown(int[] a, int n) {
        int start = ((n-1)/2);         //get the parent
 
        while(start >= 0) {             //trickle down
            trickleDown(a, start, n-1);
            start--;                   //go to next subtree
        }
    }

/**
*  trickleUp: Look at the parent and its subtree, shift elements
*  if there is an element larger than the current parent, then
*  Place that large element in the spot of the parent
*
*  @param a    The array to operate on
*  @param curr The current position of the root of the subtree
*/
    private static void trickleUp(int[] a, int curr) {
        int parent = getParent(curr);                     //get the parent
        int last = a[curr];                               //set the larger as the parent

        while(curr > 0 && a[parent] < last) {            //find something bigger than current parent
            a[curr] = a[parent];
            curr = parent;                                //move around elements
            parent = getParent(parent);
        }

        a[curr] = last;                                   //place larger element as parent
    }

/**
*  trickleDown: Look at the current root and it's subtree, find the biggest child
*  and replace the current root with it.
*
*  @param a        The array to operate on
*  @param curr     The current position and root of the subtree
*  @param currSize The end of the tree
*/
    private static void trickleDown(int[] a, int curr, int currSize) {
        int maxChild;
        int localRoot = curr;                             //save root of subtree

        while(getLeft(localRoot) <= currSize) {           //check for a child
            maxChild = getLeft(localRoot);                //assume that is the only child

            if(maxChild+1 <= currSize && a[maxChild] < a[maxChild+1]) {
                maxChild++;                              //if a right child exists and it's
            }                                            //bigger than the current max, aka the left
                                                         //child, then set left child as max
            if(a[localRoot] < a[maxChild]) {             
                swap(a, localRoot, maxChild);           //move the max to the current root if
                localRoot = maxChild;                   //it is bigger. set the new root reference
            } else {
                   localRoot = currSize;    //done, satisfy while loop condition to exit
            }
        }
    }


    public static String myName() {
        return "Joshua Benz";
    }



/*
    public static void main(String[] args) {
        int[] a = {4,5,66,98,43,76,12,15,20,18,32,37,49,7,4,7,8,9,34,65,2,1,3};
     for(int i=0; i<a.length; i++) {
            System.out.print(a[i] + " ");
        }
	System.out.println();

    //    ArraySorts.InsertionSort(a, a.length);
    //    ArraySorts.QuickSort1(a, a.length);
    //    ArraySorts.QuickSort2(a, a.length);
    //    ArraySorts.QuickSort3(a, a.length);
    //    ArraySorts.QuickSort4(a, a.length);
    //    ArraySorts.QuickSort5(a, a.length);
     //   ArraySorts.QuickSort6(a, a.length);
          ArraySorts.HeapSort2(a, a.length);
        for(int i=0; i<a.length; i++) {
	    System.out.print(a[i] + " ");
        }
    }*/
}
