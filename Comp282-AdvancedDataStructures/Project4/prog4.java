/*
*	Joshua Benz
*	Comp 282
*	Assignment 4
*	May 7, 2015
*/

import java.io.*; // for BufferedReader
import java.util.*; // for StringTokenizer

class Edge_Node {
	Vertex_Node target;
	Edge_Node next;

	public Edge_Node(Vertex_Node t, Edge_Node e) {
		target = t;
		next = e;
	}

	public Vertex_Node GetTarget() {
		return target;
	}


	public Edge_Node GetNext() {
		return next;
	}
}

class Vertex_Node {
	String name;
	Edge_Node edge_head;
	int distance;
	Vertex_Node next, parent;

	public Vertex_Node(String s, Vertex_Node v) {
		name = s;
		next = v;
		distance = -1;
	}

	public Vertex_Node GetParent() {
		return parent;
	}

	public void SetParent(Vertex_Node p) {
		parent = p;
	}

	public String GetName() {
		return name;
	}

	public int GetDistance() {
		return distance;
	}

	public void SetDistance(int d) {
		distance = d;
	}

	public Edge_Node GetNbrList() {
		return edge_head;
	}

	public Vertex_Node GetNext() {
		return next;
	}
}

class Graph {
	Vertex_Node head;
	int size;

	public Graph() {
		head = null;
		size = 0;
	}

	public Vertex_Node GetGraphHead() {
		return head;
	}

	public void clearDist() {
		Vertex_Node pt = head;
		while (pt != null) {
			pt.distance = -1;
			pt = pt.next;
		}
	}

	public Vertex_Node findVertex(String s) {
		Vertex_Node pt = head;
		while (pt != null && s.compareTo(pt.name) != 0)
			pt = pt.next;
		return pt;
	}

	public Vertex_Node input(String fileName) throws IOException {
		String inputLine, sourceName, targetName;
		Vertex_Node source = null, target;
		Edge_Node e;
		StringTokenizer input;
		BufferedReader inFile = new BufferedReader(new FileReader(fileName));
		inputLine = inFile.readLine();
		while (inputLine != null) {
			input = new StringTokenizer(inputLine);
			sourceName = input.nextToken();
			source = findVertex(sourceName);
			if (source == null) {
				head = new Vertex_Node(sourceName, head);
				source = head;
				size++;
			}
			if (input.hasMoreTokens()) {
				targetName = input.nextToken();
				target = findVertex(targetName);
				if (target == null) {
					head = new Vertex_Node(targetName, head);
					target = head;
					size++;
				}

				// put edge in one direction -- after checking for repeat
				e = source.edge_head;
				while (e != null) {
					if (e.target == target) {
						System.out.print("Multiple edges from " + source.name
								+ " to ");
						System.out.println(target.name + ".");
						break;
					}
					e = e.next;
				}
				source.edge_head = new Edge_Node(target, source.edge_head);

				// put edge in the other direction
				e = target.edge_head;
				while (e != null) {
					if (e.target == source) {
						System.out.print("Multiple edges from " + target.name
								+ " to ");
						System.out.println(source.name + ".");
						break;
					}
					e = e.next;
				}
				target.edge_head = new Edge_Node(source, target.edge_head);
			}
			inputLine = inFile.readLine();
		}
		inFile.close();
		return source;
	}

	public void output() {
		Vertex_Node v = head;
		Edge_Node e;
		while (v != null) {
			System.out.print(v.name + ":  ");
			e = v.edge_head;
			while (e != null) {
				System.out.print(e.target.name + "  ");
				e = e.next;
			}
			System.out.println();
			v = v.next;
		}
	}
	

	public void output_bfs(Vertex_Node s) {
		Queue<Vertex_Node> q = new LinkedList<Vertex_Node>();	//Use java Queue
		clearDist();				//set distances to -1, means not visited

		q.add(s);				//put starting vertex in queue
		s.SetDistance(0);			//special case: set distance to 0
		s.SetParent(null);			// set parent to null
                
		while(!q.isEmpty()) {			//go until queue is empty
			s = q.remove();
			Edge_Node e = s.GetNbrList();	//get the head of the edge list

			while(e != null) {						//until no edges
				if(e.GetTarget().GetDistance() == -1) {			//if not visited
					Vertex_Node n = e.GetTarget();			//get target
					n.SetParent(s);					//set parent
					n.SetDistance(n.GetParent().GetDistance() + 1);	//set Distance
				
					q.add(n);					//enqueue
				}
				e = e.GetNext();					//get next edge neighbor
			}
			System.out.print(s.GetName() + ", " + s.GetDistance() + ", ");
			
			if(s.GetParent() == null) {					//printing format
				System.out.println(s.GetParent());
			} else {
				System.out.println(s.GetParent().GetName());
			}

			if(q.isEmpty()) {						//exit if queue is empty
				Vertex_Node head = GetGraphHead();			//get Graph head
				while(head != null && head.GetDistance() != -1) {	//loop through graph
					head = head.GetNext();				//until either the end or
				}							//find unvisited vertex
				if(head != null && head.GetDistance() == -1) {		//if we find unvisited
					q.add(head);					//enqueue vertex
					head.SetDistance(0);				
					head.SetParent(null);				//special case again
				}
			}
		}
	}

	public void output_dfs(Vertex_Node s) {				//DRIVER
		clearDist();						//set distances to -1
		s.SetDistance(0);
		s.SetParent(null);					//take care of special case
		dfs(s);							//recursively call dfs

		Vertex_Node head = GetGraphHead();			//get graph head

		while(head != null) {					//loop until end of graph
			if(head != null && head.GetDistance() == -1) {
				head.SetDistance(0);			//if find unvisted vertex
				head.SetParent(null);			//special case
				dfs(head);				//recursively call dfs
			}
			head = head.GetNext();				//advance head
		}
	}

	private static void dfs(Vertex_Node s) {
		System.out.print(s.GetName() + ", " + s.GetDistance() + ", ");

		if(s.GetParent() == null) {
			System.out.println(s.GetParent());		//printing format
		} else {
			System.out.println(s.GetParent().GetName());
		}

		Edge_Node e = s.GetNbrList();				//get edge list head
		while(e != null) {					//loop until end of edge list
			if(e.GetTarget().GetDistance() == -1) {
				Vertex_Node n = e.GetTarget();		//get target, set parent
				n.SetParent(s);
				n.SetDistance(n.GetParent().GetDistance() + 1);	//set distance according to parent
				dfs(n);					//recursively call dfs on unvisited verticies
									//in edge list
			}			 
			e = e.GetNext();				//advance edge list pointer
		}
	}
}
