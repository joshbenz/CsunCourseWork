// You need to modify line 8 so that it points to the directory
// containing the graphs. For most IDE's I believe you can just
// make that the empty string if you put the graph files in the
// same directory as your prog4.java file.

import java.io.*; // for IOException

public class test1 {

	final static String GraphLocation = new String(
			"/home/requiem/CSUN/prog4/proj4/Graphs/");

	public static void main(String[] args) throws IOException {
		Vertex_Node startVertex;
		Graph g;

		for (int i = 1; i <= 10; i++) {
			g = new Graph();
			startVertex = g.input(GraphLocation + "Graph" + i + ".txt");
			System.out.println("Test #" + i + ":  BFS");
			System.out.println("=======");
			g.output_bfs(startVertex);
			System.out.println();
			if (i==3) {
				System.out.println("=======");
				g.output_bfs(startVertex);
				System.out.println();

			}
			// Delete everything between these comments if you did not implement
			// DFS

			System.out.println("Test #" + i + ":  DFS");
			System.out.println("=======");
			g.output_dfs(startVertex);
			System.out.println();
			if (i==3) {
				System.out.println("=======");
				g.output_dfs(startVertex);
				System.out.println();

			}

			// Delete everything between these comments if you did not implement
			// DFS
		}
	}
}
