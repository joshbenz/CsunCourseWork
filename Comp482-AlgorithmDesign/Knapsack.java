/*
 *  Joshua Benz
 *  Comp482
 *  Project2
 *
 * */

import java.io.*;
import java.lang.Integer;
import java.util.*;

public class Knapsack {
	int totalWeight;
	int[] weights;
	int[] benefits;

	int optimalGreedyBenefit = -1;
	int optimalBruteForceBenefit = -1; //for outside testing of greedy analysis

	List<Item> items = new ArrayList<>();

	public Knapsack(int W, int[] w, int[] b) {
		totalWeight = W;
		weights		= w;
		benefits	= b;
		
		for(int i=0; i<weights.length; i++) { 
			items.add(new Item(i, weights[i], benefits[i])); 
		}
	}

	private int max(int x, int y) { return (x > y)? x : y; }

	private void printArray(int[][] table) {
		for(int i=0; i<weights.length; i++) {
			for(int j=0; j<=totalWeight; j++) {
				System.out.print("  " + table[i][j]);
			}
			System.out.println();
		}
	} 

	private String printSet(List<Item> set) {
		String strSet = "{";
		for(Item item : set) {
			strSet = strSet + item.id + ",";
		}
		strSet = strSet.substring(0, strSet.length() - 1);
		return strSet + "}";
	} 

	public static int[] generateSubset(int k, int n) {
		String subset = Integer.toBinaryString(k);
		int[] subsetArray = new int[n]; //language garuntees that arrays of integral type are init with 0's

		for(int i=0; i<subset.length(); i++) {
			subsetArray[i] = Integer.parseInt("" + subset.charAt(i));
		}
		return subsetArray;
	}

	public void BruteForceSolution() {
		List<Item> tmpItems = items;
		Set<Subset> optimalSubsets = new HashSet<Subset>();

		int maxWeightSoFar 	= 0;
		int maxBenefitSoFar = 0;

		for(int i=0; i<(1<<tmpItems.size()); i++) { //every subset
			Subset subset = new Subset(generateSubset(i, tmpItems.size()), tmpItems);
			if(subset.weightSum <= totalWeight) {
				if(subset.benefitSum >= maxBenefitSoFar) {
					maxBenefitSoFar = subset.benefitSum; //keep track of last optimal subset
					optimalSubsets.add(subset);
				}
			}
		}
		//print optimal Sets only
		for(Subset s : optimalSubsets) {
			if(s.benefitSum == maxBenefitSoFar) {
				System.out.println(s.toString());
			}
		}
		optimalBruteForceBenefit = maxBenefitSoFar;
	}

	public void DynamicProgrammingSolution(boolean printBmatrix) {
		int table[][] = new int[items.size()][totalWeight+1]; 
		List<Item> optimalSet = new ArrayList<>();

		for(int i=1; i<items.size(); i++) {
			for(int j=0; j<=totalWeight; j++) {
				
				if(i==0 || j==0) {	// B[0,w] = 0  for  0 <= w <= W
					table[i][j] = 0;
				} else if(items.get(i).weight <= j) {  //B [k,w] = max { B[k-1,w] , B[k-1, w - wk] + bk }
					table[i][j] = max(table[i-1][j], items.get(i).benefit + table[i-1][j-items.get(i).weight]);
				} else { // if w < wk then B[k,w] = B[k-1,w]. 
					table[i][j] = table[i-1][j];
				}
			}
		}

		//get the optimal set from the table
		int j = totalWeight;
		for(int i=items.size()-1; i>0; i--) {
			if(table[i][j] != table[i-1][j]) {
				//this item contributes
				optimalSet.add(items.get(i));
				j -= items.get(i).weight;
			}
			if(j < 0) break;
		}

		if(printBmatrix) { printArray(table); }
		Subset optimalSubset = new Subset(optimalSet); //this will claculate all the sums
		//But for the sake of proof that my table values are correct
		//I will overwrite the benefitSum using the sum from the table
		optimalSubset.setBenefitSum(table[items.size()-1][totalWeight]);

		//Although the order of the set doesn't matter by definition, 
		//I will match the order given in the specification
		Collections.reverse(optimalSubset.items);
		System.out.println(optimalSubset.toString());
	}

	public void GreedyApproximateSolution() {
		List<Item> tmpItems = items;
		tmpItems.remove(0);
		Collections.sort(tmpItems);

		List<Item> optimalSet = new ArrayList<>();

		int w = 0, benefitSum = 0;
		for(int i=0; i<tmpItems.size(); i++) {
			if(tmpItems.get(i) != null) {
				if((w + tmpItems.get(i).weight) <= totalWeight) {
					w 			+= tmpItems.get(i).weight;
					benefitSum 	+= tmpItems.get(i).benefit;
					optimalSet.add(tmpItems.get(i));
				} // else {
				//	break; If we are not allowed to skip items, then use
				//} this, but it says "add items as long as the sum of the weights
				//is <= W"
			}
		}

		Subset greedySet = new Subset(optimalSet);
		System.out.println(greedySet.toString());

		optimalGreedyBenefit = benefitSum;
	}

	// Helper class to organize items
	private class Item implements Comparable<Item> {
		int id, weight, benefit;
		double ratio;

		Item(int id, int weight, int benefit) {
			this.id 		= id;
			this.weight 	= weight;
			this.benefit 	= benefit;
			ratio = (double)benefit / (double)weight;
		}

		@Override
		public int compareTo(Item i) {
			return Double.compare(i.ratio, this.ratio);
		}
	}

	// Helper class for organizing subsets
	private class Subset {
		List<Item> items;

		int benefitSum 	= 0;
		int weightSum 	= 0;

		Subset(int[] bin, List<Item> list) {
			items = new ArrayList<>();
			for(int i=1; i<bin.length; i++) {
				if(bin[i] == 1) {
					items.add(list.get(i));
					benefitSum += list.get(i).benefit;
					weightSum  += list.get(i).weight;
				}
			}
		}

		Subset(List<Item> list) {
			items = list;
			for(Item item : items) {
					benefitSum += item.benefit;
					weightSum  += item.weight;
			}
		}

		public void setBenefitSum(int b) { benefitSum = b; } //for dynammic programming solution

		public int hashCode() {		//overload for use of HashSet to remove duplicates
			int hashcode = 0;
			String hash = "0";

			for(Item i : this.items) {
				hash += i.id;
			}

			for(int i=0; i<hash.length(); i++) {
				hashcode += (int)hash.charAt(i);
			}
			return hashcode;
		}

		public boolean equals(Object obj) { //also need to overlaod for HashSet
			if (obj instanceof Subset) {
				Subset s = (Subset) obj;

				String itemsString = new String();
				for(Item i : this.items) { //for current items list
					itemsString += i.id;
				}

				String itemsStringOther = new String();
				for(Item i : s.items) {
					itemsStringOther += i.id;
				}

				return ((itemsStringOther.equals(itemsString)) && (this.benefitSum == s.benefitSum)
						&& (this.weightSum == s.weightSum));
			} else {
				return false;
			}
		}

		@Override
		public String toString() {
			if(this.items.isEmpty()) return "No Solution!";

			String optimal = "Optimal Set = " +	printSet(this.items);
			String weight  = "Weight Sum = "  + weightSum;
			String benefit = "Benefit Sum = " + benefitSum;

			return optimal + "  " + weight + "  " + benefit;
		}
	}
}
