/*
 *  Joshua Benz
 *  Comp482
 *  Project 3
 *
 * */

import java.util.*;
import java.io.*;

public class Graph {   
    private List<List<EdgeNode>> adjList;
    private int nVertices;
    private int nEdges;
    private String fileName;

    public Graph (String inputFileName) {
        nEdges = nVertices = 0;
        fileName = inputFileName;
        adjList = new ArrayList<>();

        File file = new File(inputFileName);
        try {
            Scanner scanner = new Scanner(file);

            if(scanner.hasNextInt()) { //get n vertices
                nVertices = scanner.nextInt();
                for(int i=0; i<nVertices; i++) {
                    adjList.add(new ArrayList<EdgeNode>());
                }
            }

            while(scanner.hasNextInt()) {
                int v = scanner.nextInt();
                int u = scanner.nextInt();
                int w = scanner.nextInt();
            
                adjList.get(v).add(new EdgeNode(v, u, w));
                nEdges++;
            }
        } catch(FileNotFoundException e) {}
    }

    public void printGraph() {
        System.out.println("Graph: nVertices = " + nVertices + "  nEdges = " + nEdges);
        System.out.println("Adjacency Lists");

        for(int i=0; i<nVertices; i++) {
            System.out.print("v= " + i + "  ");
            System.out.println(adjList.get(i));
        }
    }

    public SPPacket bfsShortestPaths(int start) {
        /*
         * In class we assumed that there was a property for
         * keeping track of whick vertices have been visited. 
         * I will be using a boolean array instead.
         *
         * */
        LinkedList<Integer> queue = new LinkedList<Integer>(); 
        SPPacket packet     = new SPPacket(start, nVertices);
        boolean visited[]   = new boolean[nVertices]; 

        queue.add(start);
        visited[start] = true; //source has been visited

        int level = 0;
        int timeToDepthIncrease = queue.size();

        while(!queue.isEmpty()) {
            int vertex = queue.poll();
            timeToDepthIncrease--;

            packet.setDistance(vertex, level);

            for(EdgeNode e : adjList.get(vertex)) {
                if(!visited[e.vertex2]) {
                    visited[e.vertex2] = true;
                    queue.add(e.vertex2);
                    packet.setParent(e.vertex2, vertex);
                }
            }
            if(timeToDepthIncrease == 0) {
                level++;
                timeToDepthIncrease = queue.size();
            }
        }
        return packet;
    }

    public SPPacket dijkstraShortestPaths(int start) {
        boolean[] visited = new boolean[nVertices];
        int[] distance    = new int[nVertices];
        int[] parent      = new int[nVertices];

        for(int i=0; i<nVertices; i++) {
            visited[i]  = false;
            distance[i] = 9999;
            parent[i]   = -1;
        }

        distance[start] = 0;
        int currVertex = start;
        int next = 0;

        while(!visited[currVertex]) {
            visited[currVertex]   = true;
            List<EdgeNode> edges  = adjList.get(currVertex);

            for(EdgeNode e : edges) {
                next        = e.vertex2;
                int weight  = e.weight;

                if(distance[next]  > (distance[currVertex] + weight)) {
                    distance[next] = distance[currVertex] + weight;
                    parent[next]   = currVertex;
                }
            }
            
            currVertex = 1;
            int dist = 9999;

            for(int i=0; i<nVertices; i++) {
                if((!visited[i]) && (dist > distance[i])) {
                    dist = distance[i];
                    currVertex = i;
                }
            }
        }
        
        return (new SPPacket(start, distance, parent));
    }

    public SPPacket bellmanFordShortestPaths(int start) {
        int[] distance      = new int[nVertices];
        int[] parent        = new int[nVertices];

        for(int i=0; i<nVertices; i++) {
            distance[i] = 9999;//Integer.MAX_VALUE keeps giving me negatives here
            parent[i]   = -1;
        }

        distance[start] = 0;

       for(int i=1; i<distance.length-1; i++) { 
            for(int j=0; j<nVertices; j++) {
                List<EdgeNode> edges = adjList.get(j); 
                for(EdgeNode e : edges) {
                    if((distance[e.vertex1] + e.weight) < distance[e.vertex2]) {
                        distance[e.vertex2] = distance[e.vertex1] + e.weight;
                        parent[e.vertex2] = e.vertex1;
                    }
                }
            }
        }
       //check for negative cycles 
        for(int i=0; i<nVertices; i++) {
            List<EdgeNode> edges = adjList.get(i);
            for(EdgeNode e : edges) {
                if((distance[e.vertex1] + e.weight) < distance[e.vertex2]) {
                    return null;
                }
            }
        }

        return (new SPPacket(start, distance, parent));
    }

    public void printShortestPaths(SPPacket spp) {
        if(spp == null) {
            System.out.println("Negative Cycle! No shortest Path.");
            return;
        }

        //System.out.println(spp.toString());

        int source      = spp.getSource();
        int[] d         = spp.getDistance();
        int[] parent    = spp.getParent();
        d[source] = 0;

        System.out.println("Shortest path from vertex " + source + " to vertex");

        for(int i=0; i<d.length; i++) {
            List<Integer> path = new ArrayList<Integer>();
            System.out.print(i + ":  ");
    
            int curr = i; 
            while(curr != parent[source]) {
                path.add(curr);
                curr = parent[curr];
            }
            Collections.reverse(path);
            System.out.print(path);
            System.out.println(" Path Weight = " + d[i]);
        }
        System.out.println();
    }

    /*
     * Perform a BFS on each vertex. Check if all vertices
     * are visited each vertex.
     *
     * */
    public boolean isStronglyConnected() {
        for(int i=0; i<nVertices; i++) {
            SPPacket packet = bfsShortestPaths(i);
            if(!isAllVerticesVisited(packet, i)) 
                return false;
        }
        return true;
    }

    private boolean isAllVerticesVisited(SPPacket p, int source) {
        // if it has a distance other than -1, then it has been visited
        // with the exception of the source
        int[] d = p.getDistance();
        d[source] = 0; 

        for(int i=0; i<d.length; i++) {
            if(d[i] == -1) return false;
        }
        return true;
    }

}

class EdgeNode {   
    int vertex1;   
    int vertex2;   
    int weight;
  
    public EdgeNode(int v1, int v2, int w) { 
        vertex1 = v1; 
        vertex2 = v2; 
        weight  = w; 
    }
  
    public String toString() {
        return "(" + vertex1 + ", " + vertex2 + ", " + weight + ")"; 
    }
}

class SPPacket {   
    int[] d;    //distance array   
    int[] parent;   //parent path array   
    int source; //source vertex

    public SPPacket(int start, int[] dist, int[] pp) {
        source  = start;
        parent  = pp;
        d       = dist;
    }

    public SPPacket(int source, int nVertices) {
        this.source = source;
        parent  = new int[nVertices];
        d       = new int[nVertices];

        for(int i=0; i<nVertices; i++) {
            parent[i] = d[i] = -1;
        }
    }

    public int[] getDistance()  { return d;         }
    public int[] getParent()    { return parent;    }
    public int getSource()      { return source;    }
    
    public void setDistance(int index, int value) {
        d[index] = value;
    }

    public void setParent(int index, int value) {
        parent[index] = value;
    }

    public String toString() {
        String table = new String();
        List<Integer> dist = new ArrayList<Integer>();
        List<Integer> p = new ArrayList<Integer>();
        
        for(int i=0; i<d.length; i++) {
            dist.add(d[i]);
            p.add(parent[i]);
        }

        table = "d " + dist.toString() + "\n" +
            "pi" + p.toString() + "\n";

        return table;
    }
}
