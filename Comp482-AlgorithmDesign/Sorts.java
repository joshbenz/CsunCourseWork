/*
 *
 *	Joshua Benz
 *	September 26, 2017
 *	Project 1
 *
 */

import java.util.Random; 

public class Sorts {
	public static boolean isSorted(int[] array) {
		for(int i=0; i<array.length-1; i++) {
			if(array[i] > array[i+1]) return false;
		}
		return true;	
	}

	public static void merge(int[] array, int start, int middle, int end) {
		int n1 = middle - start + 1; //sizes of the sub arrays
        int n2 = end - middle;

		int[] buffer1 = new int[n1];			//allocate buffers to hold tmp arrays
		int[] buffer2 = new int[n2];

		for(int i=0; i<n1; i++) { buffer1[i] = array[start + i]; 	}		//copy into tmp arrays
		for(int i=0; i<n2; i++) { buffer2[i] = array[middle+1 + i]; }

		int index1 = 0;
		int index2 = 0;
		int mergedIndex = start;

		while((index1 < n1) && (index2 < n2)) { 			//keep track of the two pointers
			if(buffer1[index1] <= buffer2[index2]) { 
				array[mergedIndex] = buffer1[index1];
				index1++;
			} else { 
				array[mergedIndex] = buffer2[index2];
				index2++;
			}
			mergedIndex++;
		}

		while(index1 < n1) {  
			array[mergedIndex] = buffer1[index1]; 
			mergedIndex++;
			index1++;
		}

		while(index2 < n2) { 
			array[mergedIndex] = buffer2[index2]; 
			mergedIndex++;
			index2++;
		}
	} 

	public static void mergeSort(int[] array, int start, int end) {

		if(start < end) {
			int middle = (start + end) / 2;
			mergeSort(array, start, middle);
			mergeSort(array, middle+1, end);
			merge(array, start, middle, end);
		}
	}

	public static void mergeSort(int[] array) {
		mergeSort(array, 0, array.length-1);
	}


	/********* QuickSort**********/

	public static int partition(int[] array, int start, int end) {
		Random rand = new Random();
		int pPivot = rand.nextInt(end-start) + start; //b-a + 1 ????
		int pivot = array[pPivot];
		swap(array, pPivot, end);
		
		int pRight = end-1;
		int pLeft  = start;

		while(pLeft <= pRight) {
			while((pLeft <= pRight) && (array[pLeft] <= pivot))  { pLeft++;  }
			while((pRight >= pLeft) && (array[pRight] >= pivot)) { pRight--; }

			if(pLeft < pRight) { swap(array, pLeft, pRight);  }
		}
			swap(array, pLeft, end);
		return pLeft; 	
	}

	public static void quickSort(int[] array, int start, int end) {
		if(start < end) {
			int pivot = partition(array, start, end);
			quickSort(array, start, pivot-1);
			quickSort(array, pivot+1, end);
		}
	}

	public static void quickSort(int[] array) {
		quickSort(array, 0, array.length-1);	
	}	

	private static void swap(int[] array, int index1, int index2) {
		int tmp = array[index1];
		array[index1] = array[index2];
		array[index2] = tmp;	
	}

	private static void printArray(int[] array) {
		for(int i=0; i<=array.length-1; i++) {
			System.out.print(array[i] + ", ");
		}
	}

	public static void main(String[] args) {
		int[] array = { 34, 67, 23, 19, 122, 300, 2, 5, 17, 18, 5, 4, 3, 19, -40, 23 };
		int[] arrayCopy = array.clone();

		System.out.println();
		mergeSort(array);
		System.out.println("MergeSort: isSorted returns " + isSorted(array) + "\nSorted Array: ");
		printArray(array);
		System.out.println("\n");

		quickSort(arrayCopy);
		System.out.println("QuickSort: isSorted returns " + isSorted(arrayCopy) + "\nSorted Array: ");
		printArray(arrayCopy);
		System.out.println();
	}
}

