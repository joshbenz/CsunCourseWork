# Joshua Benz
# Vicente Figueroa
# Yan Yan
# Dushan Perera
# Project 1

# All Mind Storm projects were running ev3dev on the brick
# ev3dev : http://www.ev3dev.org/

# We are using python 3 in these projects
# python language bindings: https://github.com/ev3dev/ev3dev-lang-python

#!/usr/bin/env python3
from ev3dev.ev3 import *
from time import sleep

######Config Section#####

###ultraSonic Sensor
sonicSensor = UltrasonicSensor()
assert sonicSensor.connected, "Connect a single US sensor to any sensor port"
sonicSensor.mode='US-DIST-CM'

###ultraSonic sensor units in cm
sonicSensor.mode = 'US-DIST-CM'
units = sonicSensor.units

### Motors
rightMotor = LargeMotor('outB')
leftMotor = LargeMotor('outA')
flipMotorRight = LargeMotor('outC')
flipMotorLeft = LargeMotor('outD')

speed = 500
forward = 360
back = -forward

##### Helper Funcions #####

def moveForward():
	rightMotor.run_to_rel_pos(position_sp=forward, speed_sp=speed, stop_action="hold")
	leftMotor.run_to_rel_pos(position_sp=forward, speed_sp=speed, stop_action="hold")

def checkSonic():
	return sonicSensor.value()/10 #convert mm to cm

def collisionDetect():
	if(checkSonic() <= 10):
		return True
	else:
		return False

def flip():
	flipMotorLeft.run_to_rel_pos(position_sp=90, speed_sp=speed, stop_action="hold")
	flipMotorRight.run_to_rel_pos(position_sp=90, speed_sp=speed, stop_action="hold") #flip up
	flipMotorLeft.wait_while('running')
	flipMotorRight.wait_while('running')

	flipMotorLeft.run_to_rel_pos(position_sp=-90, speed_sp=speed, stop_action="hold")
	flipMotorRight.run_to_rel_pos(position_sp=-90, speed_sp=speed, stop_action="hold") # back down
	flipMotorRight.wait_while('running')
	flipMotorLeft.wait_while('running')


def getUnstuck():
	backupTime = 1000
	turnTime = 1500
	haltWheels()
	leftMotor.wait_while('running')
	rightMotor.wait_while('running')

	rightMotor.run_timed(time_sp=backupTime+turnTime, speed_sp=-speed)
	leftMotor.run_timed(time_sp=backupTime, speed_sp=-speed)

	rightMotor.wait_while('running')
	leftMotor.wait_while('running')

######## main #########

while True:
	if(not collisionDetect()): #nothing to flip
		moveForward() #find something to flip
	else:
		forward += 100 # incrase chargning speed
		for i int range(0,3): # try to flip 3 times
			flip()
			moveForward()
		#done trying to flip
		forward = 360 # reset forward speed
		getUnstuck()
